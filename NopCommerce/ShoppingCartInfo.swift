//
//  ShoppingCartInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class ShoppingCartInfo: NSObject {
    
    
    
    var ShowSku :                             Bool
    var ShippingMethod       :                NSString?
    var OrderReviewData_CustomProperties     :NSDictionary?
    var ShippingAddress_Company              :                NSString?
    var ShippingAddress_FaxRequired    :                      Bool
    var ShippingAddress_FaxEnabled :                          Bool
    var ShippingAddress_AvailableCountries    :               NSArray?
    
    var ShippingAddress_Address1        :                     NSString?
    var ShippingAddress_City :                                NSString?
    var ShippingAddress_Email         :                       NSString?
    var ShippingAddress_StreetAddress2Enabled :               Bool
    var ShippingAddress_CountryEnabled            :           Bool
    
    var ShippingAddress_LastName :                            NSString?
    var ShippingAddress_CountryId  :                          NSInteger?
    
    var ShippingAddress_CountryName    :                      NSString?
    var ShippingAddress_CityRequired :                        Bool
    var ShippingAddress_FaxNumber :                           NSInteger?
    var ShippingAddress_CompanyRequired :                     Bool
    var ShippingAddress_AvailableStates :                     NSArray
    
    var ShippingAddress_StateProvinceName :                   NSString?
    var ShippingAddress_FormattedCustomAddressAttributes :    NSString?
    var ShippingAddress_CustomProperties :                    NSDictionary?
    var ShippingAddress_ZipPostalCode :                       NSString?
    var ShippingAddress_StateProvinceId :                     NSInteger?
    var ShippingAddress_CompanyEnabled :                      Bool
    var ShippingAddress_CustomAddressAttributes :             NSArray?
    
    
    var ShippingAddress_PhoneRequired :                       Bool
    var ShippingAddress_StreetAddressEnabled :                Bool
    
    var ShippingAddress_CityEnabled :                         Bool
    var ShippingAddress_StateProvinceEnable :                 Bool
    var ShippingAddress_Address2 :                            NSString?
    var ShippingAddress_ZipPostalCodeEnabled :                Bool
    var ShippingAddress_PhoneNumber :                         NSString?
    var ShippingAddress_StreetAddress2Required :              Bool
    var ShippingAddress_FirstName :                           NSString?
    var ShippingAddress_PhoneEnabled :                        Bool
    var ShippingAddress_ZipPostalCodeRequired :               Bool
    var ShippingAddress_Id :                                  NSInteger?
    var ShippingAddress_StreetAddressRequired :               Bool

    
    var CustomValues :                        NSDictionary?
    var PaymentMethod :                       NSString?

    var SelectedPickUpInStore :               Bool
    var Display :                             Bool
    var IsShipEnabled :                       Bool
    
    
    var BillingAddress_Company :              NSString?
    var BillingAddress_FaxRequired :          Bool
    var BillingAddress_FaxEnabled :           Bool
    var BillingAddress_AvailableCountries :   NSArray?
    
    var BillingAddress_Address1        :                     NSString?
    var BillingAddress_City :                                NSString?
    var BillingAddress_Email         :                       NSString?
    var BillingAddress_StreetAddress2Enabled :               Bool
    var BillingAddress_CountryEnabled            :           Bool
    
    var BillingAddress_LastName :                            NSString?
    var BillingAddress_CountryId  :                          NSInteger?
    
    var BillingAddress_CountryName    :                      NSString?
    var BillingAddress_CityRequired :                        Bool
    var BillingAddress_FaxNumber :                           NSInteger?
    var BillingAddress_CompanyRequired :                     Bool
    var BillingAddress_AvailableStates :                     NSArray?
    
    
    var BillingAddress_StateProvinceName :                   NSString?
    var BillingAddress_FormattedCustomAddressAttributes :    NSString?
    var BillingAddress_CustomProperties :                    NSDictionary?
    var BillingAddress_ZipPostalCode :                       NSString?
    var BillingAddress_StateProvinceId :                     NSInteger?
    var BillingAddress_CompanyEnabled :                      Bool
    var BillingAddress_CustomAddressAttributes :             NSArray?
    
    
    var BillingAddress_PhoneRequired :                       Bool
    var BillingAddress_StreetAddressEnabled :                Bool
    
    var BillingAddress_CityEnabled :                         Bool
    var BillingAddress_StateProvinceEnable :                 Bool?
    var BillingAddress_Address2 :                            NSString?
    var BillingAddress_ZipPostalCodeEnabled :                Bool
    var BillingAddress_PhoneNumber :                         NSString?
    var BillingAddress_StreetAddress2Required :              Bool
    var BillingAddress_FirstName :                           NSString?
    var BillingAddress_PhoneEnabled :                        Bool
    var BillingAddress_ZipPostalCodeRequired :               Bool
    var BillingAddress_Id :                                  NSInteger?
    var BillingAddress_StreetAddressRequired :               Bool
    
    
    
    var Count :                                              NSInteger
    var Warnings :                                           NSArray?
    var OnePageCheckoutEnabled  :                            Bool
    var ShowProductImages  :                                 Bool
    var DiscountBox_CustomProperties  :                      NSDictionary?
    var DiscountBox_Message :                                NSString?
    var DiscountBox_Display  :                               Bool
    var DiscountBox_CurrentCode :                            NSString?
    var DiscountBox_IsApplied   :                            Bool
    var TermsOfServiceOnOrderConfirmPage   :                 Bool
    var CheckoutAttributeInfo   :                            NSString?
    var GiftCardBox_CustomProperties  :                      NSDictionary?
    var GiftCardBox_Display           :                      Bool
    var GiftCardBox_IsApplied         :                      Bool
    var GiftCardBox_Message           :                      NSString?

    
    
    var subTotal    : NSString?
    var tax         : NSString?
    var orderTotal  : NSString?
    var shipping    : NSString?
    var SubTotalDiscount    : NSString?
    
    
    
    var Items                         :                      [CheckOutItems]
    var checkoutAttributes            :                      [CheckoutAttributes]
    
    
    
    
    
    
    init (JSON : AnyObject) {
        
        
        ShowSku                                   =  (JSON.valueForKeyPath("ShowSku") as! Bool)
        ShippingMethod                            =  (JSON.valueForKeyPath("OrderReviewData.ShippingMethod") as? NSString)
        OrderReviewData_CustomProperties          =  (JSON.valueForKeyPath("OrderReviewData.CustomProperties") as! NSDictionary)
        CustomValues                              =  (JSON.valueForKeyPath("OrderReviewData.CustomValues") as? NSDictionary)
        PaymentMethod                             =  (JSON.valueForKeyPath("OrderReviewData.PaymentMethod") as? NSString)
        
        SelectedPickUpInStore                     =  (JSON.valueForKeyPath("OrderReviewData.SelectedPickUpInStore") as! Bool)
        Display                                   =  (JSON.valueForKeyPath("OrderReviewData.Display") as! Bool)
        IsShipEnabled                             =  (JSON.valueForKeyPath("OrderReviewData.IsShippable") as! Bool)

        
        ShippingAddress_Company                   =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.Company") as? NSString)
        ShippingAddress_FaxRequired               =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.FaxRequired") as! Bool)
        ShippingAddress_FaxEnabled                =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.FaxEnabled") as! Bool)
        ShippingAddress_AvailableCountries        =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.AvailableCountries") as? NSArray)
        
        ShippingAddress_Address1                  =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.Address1") as? NSString)
        ShippingAddress_City                      =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.City") as? NSString)
        ShippingAddress_Email                     =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.Email") as? NSString)
        ShippingAddress_StreetAddress2Enabled     =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StreetAddress2Enabled") as! Bool)
        ShippingAddress_CountryEnabled            =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CountryEnabled") as! Bool)
        
        ShippingAddress_LastName                  =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.LastName") as? NSString)
        ShippingAddress_CountryId                 =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CountryId") as? NSInteger)
        
        ShippingAddress_CountryName               =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CountryName") as? NSString)
        ShippingAddress_CityRequired              =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CityRequired") as! Bool)
        ShippingAddress_FaxNumber                 =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.FaxNumber") as? NSInteger)
        ShippingAddress_CompanyRequired           =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CompanyRequired") as! Bool)
        ShippingAddress_AvailableStates           =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.AvailableStates") as! NSArray)
        
        ShippingAddress_StateProvinceName         =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StateProvinceName") as? NSString)
        ShippingAddress_FormattedCustomAddressAttributes = (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.FormattedCustomAddressAttributes") as? NSString)
        ShippingAddress_CustomProperties          =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CustomProperties") as? NSDictionary)
        ShippingAddress_ZipPostalCode             =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.ZipPostalCode") as? NSString)
        ShippingAddress_StateProvinceId           =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StateProvinceId") as? NSInteger)
        ShippingAddress_CompanyEnabled            =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CompanyEnabled") as! Bool)
        ShippingAddress_CustomAddressAttributes   =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CustomAddressAttributes") as? NSArray)
        
        
        ShippingAddress_PhoneRequired             =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.PhoneRequired") as! Bool)
        ShippingAddress_StreetAddressEnabled      =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StreetAddressEnabled") as! Bool)
        
        ShippingAddress_CityEnabled               =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.CityEnabled") as! Bool)
        ShippingAddress_StateProvinceEnable       =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StateProvinceEnabled") as! Bool)
        ShippingAddress_Address2                  =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.Address2") as? NSString)
        ShippingAddress_ZipPostalCodeEnabled      =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.ZipPostalCodeEnabled") as! Bool)
        ShippingAddress_PhoneNumber               =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.PhoneNumber") as? NSString)
        ShippingAddress_StreetAddress2Required    =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StreetAddress2Required") as! Bool)
        ShippingAddress_FirstName                 =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.FirstName") as? NSString)
        ShippingAddress_PhoneEnabled              =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.PhoneEnabled") as! Bool)
        ShippingAddress_ZipPostalCodeRequired     =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.ZipPostalCodeRequired") as! Bool)
        ShippingAddress_Id                        =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.Id") as? NSInteger)
        ShippingAddress_StreetAddressRequired     =  (JSON.valueForKeyPath("OrderReviewData.ShippingAddress.StreetAddressRequired") as! Bool)
        
        
        
        
        BillingAddress_Company                     =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.Company") as? NSString)
        BillingAddress_FaxRequired                 =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.FaxRequired") as! Bool)
        BillingAddress_FaxEnabled                  =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.FaxEnabled") as! Bool)
        BillingAddress_AvailableCountries          =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.AvailableCountries") as? NSArray)
        
        BillingAddress_Address1                    =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.Address1") as? NSString)
        BillingAddress_City                        =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.City") as? NSString)
        BillingAddress_Email                       =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.Email") as? NSString)
        BillingAddress_StreetAddress2Enabled       =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StreetAddress2Enabled") as! Bool)
        BillingAddress_CountryEnabled              =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CountryEnabled") as! Bool)
        
        BillingAddress_LastName                    =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.LastName") as? NSString)
        BillingAddress_CountryId                   =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CountryId") as? NSInteger)
        
        BillingAddress_CountryName                 =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CountryName") as? NSString)
        BillingAddress_CityRequired                =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CityRequired") as! Bool)
        BillingAddress_FaxNumber                   =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.FaxNumber") as? NSInteger)
        BillingAddress_CompanyRequired             =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CompanyRequired") as! Bool)
        BillingAddress_AvailableStates             =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.AvailableStates") as? NSArray)
        
        
        BillingAddress_StateProvinceName           =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StateProvinceName") as? NSString)
        BillingAddress_FormattedCustomAddressAttributes =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.FormattedCustomAddressAttributes") as? NSString)
        BillingAddress_CustomProperties            =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CustomProperties") as? NSDictionary)
        BillingAddress_ZipPostalCode               =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.ZipPostalCode") as? NSString)
        BillingAddress_StateProvinceId             =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StateProvinceId") as? NSInteger)
        BillingAddress_CompanyEnabled              =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CompanyEnabled") as! Bool)
        BillingAddress_CustomAddressAttributes     =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CustomAddressAttributes") as? NSArray)
        
        
        BillingAddress_PhoneRequired               =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.PhoneRequired") as! Bool)
        BillingAddress_StreetAddressEnabled        =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StreetAddressEnabled") as! Bool)
        
        BillingAddress_CityEnabled                 =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.CityEnabled") as! Bool)
        BillingAddress_StateProvinceEnable         =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StateProvinceEnable") as? Bool)
        BillingAddress_Address2                    =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.Address2") as? NSString)
        BillingAddress_ZipPostalCodeEnabled        =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.ZipPostalCodeEnabled") as! Bool)
        BillingAddress_PhoneNumber                 =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.PhoneNumber") as? NSString)
        BillingAddress_StreetAddress2Required      =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StreetAddress2Required") as! Bool)
        BillingAddress_FirstName                   =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.FirstName") as? NSString)
        BillingAddress_PhoneEnabled                =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.PhoneEnabled") as! Bool)
        BillingAddress_ZipPostalCodeRequired       =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.ZipPostalCodeRequired") as! Bool)
        BillingAddress_Id                          =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.Id") as? NSInteger)
        BillingAddress_StreetAddressRequired       =  (JSON.valueForKeyPath("OrderReviewData.BillingAddress.StreetAddressRequired") as! Bool)
        
        
        
        Count                                      =  (JSON.valueForKeyPath("Count") as! NSInteger)
        Warnings                                   =  (JSON.valueForKeyPath("Warnings") as? NSArray)
        OnePageCheckoutEnabled                     =  (JSON.valueForKeyPath("ShowProductImages") as! Bool)
        ShowProductImages                          =  (JSON.valueForKeyPath("ShowProductImages") as! Bool)
        DiscountBox_CustomProperties               =  (JSON.valueForKeyPath("DiscountBox.CustomProperties") as? NSDictionary)
        DiscountBox_Message                        =  (JSON.valueForKeyPath("DiscountBox.Message") as? NSString)
        DiscountBox_Display                        =  (JSON.valueForKeyPath("DiscountBox.Display") as! Bool)
        DiscountBox_CurrentCode                                =  (JSON.valueForKeyPath("DiscountBox.CurrentCode") as? NSString)
        DiscountBox_IsApplied                                  =  (JSON.valueForKeyPath("DiscountBox.IsApplied") as! Bool)
        TermsOfServiceOnOrderConfirmPage           =  (JSON.valueForKeyPath("TermsOfServiceOnOrderConfirmPage") as! Bool)
        CheckoutAttributeInfo                      =  (JSON.valueForKeyPath("CheckoutAttributeInfo") as? NSString)
        GiftCardBox_CustomProperties               =  (JSON.valueForKeyPath("GiftCardBox.GiftCardBox.CustomProperties") as? NSDictionary)
        GiftCardBox_Display                        =  (JSON.valueForKeyPath("GiftCardBox.Display") as! Bool)
        GiftCardBox_IsApplied                      =  (JSON.valueForKeyPath("GiftCardBox.IsApplied") as! Bool)
        GiftCardBox_Message                        =  (JSON.valueForKeyPath("GiftCardBox.Message") as? NSString)
        
        
        
        
        
        subTotal                                   =  (JSON.valueForKeyPath("OrderTotalResponseModel.SubTotal") as? NSString)
        tax                                        =  (JSON.valueForKeyPath("OrderTotalResponseModel.Tax") as? NSString)
        orderTotal                                 =  (JSON.valueForKeyPath("OrderTotalResponseModel.OrderTotal") as? NSString)
        SubTotalDiscount                           =  (JSON.valueForKeyPath("OrderTotalResponseModel.SubTotalDiscount") as? NSString)
        shipping                                   =   (JSON.valueForKeyPath("OrderTotalResponseModel.Shipping") as? NSString)
        
        
        
        
        Items                                      = [CheckOutItems] ()
        checkoutAttributes                         = [CheckoutAttributes] ()
 
        
        for data in (JSON.valueForKeyPath("Items") as! NSArray){
            
            let checkOut = CheckOutItems(JSON: data)
            //print(bread.Name)
            self.Items.append(checkOut)

    }
        
        
        for data in (JSON.valueForKeyPath("CheckoutAttributes") as! NSArray){
            
            let attributs = CheckoutAttributes(JSON: data)
            //print(bread.Name)
            self.checkoutAttributes.append(attributs)
            
        }

    }
}



