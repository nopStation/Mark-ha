//
//  OrderDetailsInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class OrderDetailsInfo: NSObject {
    
    var billingAddress: GenericBillingAddress?
    var shippingAddress: GenericBillingAddress?
    var items : [CheckOutItems] = []
    
    
    var PaymentMethod : String?
    var PaymentMethodStatus : String?
    var ShippingMethod : String?
    var ShippingStatus : String?
    
    
    var OrderSubtotal    : String?
    var Tax         : String?
    var OrderTotal  : String?
    var OrderShipping    : String?
    var OrderSubTotalDiscount    : String?
    var CheckoutAttributeInfo    : String?
    
    

}
