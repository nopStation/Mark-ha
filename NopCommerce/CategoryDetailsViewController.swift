//
//  CategoryDetailsViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 11/19/15.
//  Copyright (c) 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
import MFSideMenu

class CategoryDetailsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var subCategoryDDContainerView: UIView!
    @IBOutlet weak var subCategoryTableView: UITableView!
    @IBOutlet weak var subCategoryTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var subCategoryDropDownButton: UIButton!
    @IBOutlet weak var contentCollectionView: UICollectionView!
    @IBOutlet weak var slideBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    var aPIManager : APIManager?
    var apiManagerClient: APIManagerClient!
    var manufacturerFlag = false
    var categoryId : NSInteger!
    var categoryName : String!
    var subCategoriesArray : [FeaturedProductsAndCategory] = []
    var categoryDetailsInfo = CategoryDetailsInfo()
    var rightMenu : GlobalRightMenu?
    var flagForNavigation = Bool()
    var isThereAnyPagingINdexFlag = Bool()
    var pagingIndex = NSInteger ()
    
    
    override func viewWillAppear(animated: Bool) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.slideContainer.panMode = MFSideMenuPanModeNone
        self.reloadDropDownMenuList()
    }

    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
        self.aPIManager!.getShoppingCartCount(
            onSuccess:{
                getTotal in
                self.updateShoppingCartCount()
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })

    }

    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.subCategoryTableView.registerNib(UINib(nibName: "SubCategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "SubCategoryTableViewCell")
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.categoryNameLabel.text = self.categoryName
        self.subCategoryDropDownButton.hidden = true
        self.subCategoryDDContainerView.userInteractionEnabled = true
        self.subCategoryDDContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "hideSubCategoryDropDown:"))
        self.subCategoryDDContainerView.hidden = true
        
        pagingIndex = 1
        
        if flagForNavigation{
            self.slideBtn.hidden = true
        }
        else{
            self.backBtn.hidden = true
            
        }
        self.view.frame = UIScreen.mainScreen().bounds
        self.contentCollectionView.registerNib(UINib(nibName: "CategoryDetailsCollectionViewCell", bundle: NSBundle(identifier: "CategoryDetailsCollectionViewCell")), forCellWithReuseIdentifier: "CategoryDetailsCollectionViewCell")
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"reloadCategoryTableView:", name:"com.notification.reloadCategoryTableView", object: nil)
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        
        self.loadCategoryDetails()
        
    }
    
    func loadCategoryDetails() {
    
        self.showLoading()
        pagingIndex = 1

        var paramDic = [String:AnyObject]()
        paramDic["pagenumber"] = 1
        
        self.aPIManager = APIManager()
        self.aPIManager!.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params: paramDic,
            onSuccess:{
                getProducts in
                
                self.categoryDetailsInfo = getProducts
                self.apiManagerClient.categoryDetailsInfo = getProducts
                print(self.apiManagerClient.categoryDetailsInfo.notFilteredItems.count);
                
                self.contentCollectionView.reloadData()
                
                NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                
                if self.manufacturerFlag == false
                {
                    self.loadSubCategories()
                }
                else
                {
                    self.hideLoading()
                }
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
    }
    
    func loadSubCategories() {
        
        self.aPIManager = APIManager()
        self.aPIManager!.loadFeaturedProductsAndCategory(self.categoryId,
            onSuccess:{
                getSubCategories in
                
                self.subCategoriesArray = getSubCategories
                if self.subCategoriesArray.count == 0 {
                    self.subCategoryDropDownButton.hidden = true
                } else {
                    self.subCategoryDropDownButton.hidden = false
                }
                
                self.subCategoryTableView.reloadData()
                self.hideLoading()
                
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
    
    }
    
    func hideSubCategoryDropDown(gesture: UITapGestureRecognizer!) {
        
        self.subCategoryDropDownButton.selected = false
        
        UIView.animateWithDuration(2, animations: {
            
            self.subCategoryTableViewHeightConstraint.constant = 0
            
            }, completion: {
                (value: Bool) in
                
                self.subCategoryDDContainerView.hidden = true
        })
    }
    
    func showSubCategoryDropDown() {
        
        self.subCategoryDDContainerView.hidden = false
        
        UIView.animateWithDuration(2, animations: {
            if self.subCategoriesArray.count >= 5 {
                self.subCategoryTableViewHeightConstraint.constant = CGFloat(5 * 70)
            }
            else {
                self.subCategoryTableViewHeightConstraint.constant = CGFloat(self.subCategoriesArray.count * 70)
             }
            }, completion: {
                (value: Bool) in
                
        })
    }
    
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadCategoryTableView", object: nil)
    }
    
    func reloadCategoryTableView(notification: NSNotification) {
        
        
        pagingIndex = 1

        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.aPIManager = APIManager()
        var paramDic = [String:AnyObject]()
        paramDic["pagenumber"] = "1"
        
        if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice
        {
            paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice)
        }
        
        if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0)
        {
            var specsString = "\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId!)"
            for var i = 1; i < self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count; i++
            {
                specsString += ",\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId!)"
            }
            
            paramDic["specs"] = specsString
        }
        
        self.aPIManager!.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params:  paramDic,
            onSuccess:{
                getProducts in
                self.categoryDetailsInfo = getProducts
                self.apiManagerClient.categoryDetailsInfo = getProducts
                self.contentCollectionView.reloadData()
                NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            },
            onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })

        
    }
    
    @IBAction func slideBtnAct(sender: AnyObject) {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleLeftSideMenuCompletion(nil)
   
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
 
    }
    
    @IBAction func filterBtnAct(sender: AnyObject) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleRightSideMenuCompletion(nil)
        
    }
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
         let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
         self.navigationController!.pushViewController(shoppingCartView, animated: true)
        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
    }
    @IBAction func backBtnAct(sender: AnyObject) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let slideMenuViewC = SlideMenuViewController(nibName: "SlideMenuViewController", bundle: nil)
        let homeViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        appDelegate.slideContainer.leftMenuViewController=slideMenuViewC;
        appDelegate.slideContainer.centerViewController=homeViewC;
  
    }
    @IBAction func showSubCategoryList(sender: AnyObject) {
        if self.subCategoryDropDownButton.selected {
            self.subCategoryDropDownButton.selected = false
            self.hideSubCategoryDropDown(nil)
        } else {
            self.subCategoryDropDownButton.selected = true
            self.showSubCategoryDropDown()
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            
            return CGSizeMake(collectionView.bounds.size.width/2-15, 236)
            
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print(self.categoryDetailsInfo.productsInfo.count)
        
        return self.categoryDetailsInfo.productsInfo.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell : CategoryDetailsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryDetailsCollectionViewCell", forIndexPath: indexPath) as! CategoryDetailsCollectionViewCell

        self.roundCell(cell)
        
        let imageUrl = self.categoryDetailsInfo.productsInfo[indexPath.item].ImageUrl as String
        cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
        cell.nameLbl.text = self.categoryDetailsInfo.productsInfo[indexPath.item].Name as String
        
        if let price = self.categoryDetailsInfo.productsInfo[indexPath.item].Price {
          cell.amountLbl.text = price as String
        }
        return cell
    }
    
    
    func roundCell(cell:UIView) {
        
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
        cell.layer.shadowOpacity = 0.30
        cell.layer.shadowRadius = 1
        cell.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor

        
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        print(self.categoryDetailsInfo.productsInfo[indexPath.item].id)

        if self.categoryDetailsInfo.productsInfo[indexPath.item].id != 0 {

        let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
        productDetailsViewController.productId  = self.categoryDetailsInfo.productsInfo[indexPath.item].id
        
        print(self.categoryDetailsInfo.productsInfo[indexPath.item].id)
        self.navigationController!.pushViewController(productDetailsViewController, animated: true)
        }
        
    }
    
   func scrollViewDidEndDecelerating(scrollView: UIScrollView) { 
        if ((scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height)  && scrollView == contentCollectionView){

            
            print(categoryDetailsInfo.TotalPages)
            
            if self.pagingIndex < categoryDetailsInfo.TotalPages {
                
            
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            self.aPIManager = APIManager()
            
                var paramDic = [String:AnyObject]()
                
                self.pagingIndex++
                
                 paramDic["pagenumber"] = self.pagingIndex
                
                if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice
                {
                    paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice)
                }
                
                if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0)
                {
                    var specsString = "\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId)"
                    for var i = 1; i < self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count; i++
                    {
                        specsString += ",\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId)"
                    }
                    
                    paramDic["specs"] = specsString
                }
                
            self.aPIManager!.loadCategoryDetails(self.manufacturerFlag, categoryId: self.categoryId, params:  paramDic,
                onSuccess:{
                    
                    getProducts in
                    
                    
                    self.categoryDetailsInfo.productsInfo.appendContentsOf(getProducts.productsInfo)
                    self.apiManagerClient.categoryDetailsInfo.productsInfo = self.categoryDetailsInfo.productsInfo

                    print(self.categoryDetailsInfo.productsInfo.count)


                    self.contentCollectionView.reloadData()
                    
                    NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);

                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                 },
                onError: { message in
                    print(message)
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            })
        }
      }
    }
    
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }

    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.subCategoriesArray.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("SubCategoryTableViewCell") as! SubCategoryTableViewCell!
        if cell == nil {
            cell = SubCategoryTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "SubCategoryTableViewCell")
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell.subCategoryButton.layer.cornerRadius = 8.0
        cell.subCategoryButton.setTitle(self.subCategoriesArray[indexPath.row].name, forState: UIControlState.Normal)
        cell.subCategoryButton.tag = indexPath.row
        cell.subCategoryButton.addTarget(self, action: Selector("selectSubCategory:"), forControlEvents: UIControlEvents.TouchUpInside)
        return cell
        
    }
    func selectSubCategory(sender: UIButton) {
        self.categoryName = self.subCategoriesArray[sender.tag].name
        self.categoryNameLabel.text = self.categoryName
        self.categoryId =  self.subCategoriesArray[sender.tag].idd
        self.hideSubCategoryDropDown(nil)
        self.loadCategoryDetails()
    }
    
    
}
