//
//  ShippingMethod.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class ShippingMethod: NSObject {
    
    
    
    var Name             : NSString
    var Selected         : NSInteger
    var Description      : NSString
    var ShippingRateComputationMethodSystemName         : NSString
    
    init (JSON: [String: AnyObject]) {
        
        self.Name = JSON["Name"] as! NSString
        
        self.Selected = JSON["Selected"]as! NSInteger
        self.Description = JSON["Description"] as! NSString
        self.ShippingRateComputationMethodSystemName = JSON["ShippingRateComputationMethodSystemName"]as! NSString
        
        
    }


}
