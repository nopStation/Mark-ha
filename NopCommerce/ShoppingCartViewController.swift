//
//  ShoppingCartViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
//import NMPopUpViewSwift


class ShoppingCartViewController: UIViewController , UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate {

    @IBOutlet weak var attributsTableViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var contentTableViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var attributesTableView: UITableView!
    @IBOutlet weak var discountView: UIView!
    @IBOutlet weak var costView: UIView!
    @IBOutlet weak var attributeTableContainer: UIView!
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var couponTextF: UITextField!
    @IBOutlet weak var attributeTableContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributeTableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributeTableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    var aPIManager : APIManager?
    var popViewController : PopUpViewControllerSwift!
    var shoppingCartInfoArray = [ShoppingCartInfo]()
    var dropDownMenuSequenceNumber = NSInteger ()
    var dropDownMenuSelectedIndex = NSInteger ()

    var flag = Bool ()
    let dropDown = DropDown()
    var rightMenu : GlobalRightMenu?
    var checkOutGuestflag = false
    let dictionaryArray = NSMutableArray ()
    var totalAttributsViewHeight = NSInteger ()
    var increaseOrDecreaseFlag = NSInteger ()
    var textFieldStringRowIndex = NSInteger()
    var textFieldString = NSString ()
    var  textFieldDic = [Int:AnyObject]()
   
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
        
    }
    

    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.containerScrollView.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"proceedToLogin:", name:"com.notification.proceedToLogin", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"proceedToRegister:", name:"com.notification.proceedToRegister", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"proceedToCheckOut:", name:"com.notification.proceedToCheckOut", object: nil)

        increaseOrDecreaseFlag = 0
        flag = false
        dropDownMenuSequenceNumber = -1
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)

        dropDown.selectionAction = {
            [unowned self] (index, item) in
            print(self.shoppingCartInfoArray[0].checkoutAttributes[self.dropDownMenuSequenceNumber].Values[index].Name as String)
            self.shoppingCartInfoArray[0].checkoutAttributes[self.dropDownMenuSequenceNumber].Values[index].Name as String
            self.dropDownMenuSelectedIndex = index
            let indexPath = NSIndexPath(forRow: self.dropDownMenuSequenceNumber, inSection: 0)
            self.attributesTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.loadProductDetailsPagePrice(Int(self.dropDownMenuSequenceNumber),itemIndex: Int(self.dropDownMenuSelectedIndex))
 
        }
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)


    
        
        self.aPIManager = APIManager()
        
        self.showLoading()
        
        self.aPIManager!.loadShoppingCart(
            onSuccess:{
                getProducts in
                self.shoppingCartInfoArray = getProducts
                self.flag = true
                //let totalItems = self.shoppingCartInfoArray[0].Items.count as CGFloat
                self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
                self.totalAttributsViewHeight = 0
                for var i = 0 ; i < self.shoppingCartInfoArray[0].checkoutAttributes.count ; i++ {
                    let productAttributes = self.shoppingCartInfoArray[0].checkoutAttributes[i] as CheckoutAttributes
                    if productAttributes.AttributeControlType == 1
                    {
                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                        for var j = 0 ; j < self.shoppingCartInfoArray[0].checkoutAttributes[i].Values.count ; j++ {
                            if self.shoppingCartInfoArray[0].checkoutAttributes[i].Values[j].IsPreSelected {
                                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[i].Values[j].Id,
                                    "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[i].Id)]
                                self.dictionaryArray.addObject(parameters)
                                break
                            }
                        }
                    }
                    else if productAttributes.AttributeControlType == 2{
                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 85
                        
                        for var j = 0 ; j < self.shoppingCartInfoArray[0].checkoutAttributes[i].Values.count ; j++ {
                            if self.shoppingCartInfoArray[0].checkoutAttributes[i].Values[j].IsPreSelected {
                                
                                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[i].Values[j].Id,
                                    "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[i].Id)]
                                self.dictionaryArray.addObject(parameters)
                                break
                            }
                        }
                    }
                    else if productAttributes.AttributeControlType == 3{
                        
                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 41 + 44 * productAttributes.Values.count
                    }
                    else if productAttributes.AttributeControlType == 4{
                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                        
                        self.textFieldStringRowIndex = i
                        
                        self.textFieldDic[i] = self.shoppingCartInfoArray[0].checkoutAttributes[i].DefaultValue!
   
                    }
                }
                self.attributsTableViewHeightConstant.constant = CGFloat(self.totalAttributsViewHeight)
                
                if self.shoppingCartInfoArray[0].checkoutAttributes.count == 0
                {
                    self.attributsTableViewHeightConstant.constant = 0
                    self.attributeTableContainerBottomConstraint.constant = 0
                    self.attributeTableTopConstraint.constant = 0
                    self.attributeTableBottomConstraint.constant = 0
                    self.attributeTableContainer.hidden = true
                }
                
                self.checkOutForGuest()
                
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
        
    }

    deinit {
        if self.view != nil {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToLogin", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToRegister", object: nil)
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.proceedToCheckOut", object: nil)

        }
    }
    func checkOutForGuest() {
        
        self.aPIManager!.checkOutForGuest(
            onSuccess:{
                checkOutGuest in
               
                self.checkOutGuestflag = checkOutGuest
                self.hideLoading()
                self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
                self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
                self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
                self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String
                
                
                if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
                    self.showToast("Cart is empty")
                }else {
                    self.containerScrollView.hidden = false;
                    
                    
                }

                
                self.contentTableView.reloadData()
                self.attributesTableView.reloadData()
                self.roundRectToAllView(self.attributeTableContainer)
                self.roundRectToAllView(self.discountView)
                self.roundRectToAllView(self.costView)
                
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
    }
    
    func decorateAttributesTableContainer()
    {
        self.attributeTableContainer.layer.shadowColor = UIColor.darkGrayColor().CGColor
        self.attributeTableContainer.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        self.attributeTableContainer.layer.shadowOpacity = 0.50
        self.attributeTableContainer.layer.shadowRadius = 1
        self.attributeTableContainer.layer.masksToBounds = true
        self.attributeTableContainer.layer.borderWidth = 0.5
        self.attributeTableContainer.layer.borderColor = UIColor.lightGrayColor().CGColor
        self.attributeTableContainer.layer.masksToBounds = true
        self.attributeTableContainer.clipsToBounds = false
    }
    
    //checkOutForGuest
    func loadTotalOrder () {
        
        
        aPIManager?.loadTotalOrder(onSuccess: {
            gotTotalOrder in
            
           
            print(gotTotalOrder.subTotal)
            self.subTotalLbl.text = gotTotalOrder.subTotal as? String
            self.shippingLbl.text = gotTotalOrder.shipping as? String
            self.taxLbl.text      = gotTotalOrder.tax as? String
            self.totalLbl.text    = gotTotalOrder.orderTotal as? String
            

            self.contentTableView.reloadData()
            self.attributesTableView.reloadData()
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
        })
  
        
    }
    
   
    
    func proceedToLogin(notification: NSNotification) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let signInViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
        self.navigationController?.pushViewController(signInViewC, animated: true)

    }

    func proceedToRegister(notification: NSNotification) {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerViewC, animated: true)
  
    }
    
    func proceedToCheckOut(notification: NSNotification) {
        
        let finalDicArray = NSMutableArray ()
        for key in self.textFieldDic.keys {
            let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[key].Id)]
            finalDicArray.addObject(parameters)
        }
        finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
        aPIManager?.loadProceedToCheckout(finalDicArray, onSuccess:{
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let checkOutView = mainStoryBoard.instantiateViewControllerWithIdentifier("CheckOutViewController") as! CheckOutViewController
            self.navigationController?.pushViewController(checkOutView, animated: true)

            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)
    
        })

    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func proceedToCheckOutBtnAct(sender: AnyObject) {
        
        
    let defaults = NSUserDefaults.standardUserDefaults()
     if let token = defaults.stringForKey("accessToken") {
            print(token)
           let finalDicArray = NSMutableArray ()
           for key in self.textFieldDic.keys {
               let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                  "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[key].Id)]
                 finalDicArray.addObject(parameters)
              }
           finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
        
            aPIManager?.loadProceedToCheckout(finalDicArray, onSuccess:{
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let checkOutView = mainStoryBoard.instantiateViewControllerWithIdentifier("CheckOutViewController") as! CheckOutViewController
            self.navigationController?.pushViewController(checkOutView, animated: true)
            
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)

                
        })
     }else {
        
        let bundle = NSBundle(forClass: PopUpViewControllerSwift.self)
        self.popViewController = PopUpViewControllerSwift(nibName: "PopUpViewController_iPhone6", bundle: bundle)
        self.popViewController.showInView(self.view, withImage: UIImage(named: "typpzDemo"), withMessage: "You just triggered a great popup window", animated: true, checkOutGuestflag: self.checkOutGuestflag)
        

        
        }
        
        
    }
    
    
    func setRoundedBorder(radius : CGFloat, withBorderWidth borderWidth: CGFloat, withColor color : UIColor, forButton button : UIButton)
    {
        let l : CALayer = button.layer
        l.masksToBounds = true
        l.cornerRadius = radius
        l.borderWidth = borderWidth
        l.borderColor = color.CGColor
    }

    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
    }
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    
    @IBAction func applyCouponBtnAct(sender: AnyObject) {
        
        
        if self.couponTextF.text?.characters.count > 0 {
            self.view.endEditing(true)
            aPIManager?.applyDiscountCoupon(self.couponTextF.text!, onSuccess:{
                gotTotalOrder in
                self.subTotalLbl.text = gotTotalOrder.subTotal as? String
                self.shippingLbl.text = gotTotalOrder.shipping as? String
                self.taxLbl.text      = gotTotalOrder.tax as? String
                self.totalLbl.text    = gotTotalOrder.orderTotal as? String
                self.hideLoading()
                self.showToast("Your Coupon Added successfully")
                },
                onError: {
                    message in
                    print(message)
                    self.hideLoading()
                    self.showToast(message)
            })
       }
        
    }
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if tableView == attributesTableView {
        let productAttributes = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row] as CheckoutAttributes
        var cell = CGFloat()
        if productAttributes.AttributeControlType == 1 {
            cell = 65
        }
        else if productAttributes.AttributeControlType == 2 {
            cell = 85
        }
        else if productAttributes.AttributeControlType == 3 {
            cell = 41 + 44 * CGFloat(productAttributes.Values.count)
        }else if productAttributes.AttributeControlType == 4 {
            cell = 65
        }
            return cell;
      }
        return 150
        
    }

    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.contentTableView {
            if flag {
              return self.shoppingCartInfoArray[0].Items.count
            }
        }else if tableView == self.attributesTableView  {
            if flag {
          return self.shoppingCartInfoArray[0].checkoutAttributes.count
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        
        if tableView == contentTableView {
        
        var cell: ItemsTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell
            
        if cell == nil {
            tableView.registerNib(UINib(nibName: "ItemsTableViewCell", bundle: nil), forCellReuseIdentifier: "ItemsTableViewCell")
            cell = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell
        }
        
            
        cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
        cell.backgroundColor = UIColor.whiteColor()
        cell.itemImageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.itemImageView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.itemImageView.layer.shadowOpacity = 0.70
        cell.itemImageView.layer.shadowRadius = 4
        cell.itemImageView.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.itemImageView.layer.borderWidth = 1
        cell.itemImageView.layer.borderColor = UIColor.grayColor().CGColor
            
        let imageUrl = self.shoppingCartInfoArray[0].Items[indexPath.row].ImageUrl
            
         print(imageUrl)
         cell.itemImageView!.sd_setImageWithURL(NSURL(string: imageUrl!))
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell.titleLbl.text = self.shoppingCartInfoArray[0].Items[indexPath.row].ProductName as String
        cell.amountLbl.text = self.shoppingCartInfoArray[0].Items[indexPath.row].UnitPrice as String
        cell.plusBtn.tag   =  indexPath.row;
        cell.minusBtn.tag  =  indexPath.row;
        cell.deleteBtn.tag =  indexPath.row;
        print(self.shoppingCartInfoArray[0].Items[indexPath.row].Quantity)
            
        cell.incrementLbl.text = String(format: "%d",self.shoppingCartInfoArray[0].Items[indexPath.row].Quantity as NSInteger)
            
        cell.plusBtn.addTarget(self, action: "plusBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.minusBtn.addTarget(self, action: "minusBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)
            
        cell.deleteBtn.addTarget(self, action: "deleteBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)

        self.roundRectToAllView(cell.containerView)
            
           return cell
      }

        else if tableView == attributesTableView {
            let productAttributes = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row] as CheckoutAttributes
            let index = indexPath.row as Int
            print(index)
            print(productAttributes.AttributeControlType)
            
            
            if productAttributes.AttributeControlType == 1 {
                
                var cell: DropdownListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
                if cell == nil {
                    tableView.registerNib(UINib(nibName: "DropdownListTableViewCell", bundle: nil), forCellReuseIdentifier: "DropdownListTableViewCell")
                    cell = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
                }
                dropDown.anchorView = cell
                dropDown.bottomOffset = CGPoint(x: cell.frame.origin.x, y:cell.frame.origin.y + 55)
                
                print(cell.frame.origin.y)
                cell?.selectionStyle = UITableViewCellSelectionStyle.None
                cell.dropDownBtn.tag = indexPath.row
                if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].IsRequired {
                    
                    let labelText = NSMutableAttributedString(string: self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String)
                    labelText.appendAttributedString(NSAttributedString(string:"*"))
                    let selectedRange = NSMakeRange(labelText.length - 1, 1);
                    
                    labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                    labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                    
                    cell.nameLbl.attributedText = labelText
                }
                else
                {
                    cell.nameLbl.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String
                }
                for var j = 0 ; j < self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values.count ; j++ {
                    if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[j].IsPreSelected {
                        cell.titleLbl.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[j].Name as String
                        break
                    }
                }
                if self.dropDownMenuSequenceNumber == indexPath.row {
                    
                    print(self.dropDownMenuSequenceNumber)
                    cell.titleLbl.text = self.shoppingCartInfoArray[0].checkoutAttributes[self.dropDownMenuSequenceNumber].Values[dropDownMenuSelectedIndex].Name as String
                }
                cell.dropDownBtn.addTarget(self, action: "buttonActionForDropDown:", forControlEvents: UIControlEvents.TouchUpInside)
                return cell
            }
            else if productAttributes.AttributeControlType == 2 {
                
                var cell: RadioListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
                tableView.registerNib(UINib(nibName: "RadioListTableViewCell", bundle: nil), forCellReuseIdentifier: "RadioListTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
                cell?.selectionStyle = UITableViewCellSelectionStyle.None
                if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].IsRequired {
                    
                    let labelText = NSMutableAttributedString(string: self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String)
                    labelText.appendAttributedString(NSAttributedString(string:"*"))
                    let selectedRange = NSMakeRange(labelText.length - 1, 1);
                    labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                    labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                    cell.titleLbl.attributedText = labelText
                }
                else
                {
                    cell.titleLbl.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String
                }
                cell.scrollViewForRadioList.contentSize = CGSize(width:self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values.count * 150, height:44)
                var titleXPoint = NSInteger ()
                var buttonXPoint = NSInteger ()
                titleXPoint = 38
                buttonXPoint = 5
                
                
                for var i = 0 ; i < self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values.count ; i++ {
                    
                    let button   = UIButton(type: UIButtonType.Custom) as UIButton
                    button.frame = CGRectMake( CGFloat(buttonXPoint), 10, 30, 30)
                    button.setImage(UIImage(named: "radio-selected.png"), forState: UIControlState.Selected)
                    button.setImage(UIImage(named: "radio-unselected.png"), forState: UIControlState.Normal)
                    button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                    button.tag = i
                    button.addTarget(self, action: "buttonActionForRadioList:", forControlEvents: UIControlEvents.TouchUpInside)
                    
                    if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[i].IsPreSelected {
                        button.selected = true
                    }
                    else
                    {
                        button.selected = false
                    }
                    
                    
                    cell.scrollViewForRadioList.addSubview(button)
                    
                    let label = UILabel(frame: CGRectMake(CGFloat(titleXPoint), 10, 100, 30))
                    label.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[i].Name as String
                    cell.scrollViewForRadioList.addSubview(label)
                    buttonXPoint = buttonXPoint + 100 + 32
                    titleXPoint =  buttonXPoint + 35
                }
                return cell
                
            }
                
            else if productAttributes.AttributeControlType == 3{
                
                var cell: CheckboxesTableViewCell! = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
                
                if cell == nil {
                    tableView.registerNib(UINib(nibName: "CheckboxesTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckboxesTableViewCell")
                    cell = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.None

                cell.checkBoxScrollView.contentSize = CGSize(width: 200, height: self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values.count * 44 )
                var titleXPoint = NSInteger ()
                var buttonXPoint = NSInteger ()
                
                if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].IsRequired {
                    
                    let labelText = NSMutableAttributedString(string: self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String)
                    labelText.appendAttributedString(NSAttributedString(string:"*"))
                    let selectedRange = NSMakeRange(labelText.length - 1, 1);
                    
                    labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                    labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                    
                    cell.titleLbl.attributedText = labelText
                }
                else
                {
                    cell.titleLbl.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String
                }
                titleXPoint = 5
                buttonXPoint = 5
                for var i = 0 ; i < self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values.count ; i++ {
                    
                    let button   = UIButton(type: UIButtonType.Custom) as UIButton
                    button.frame = CGRectMake( 5, CGFloat (buttonXPoint), 30, 30)
                    button.setImage(UIImage(named: "unselected.png"), forState: UIControlState.Normal)
                    button.setImage(UIImage(named: "selected.png"), forState: UIControlState.Selected)
                    button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                    button.tag = i
                    button.addTarget(self, action: "buttonActionForCheckMark:", forControlEvents: UIControlEvents.TouchUpInside)
                    
                    let label = UILabel(frame: CGRectMake(38, CGFloat(titleXPoint), 250, 30))
                    
                    if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[i].IsPreSelected {
                        
                        button.selected = true
                        
                        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[i].Id,
                            "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Id)]
                        self.dictionaryArray.addObject(parameters)
                    }
                    else
                    {
                        button.selected = false
                    }
                    cell.checkBoxScrollView.addSubview(button)
                    label.text = self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Values[i].Name as String
                    
                    cell.checkBoxScrollView.addSubview(label)
                    buttonXPoint = buttonXPoint + 38
                    titleXPoint =  titleXPoint + 38
                    
                }
                
                return cell
                
            }
            else if productAttributes.AttributeControlType == 4{
                var cell: TextBoxTableViewCell! = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
                
                if cell == nil {
                    tableView.registerNib(UINib(nibName: "TextBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "TextBoxTableViewCell")
                    cell = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
                }
                cell?.selectionStyle = UITableViewCellSelectionStyle.None

                cell.titleLbl.text = productAttributes.DefaultValue as? String
                
                if self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].IsRequired {
                    
                    let labelText = NSMutableAttributedString(string: self.shoppingCartInfoArray[0].checkoutAttributes[indexPath.row].Name as String)
                    labelText.appendAttributedString(NSAttributedString(string:"*"))
                    let selectedRange = NSMakeRange(labelText.length - 1, 1);
                    
                    labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                    labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                    
                    cell.nameLbl.attributedText = labelText
                }
                else
                {
                    cell.nameLbl.text = productAttributes.Name as String
                }
                cell.titleLbl.delegate = self
                cell.titleLbl.addTarget(self, action: "textFieldDidChanged:", forControlEvents: UIControlEvents.EditingChanged)
                return cell
            }
            
        }
        let cell: ItemsTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell

        return cell

    }
    func roundRectToAllView(view: UIView )->Void{
        view.layer.cornerRadius = 4
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 0.5
    }

    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if tableView == contentTableView {
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.shoppingCartInfoArray[0].Items[indexPath.row].ProductId
            productDetailsViewController.shoppingCartId  = self.shoppingCartInfoArray[0].Items[indexPath.row].Id
            print(productDetailsViewController.productId)
            print(productDetailsViewController.shoppingCartId)
            self.navigationController!.pushViewController(productDetailsViewController, animated: true)
        }
    }

    
//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        return true
//    }
//    
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        
//        if tableView == self.contentTableView {
//        if editingStyle == UITableViewCellEditingStyle.Delete {
//            
//            
//            
//            
//            print(self.shoppingCartInfoArray[0].Items[indexPath.row].Id)
//            
//            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[indexPath.row].Id,
//                "key":"removefromcart"]
//
//            let dicArray = NSMutableArray ()
//            
//            dicArray.addObject(parameters)
//            
//            aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
//                
//                    updateCart in
//                
//                    self.updateShoppingCartCount()
//                
//                    self.shoppingCartInfoArray[0].Items.removeAtIndex(indexPath.row)
//                    self.contentTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//                    self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 200.0
//                
//                    self.contentTableView.performSelector("reloadData", withObject: nil, afterDelay: 0.5)
//                
//                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                },
//                onError: {
//                    message in
//                    print(message)
//                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                    
//            })
//            
//          }
//            
//        }
//    }
    
    
    
    func plusBtnAction(sender:UIButton!)
    {
        
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        let cell  = self.contentTableView.cellForRowAtIndexPath(indexPath) as! ItemsTableViewCell
        self.shoppingCartInfoArray[0].Items[sender.tag].Quantity++
        cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity,
            "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id )]
        
        let dicArray = NSMutableArray ()
        
        dicArray.addObject(parameters)
        
        self.showLoading()
        
         aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
            
            updateCart in
            self.shoppingCartInfoArray = updateCart
            self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
            self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
            self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
            self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String
            self.updateShoppingCartCount()
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
        })
    }
    
    
    
    func deleteBtnAction(sender:UIButton!)
    {
        
        
        self.showLoading()
        
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id,
            "key":"removefromcart"]
        let dicArray = NSMutableArray ()
        dicArray.addObject(parameters)
        aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
            updateCart in

            self.shoppingCartInfoArray = updateCart
            self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
            self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
            self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
            self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String

            self.updateShoppingCartCount()
            
            let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.contentTableView)
            let cellIndexPath = self.contentTableView.indexPathForRowAtPoint(pointInTable)
            self.contentTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
            self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
            self.contentTableView.performSelector("reloadData", withObject: nil, afterDelay: 0.5)
            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            
            if self.shoppingCartInfoArray[0].Items.count == 0 {
                
                self.navigationController?.popViewControllerAnimated(true)
            }
            
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
        })
    }
    
    func minusBtnAction(sender:UIButton!)
    {
      if self.shoppingCartInfoArray[0].Items[sender.tag].Quantity > 1 {
        
        print(sender.tag)
        let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
        
        print(indexPath)
        let cell  = self.contentTableView.cellForRowAtIndexPath(indexPath) as! ItemsTableViewCell
        self.shoppingCartInfoArray[0].Items[sender.tag].Quantity--
        cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity,
                "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id )]
        let dicArray = NSMutableArray ()
        dicArray.addObject(parameters)
        self.showLoading()
        aPIManager?.DeleteAndUpdate(dicArray, onSuccess: {
                updateCart in
                self.shoppingCartInfoArray = updateCart
                self.subTotalLbl.text = self.shoppingCartInfoArray[0].subTotal as? String
                self.shippingLbl.text = self.shoppingCartInfoArray[0].shipping as? String
                self.taxLbl.text      = self.shoppingCartInfoArray[0].tax as? String
                self.totalLbl.text    = self.shoppingCartInfoArray[0].orderTotal as? String

                self.updateShoppingCartCount()
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)

                },
                onError: {
                    message in
                    print(message)
                    self.hideLoading()
                    self.showToast(message)
            })
//            aPIManager?.loadUpdateCart( self.shoppingCartInfoArray[0].Items[sender.tag].Quantity, itemId: self.shoppingCartInfoArray[0].Items[sender.tag].Id, onSuccess: {
//                
//                updateCart in
//                
//                
//                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                },
//                onError: {
//                    message in
//                    print(message)
//                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                    
//            })
//            
        }
    }
    // MARK: -UITableView Cell Button Action
    func buttonActionForRadioList(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])
        
        
        for var i = 0 ; i < self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values.count ; i++ {
            
            self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values[i].IsPreSelected = false
        }
        
        self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values[itemIndex!].IsPreSelected = true
        let indexPath = NSIndexPath(forRow: rowIndex!, inSection: 0)
        self.attributesTableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        
        
        self.loadProductDetailsPagePrice(rowIndex!,itemIndex: itemIndex!)
        
        
    }
    func loadProductDetailsPagePrice(rowIndex : NSInteger , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger ) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            for var i = 0 ; i < self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Values.count ; i++ {
                
                for var j = 0 ; j < dictionaryArray.count ; j++ {
                    let dic  = dictionaryArray.objectAtIndex(j)
                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Values[i].Id) == Int(id)  {
                        
                        indexValueCheckflag = 1
                        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Values[itemIndex].Id,
                            "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Id)]
                         self.dictionaryArray.replaceObjectAtIndex(j, withObject: parameters)
                        break
                    }
                }
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Values[itemIndex].Id,
                    "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Id)]
                self.dictionaryArray.addObject(parameters)
            }
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Values[itemIndex].Id,
                "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex].Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        
//        self.aPIManager!.loadProductDetailsPagePrice(productId, parameters: dictionaryArray,
//            onSuccess:{
//                
//                priceValue in
//                let priceValue = priceValue["Price"]
//                self.priceLbl.text = priceValue as? String
//                print(priceValue)
//                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//            },
//            onError: {
//                message in
//                print(message)
//                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
//                
//        })
        
    }

    func buttonActionForDropDown(sender:UIButton!)
    {
        print(sender.tag)
        dropDownMenuSequenceNumber = sender.tag
        let coordinates = sender.frame.origin as CGPoint
        print(coordinates.y)
        dropDown.dataSource.removeAll()
        for var index = 0; index < self.shoppingCartInfoArray[0].checkoutAttributes[sender.tag].Values.count ; index++ {
            dropDown.dataSource.append(self.shoppingCartInfoArray[0].checkoutAttributes[sender.tag].Values[index].Name as String)
        }
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }

        
    }
    func buttonActionForCheckMark(sender:UIButton!)
    {
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])
        
        print(sender.tag)
        
        if  sender.selected {
            sender.selected = false
            
        }
        else {
            sender.selected = true
            
        }
        if Int(dictionaryArray.count as NSInteger ) != 0 {
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            for var j = 0 ; j < dictionaryArray.count ; j++ {
                let dic  = dictionaryArray.objectAtIndex(j)
                let id  = dic.valueForKey("value") as! NSInteger
                if Int(self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values[itemIndex!].Id) == Int(id)  {
                    
                    indexValueCheckflag = 1
                    self.dictionaryArray.removeObjectAtIndex(j)
                    break
                }
                
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values[itemIndex!].Id,
                    "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Id)]
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Values[itemIndex!].Id,
                "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[rowIndex!].Id)]
            self.dictionaryArray.addObject(parameters)
        }
    }

    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }

    
    func textFieldDidChanged(textField:UITextField ){
        
        print(textField.tag)
        print(textField.text)
        self.textFieldDic[textField.tag] = textField.text!
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
        //self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight) + 40.0
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        textFieldString = String(format: "%@",textField.text!)
        textField.resignFirstResponder();
        
        
        //self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight) - 40.0
        
        return true;
    }

}
