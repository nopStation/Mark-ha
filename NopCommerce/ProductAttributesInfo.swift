//
//  ProductAttributesInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 11/25/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class ProductAttributesInfo: NSObject {
    
    
    
    var AllowedFileExtensions     : NSArray?
    var AttributeControlType      : NSInteger
    var CustomProperties          : NSDictionary?
    var DefaultValue              : NSString?
    var Description               : NSString?
    var Id                        : NSInteger
    var IsRequired                : Bool
    var Name                      : NSString
    var ProductAttributeId        : NSInteger
    var ProductId                 : NSInteger
    var SelectedDay               : NSString?
    var SelectedMonth             : NSString?
    var SelectedYear              : NSString?
    var TextPrompt                : NSString?
    var Values                    : [ValusInfo]
    
    init(JSON : AnyObject) {
        
        self.AllowedFileExtensions     = (JSON.valueForKeyPath("AllowedFileExtensions") as! NSArray)
        self.AttributeControlType      = (JSON.valueForKeyPath("AttributeControlType") as! NSInteger)
        self.CustomProperties          = (JSON.valueForKeyPath("CustomProperties") as! NSDictionary)
        self.DefaultValue              = (JSON.valueForKeyPath("DefaultValue") as? NSString)
        self.Description               = (JSON.valueForKeyPath("Description") as? NSString)
        self.Id                        = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.IsRequired                = (JSON.valueForKeyPath("IsRequired") as! Bool)
        self.Name                      = (JSON.valueForKeyPath("Name") as! NSString)
        self.ProductAttributeId        = (JSON.valueForKeyPath("ProductAttributeId") as! NSInteger)
        self.ProductId                 = (JSON.valueForKeyPath("ProductId") as! NSInteger)
        self.SelectedDay               = (JSON.valueForKeyPath("SelectedDay") as? NSString)
        self.SelectedMonth             = (JSON.valueForKeyPath("SelectedMonth") as? NSString)
        self.SelectedYear              = (JSON.valueForKeyPath("SelectedYear") as? NSString)
        self.TextPrompt                = (JSON.valueForKeyPath("TextPrompt") as? NSString)
        self.Values                    = [ValusInfo]()
        
        for value in (JSON.valueForKeyPath("Values") as! NSArray){
            
            let valueData = ValusInfo(JSON: value)
            self.Values.append(valueData)
            
        }  
    }

}

class ValusInfo: NSObject {
    
    
    
    var ColorSquaresRgb            : NSString?
    var CustomProperties           : NSString?
    var Id                         : NSInteger
    var IsPreSelected              : Bool
    var Name                       : NSString
    var ImageUrl                   : NSString?
    var PriceAdjustment            : NSString?
    var PriceAdjustmentValue       : NSInteger

    init(JSON : AnyObject) {
        
        self.ColorSquaresRgb       = (JSON.valueForKeyPath("ColorSquaresRgb") as? NSString)
        self.CustomProperties      = (JSON.valueForKeyPath("CustomProperties") as? NSString)
        self.Id                    = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.IsPreSelected         = (JSON.valueForKeyPath("IsPreSelected") as! Bool)
        self.Name                  = (JSON.valueForKeyPath("Name") as! NSString)
        self.ImageUrl              = (JSON.valueForKeyPath("PictureModel.ImageUrl") as? NSString)
        self.PriceAdjustment       = (JSON.valueForKeyPath("PriceAdjustment") as? NSString)
        self.PriceAdjustmentValue  = (JSON.valueForKeyPath("PriceAdjustmentValue") as! NSInteger)
        
    }
    
}



