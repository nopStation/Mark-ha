//
//  SlideMenuViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 10/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class SlideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var homeBtn: UIButton!

    
    
    let cellIdentifier : String = "CategoryTableViewCell"
    var headerCategoryArray = [GetAllCategories]()
    var apiManagerClient: APIManagerClient!
    var productCategories = [String]()
    var headerTitle : String = ""
    var catDetails = NSMutableArray()

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.view.frame = UIScreen.mainScreen().bounds
        
        let home = "  HOME"
        homeBtn.setTitle(home, forState: UIControlState.Normal)
        contentTableView.registerNib(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"reloadContainerTableView:", name:"com.notification.reloadTableView", object: nil)
        

    }

    deinit {
        
        if self.view != nil {
            
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadTableView", object: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadContainerTableView(notification: NSNotification) {

        self.headerCategoryArray = []
        for category in self.apiManagerClient.leftMenuCategoriesArray {
            if category.isLowestItem == false {
                self.headerCategoryArray.append(category)
            }
        }
        self.contentTableView.reloadData()
    }
    
    
    // MARK: - Table view data source

    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! CategoryTableViewCell!
        
        let name = self.headerCategoryArray[section].name
        headerCell.titleLabel.text = name.uppercaseString
        headerCell.collapseButton.tag = section
        
        headerCell.contentView.backgroundColor = UIColor.whiteColor()
        headerCell.titleLabel.textColor = UIColor.blackColor()
        
        headerCell.seperatorView.hidden = true
        if self.headerCategoryArray[section].parentCategoryId == 0 {
            
            headerCell.titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
            
            headerCell.imageViewWidthConstraint.constant = 0
            headerCell.titleLabelLeadingConstraint.constant = 0
            
            headerCell.closureImageView = nil
            headerCell.collapseButton.hidden = true
            headerCell.seperatorView.hidden = false
            
        } else {
            
            headerCell.imageViewWidthConstraint.constant = 25
            headerCell.titleLabelLeadingConstraint.constant = 8
            headerCell.closureImageView.image = nil
            let imageUrl = self.headerCategoryArray[section].iconPath
            Alamofire.request(.GET, imageUrl).response {
                (request, response, data, error) in
                headerCell.closureImageView.image = UIImage(data: data!, scale:1)
            }
            
            if self.headerCategoryArray[section].subCategories.count == 0 {
                headerCell.collapseButton.hidden = true
            } else {
                headerCell.collapseButton.hidden = false
                headerCell.collapseButton.addTarget(self, action: Selector("collapseCategory:"), forControlEvents: UIControlEvents.TouchUpInside)
                
                if self.headerCategoryArray[section].collapsed == false {
                    headerCell.collapseButton.selected = false
                } else {
                    headerCell.collapseButton.selected = true
                }
            }
        }
        
        headerCell.tag = section;
        let tap = UITapGestureRecognizer(target: self, action: Selector("handleTapForHeader:"))
        headerCell.addGestureRecognizer(tap)
        
        return headerCell
        
    }
    
    
    func collapseCategory(sender: UIButton) {
        self.headerCategoryArray[sender.tag].collapsed = !self.headerCategoryArray[sender.tag].collapsed
        self.contentTableView.reloadData()
    }
    
    func handleTapForHeader (sender: UITapGestureRecognizer) {
    
        print(sender.view?.tag)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        
        let category = self.headerCategoryArray[(sender.view?.tag)!]
        let categoryDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
        categoryDetailsViewController.categoryId = category.idd
        categoryDetailsViewController.categoryName = category.name
        appDelegate.slideContainer.centerViewController = categoryDetailsViewController
        
        let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
        aVariable.rightMenuViewController=rightMenuViewC;
        aVariable.toggleLeftSideMenuCompletion(nil)
        
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return self.headerCategoryArray.count
   }
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       if self.headerCategoryArray[section].collapsed == false {
            return 0
       } else {
            return self.headerCategoryArray[section].subCategories.count
       }
        
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! CategoryTableViewCell!
        if cell == nil {
            cell = CategoryTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.collapseButton.hidden = true
        
        cell.titleLabel.text = self.headerCategoryArray[indexPath.section].subCategories[indexPath.row].name
        
        return cell
            
     }
    
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        print([indexPath.row])
        let subCategory = self.headerCategoryArray[indexPath.section].subCategories[indexPath.row]
        print(subCategory.idd)
        
        
        let categoryDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
        categoryDetailsViewController.categoryId = subCategory.idd
        categoryDetailsViewController.categoryName = subCategory.name
        appDelegate.slideContainer.centerViewController = categoryDetailsViewController
        
        let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
        aVariable.rightMenuViewController=rightMenuViewC;
        aVariable.toggleLeftSideMenuCompletion(nil)
        
    }

    
    @IBAction func homeBtnAct(sender: AnyObject) {
       
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        //let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        //let homeViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        appDelegate.slideContainer.centerViewController = appDelegate.homeViewC;
        aVariable.toggleLeftSideMenuCompletion(nil)
        
    }

}
