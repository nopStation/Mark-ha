//
//  HomePageCategoryHeaderView.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/6/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class HomePageCategoryHeaderView: UIView {
    
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var headerNameButton: UIButton!
    @IBOutlet weak var headerCategoryButton: UIButton!
    
}
