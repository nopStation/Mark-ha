//
//  MyAccountViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown

class MyAccountViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var newsLetterButton: UIButton!
//    @IBOutlet weak var passwordTextField: UITextField!
//    @IBOutlet weak var confirmPassTextField: UITextField!
    
    var rightMenu : GlobalRightMenu?

    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    var dobSelected: Bool = false
    var dobDay: Int = 0
    var dobMonth: Int = 0
    var dobYear: Int = 0
    
    var genderSelected: Bool = false
    var gender: String = ""
    
    var newsLetterSelected: Bool = false
    
    var aPIManager : APIManager?
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
    }
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.aPIManager = APIManager()
        
        self.containerView.layer.cornerRadius = 4.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
       
        self.showLoading()
        aPIManager!.loadCustomerInfo(
            onSuccess: {
                accountInfo in
                self.hideLoading()
                
                if let firstName = accountInfo["FirstName"] as? String {
                    self.firstNameTextField.text = firstName
                } else {
                    self.firstNameTextField.text = ""
                }
                
                if let lastName = accountInfo["LastName"] as? String {
                    self.lastNameTextField.text = lastName
                } else {
                    self.lastNameTextField.text = ""
                }
                
                if let email =  accountInfo["Email"] as? String {
                    self.emailTextField.text = email
                } else {
                    self.emailTextField.text = ""
                }
                
                if let phoneNumber  =  accountInfo["Phone"] as? String {
                    self.phoneNumberTextField.text = phoneNumber
                } else {
                    self.phoneNumberTextField.text = ""
                }

                
                if let customerGender =  accountInfo["Gender"] as? String {
                    self.genderSelected = true
                    self.gender = customerGender
                    if self.gender == "M" {
                        self.maleRadioButton.selected = true
                        self.femaleRadioButton.selected = false
                    } else if self.gender == "F" {
                        self.maleRadioButton.selected = false
                        self.femaleRadioButton.selected = true
                    }
                } else {
                    self.genderSelected = false
                    self.gender = ""
                    self.maleRadioButton.selected = false
                    self.femaleRadioButton.selected = false
                }
                
                if let company = accountInfo["Company"] as? String {
                    self.companyTextField.text = company
                } else {
                    self.companyTextField.text = ""
                }
                
                
                if let newsletter =  accountInfo["Newsletter"] as? Bool {
                    self.newsLetterSelected =  newsletter
                    if self.newsLetterSelected == true {
                        self.newsLetterButton.selected = true
                    } else {
                        self.newsLetterButton.selected = false
                    }
                }
                
                
                if let bday = accountInfo["DateOfBirthDay"] as? Int, bmonth = accountInfo["DateOfBirthMonth"] as? Int, byear = accountInfo["DateOfBirthYear"] as? Int {
                    self.dobSelected = true
                    self.dobDay = bday
                    self.dobMonth = bmonth
                    self.dobYear = byear
                    self.dobLabel.text = "\(self.dobDay)/\(self.dobMonth)/\(self.dobYear)"
                } else {
                    self.dobSelected = false
                    self.dobDay = 0
                    self.dobMonth = 0
                    self.dobYear = 0
                    self.dobLabel.text = "dd/mm/yyyy"
                }
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
    }

    deinit
    {
        if self.view != nil{
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
    }
    
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
        
    }

    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {

       let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
       self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func showDOBPicker(sender: AnyObject) {
        
        DatePickerDialog().show("Choose Date of birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .Date) {
            (date) -> Void in
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.dobSelected = true
            self.dobLabel.text = "\(dateFormatter.stringFromDate(date))"
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            self.dobDay = components.day
            self.dobMonth = components.month
            self.dobYear =  components.year
        }
    }
    
    @IBAction func selectMale(sender: UIButton) {
        self.maleRadioButton.selected = true
        self.femaleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "M"
    }
    
    @IBAction func selectFemale(sender: UIButton) {
        self.femaleRadioButton.selected = true
        self.maleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "F"
    }
    
    @IBAction func toggleNewsLetterOption(sender: UIButton) {
        if self.newsLetterButton.selected {
            self.newsLetterButton.selected = false
            self.newsLetterSelected = false
        } else {
            self.newsLetterButton.selected = true
            self.newsLetterSelected = true
        }
    }
    
    @IBAction func saveAccountInfo(sender: UIButton) {
        
        let firstName = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if firstName.characters.count == 0 {
            self.showToast("Enter First Name")
            return
        }
        
        let lastName = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if lastName.characters.count == 0 {
            self.showToast("Enter Last Name")
            return
        }
        
//        if self.dobSelected == false {
//            self.showToast("Choose Date of birth")
//            return
//        }
//        
//        if self.genderSelected == false {
//            self.showToast("Choose gender")
//            return
//        }
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            self.showToast("Enter E-Mail address")
            return
        }
        
        if self.isValidEmail(email) == false {
            self.showToast("Invalid E-Mail address")
            return
        }
        
//        let company = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        if company.characters.count == 0 {
//            self.showToast("Enter company name")
//            return
//        }
//        
//        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        if password.characters.count == 0 {
//            self.showToast("Enter password")
//            return
//        }
//        
//        let confirmedPassword = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        if confirmedPassword.characters.count == 0 {
//            self.showToast("Confirm your password")
//            return
//        }
//        
//        if password != confirmedPassword {
//            self.showToast("Passwords don't match")
//            return
//        }
        
        self.proceedToSave()
    }
    
    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
    }
    
    func proceedToSave()
    {
        var params = [String: AnyObject]()
        params["FirstName"] = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["LastName"] = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["DateOfBirthDay"] = self.dobDay
        params["DateOfBirthMonth"] = self.dobMonth
        params["DateOfBirthYear"] = self.dobYear
        params["Email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Phone"] = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())

        params["Company"] = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Newsletter"] = self.newsLetterSelected ? "true":"false"
        params["Gender"] = self.gender
//        params["Password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        params["ConfirmPassword"] = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        self.showLoading()
        aPIManager!.saveCustomerInfo(params,
            onSuccess: {
                self.hideLoading()
                self.showToast("Account information saved successfully")
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.scrollViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.scrollViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }
    
}
