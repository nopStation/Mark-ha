//
//  GetHomePageProducts.swift
//  NopCommerce
//
//  Created by BS-125 on 11/3/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
public class GetHomePageProducts : NSObject {
    
    var CustomProperties :  String
    var ImageUrl :          String
    var Id :                NSInteger
    var Name :              String
    var OldPrice :          NSInteger
    var Price :             String
    var AllowCustomerReviews : Bool
    var ProductId :         NSInteger
    var RatingSum :         NSInteger
    var TotalReviews :      NSInteger
    var ShortDescription :  String

    init(CustomProperties : String, ImageUrl : String , Id : NSInteger , Name : String , OldPrice : NSInteger , Price : String , AllowCustomerReviews : Bool , ProductId : NSInteger , RatingSum : NSInteger , TotalReviews : NSInteger, ShortDescription : String) {
        
        self.CustomProperties = CustomProperties
        self.ImageUrl = ImageUrl
        self.Id = Id
        self.Name = Name
        self.OldPrice = OldPrice
        self.Price =  Price
        self.AllowCustomerReviews = AllowCustomerReviews
        self.ProductId = ProductId
        self.RatingSum = RatingSum
        self.TotalReviews = TotalReviews
        self.ShortDescription = ShortDescription
        
    }
}
