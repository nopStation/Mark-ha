//
//  RightSlideMenuViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 11/19/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD

class RightSlideMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    var apiManagerClient: APIManagerClient!
    @IBOutlet weak var lowerPriceLabel: UILabel!
    @IBOutlet weak var upperPriceLabel: UILabel!
    @IBOutlet weak var slideContainerView: UIView!

    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var containerTableViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var alreadyFilteredItemScrollView: UIScrollView!
    
    @IBOutlet weak var alreadyFilteredItemsVIewHeightConstraint: NSLayoutConstraint!

    //let rangeSlider = GZRangeSlider ()
    var notFilteredItemsOrderedSet: NSMutableOrderedSet = []

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame = UIScreen.mainScreen().bounds
        alreadyFilteredItemsVIewHeightConstraint.constant = 0
        //rangeSlider1.addTarget(self, action: "rangeSliderValueChanged:", forControlEvents: .ValueChanged)
        
        self.apiManagerClient = APIManagerClient.sharedInstance
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"reloadFilterTableView:", name:"com.notification.reloadFilterTableView", object: nil)

        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"SliderEndTrackingWithTouch:", name:"com.notification.SliderEndTrackingWithTouch", object: nil)

        
        
//        let width: CGFloat = 170
//        rangeSlider.frame = CGRect(x: 0, y: 0 ,
//            width: width, height: 30.0)
//        slideContainerView.addSubview(rangeSlider)

        
   
    }
    
    override func viewDidAppear(animated: Bool) {

    
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
    }

    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.reloadFilterTableView", object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.SliderEndTrackingWithTouch", object: nil)

    }
    
    
    
    
    
    
    @IBAction func removeFilterBtnAct(sender: AnyObject) {
        
        
        self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.removeAll()
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadCategoryTableView", object:nil);
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleRightSideMenuCompletion(nil)
        

        
        
    }
    
    func SliderEndTrackingWithTouch(notification: NSNotification) {
        
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadCategoryTableView", object:nil);
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleRightSideMenuCompletion(nil)
 
        
        
    }

    @IBAction func backBtnAct(sender: AnyObject) {
        
        
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleRightSideMenuCompletion(nil)
        
 
        
        
    }
    
   
    
    
    func reloadFilterTableView(notification: NSNotification) {
       

        for view in slideContainerView.subviews{
            view.removeFromSuperview()
        }
        
        
        let width: CGFloat = slideContainerView.bounds.width
        let rangeSlider = GZRangeSlider(frame: CGRect(x: 0, y: 0 ,
            width: width, height: 30.0))
        
        
        print(self.apiManagerClient.categoryDetailsInfo.fromPrice!)
        print(self.apiManagerClient.categoryDetailsInfo.toPrice!)
        
        self.lowerPriceLabel.text = String(format: "%.1f", self.apiManagerClient.categoryDetailsInfo.fromPrice!)
        self.upperPriceLabel.text = String(format: "%.1f", self.apiManagerClient.categoryDetailsInfo.toPrice!)
        
        rangeSlider.setRange(Int(self.apiManagerClient.categoryDetailsInfo.fromPrice!), maxRange: Int( self.apiManagerClient.categoryDetailsInfo.toPrice!), accuracy: 1)
        rangeSlider.valueChangeClosure = {
            (left, right) -> () in
            print("left = \(left)  right = \(right) \n")
            
            self.apiManagerClient.categoryDetailsInfo.fromPrice = Double(left)
            self.apiManagerClient.categoryDetailsInfo.toPrice   = Double(right)

            self.lowerPriceLabel.text = String(format: "%.1f",  Double(left))
            self.upperPriceLabel.text = String(format: "%.1f",  Double(right))
        }
        

        slideContainerView.addSubview(rangeSlider)

        
        
        if self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count == 0 {
            alreadyFilteredItemsVIewHeightConstraint.constant = 0
        } else {
            alreadyFilteredItemsVIewHeightConstraint.constant = 105
            
            
            let subViews = alreadyFilteredItemScrollView.subviews
            for subview in subViews{
                subview.removeFromSuperview()
            }

            var itemScrollViewTotalWidth = 0
            var buttonXPoint: CGFloat = 5.0
            for var i = 0 ; i < self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count;i++ {

                    let buttonTitle = String(format: "%@ %@", (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].SpecificationAttributeName as? String)!, (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].SpecificationAttributeOptionName as? String)!)
                    let constraintRect = CGSize(width: CGFloat.max, height: 30)
                    let boundingBox = buttonTitle.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(18.0)], context: nil)
                    
                    
                    let button   = UIButton(type: UIButtonType.Custom) as UIButton
                    button.frame = CGRectMake( buttonXPoint, 5, boundingBox.width + 10, 30)
                    button.tag = self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId!
                    
                    print(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].SpecificationAttributeOptionName)
                    button.setTitle(buttonTitle , forState: UIControlState.Normal)
                    
                    button.layer.cornerRadius = 4.0
                    //button.layer.borderWidth = 1
                    //button.layer.borderColor = UIColor.grayColor().CGColor
                    button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                    button.backgroundColor = UIColor(red: 22/255.0, green: 158/255.0, blue: 179/255.0, alpha: 1)
                    
                    
                    button.titleLabel?.font = UIFont(name: "System", size: 13)
                    alreadyFilteredItemScrollView.addSubview(button)
                    
                    buttonXPoint += (boundingBox.width + 10) + 10
                    itemScrollViewTotalWidth++
            }
            
            alreadyFilteredItemScrollView.contentSize = CGSize(width:buttonXPoint, height:40)

            
            
        }

        
       notFilteredItemsOrderedSet.removeAllObjects()
        
        for notFilteredItem in self.apiManagerClient.categoryDetailsInfo.notFilteredItems {
            let specificationAttributeName = notFilteredItem.SpecificationAttributeName!
            if !notFilteredItemsOrderedSet.containsObject(specificationAttributeName) {
                notFilteredItemsOrderedSet.addObject(specificationAttributeName)
            }
        }
        print(notFilteredItemsOrderedSet.count)

        self.containerTableViewHeightConstraint.constant = CGFloat(notFilteredItemsOrderedSet.count) * CGFloat(77.0)
        self.containerTableView.reloadData()
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) , \(rangeSlider.upperValue))")
        
        lowerPriceLabel.text = String(format:"%.1f", rangeSlider.lowerValue)
        upperPriceLabel.text = String(format:"%.1f", rangeSlider.upperValue)
        
    }

    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return notFilteredItemsOrderedSet.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        var cell: FilterTableViewCell! = tableView.dequeueReusableCellWithIdentifier("FilterTableViewCell") as? FilterTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("FilterTableViewCell") as? FilterTableViewCell
            }
        
        
        cell.itemName.text = notFilteredItemsOrderedSet[indexPath.row] as? String
            
            let spAttributeName = notFilteredItemsOrderedSet[indexPath.row] as! NSString
        

        let subViews = cell.itemScrollView.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        var itemScrollViewTotalWidth = 0
        var buttonXPoint: CGFloat = 5.0
            for var i = 0 ; i < self.apiManagerClient.categoryDetailsInfo.notFilteredItems.count;i++ {
            
                if spAttributeName == self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeName
                {
                    
                    let buttonTitle = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeOptionName as? String
                    
                    let constraintRect = CGSize(width: CGFloat.max, height: 30)
                    let boundingBox = buttonTitle!.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(18.0)], context: nil)
                    
                    
                    let button   = UIButton(type: UIButtonType.Custom) as UIButton
                    button.frame = CGRectMake( buttonXPoint, 5, boundingBox.width + 10, 30)
                    button.tag = self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].FilterId!

                    print(self.apiManagerClient.categoryDetailsInfo.notFilteredItems[i].SpecificationAttributeOptionName)
                    button.setTitle(buttonTitle! , forState: UIControlState.Normal)
                    
                    button.layer.cornerRadius = 4.0
                    button.layer.borderWidth = 1
                    button.layer.borderColor = UIColor.grayColor().CGColor
                    button.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
                    
                    
                    
                    button.titleLabel?.font = UIFont(name: "System", size: 13)
                    button.addTarget(self, action: "buttonActionForAttributeItem:", forControlEvents: UIControlEvents.TouchUpInside)
                    cell.itemScrollView.addSubview(button)
 
                    buttonXPoint += (boundingBox.width + 10) + 10
                    itemScrollViewTotalWidth++
                }
            }
        
        cell.itemScrollView.contentSize = CGSize(width:buttonXPoint, height:40)

        
        return cell
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
    }

    func buttonActionForAttributeItem(sender:UIButton!)
    {
       
        sender.backgroundColor = UIColor.lightGrayColor()
        for notFilteredItem in self.apiManagerClient.categoryDetailsInfo.notFilteredItems {
            if notFilteredItem.FilterId == sender.tag {
                
                self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.append(notFilteredItem)
            }
        }
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadCategoryTableView", object:nil);
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleRightSideMenuCompletion(nil)
   
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
