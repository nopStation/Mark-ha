//
//  BillingAddressModel.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/15/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class BillingAddressModel: NSObject {
    
    var newBillingAddress: GenericBillingAddress?
    var existingBillingAddresses: [GenericBillingAddress] = []
    
}


public class GenericBillingAddress: NSObject {
    
    var id: Int?
    var firstName: NSString?
    var lastName: NSString?
    var email: NSString?
    var companyEnabled: Bool
    var companyRequired: Bool
    var companyName: NSString?
    var countryEnabled: Bool
    var countryId: Int?
    var countryName: NSString?
    var stateProvinceEnabled: Bool
    var stateProvinceId: Int?
    var stateProvinceName: NSString?
    var cityEnabled: Bool
    var cityRequired: Bool
    var cityName: NSString?
    var streetAddressEnabled: Bool
    var streetAddressRequired: Bool
    var address1: NSString?
    var streetAddress2Enabled: Bool
    var streetAddress2Required: Bool
    var address2: NSString?
    var zipPostalCodeEnabled: Bool
    var zipPostalCodeRequired: Bool
    var zipPostalCode: NSString?
    var phoneEnabled: Bool
    var phoneRequired: Bool
    var phoneNumber: NSString?
    var faxEnabled: Bool
    var faxRequired: Bool
    var faxNumber: NSString?
    var availableCountries: [BillingCountry]
    var availableStates: [BillingState]
    
    init(address: [String: AnyObject]) {
        
        self.id = address["Id"] as? Int
        
        self.firstName = address["FirstName"] as? NSString
        self.lastName = address["LastName"] as? NSString
        self.email = address["Email"] as? NSString
        
        self.companyEnabled = address["CompanyEnabled"] as! Bool
        self.companyRequired = address["CompanyRequired"] as! Bool
        self.companyName = address["Company"] as? NSString
        
        self.countryEnabled = address["CountryEnabled"] as! Bool
        self.countryId = address["CountryId"] as? Int
        self.countryName = address["CountryName"] as? NSString
        
        self.stateProvinceEnabled = address["StateProvinceEnabled"] as! Bool
        self.stateProvinceId = address["StateProvinceId"] as? Int
        self.stateProvinceName = address["StateProvinceName"] as? NSString
        
        self.cityEnabled = address["CityEnabled"] as! Bool
        self.cityRequired = address["CityRequired"] as! Bool
        self.cityName = address["City"] as? NSString
        
        self.streetAddressEnabled = address["StreetAddressEnabled"] as! Bool
        self.streetAddressRequired = address["StreetAddressRequired"] as! Bool
        self.address1 = address["Address1"] as? NSString
        
        self.streetAddress2Enabled = address["StreetAddress2Enabled"] as! Bool
        self.streetAddress2Required = address["StreetAddress2Required"] as! Bool
        self.address2 = address["Address2"] as? NSString
        
        self.zipPostalCodeEnabled = address["ZipPostalCodeEnabled"] as! Bool
        self.zipPostalCodeRequired = address["ZipPostalCodeRequired"] as! Bool
        self.zipPostalCode = address["ZipPostalCode"] as? NSString
        
        self.phoneEnabled = address["PhoneEnabled"] as! Bool
        self.phoneRequired = address["PhoneRequired"] as! Bool
        self.phoneNumber = address["PhoneNumber"] as? NSString
        
        self.faxEnabled = address["FaxEnabled"] as! Bool
        self.faxRequired = address["FaxRequired"] as! Bool
        self.faxNumber = address["FaxNumber"] as? NSString
        
        self.availableCountries = [BillingCountry]()
        for country in address["AvailableCountries"] as! NSArray {
           let billingCountry = BillingCountry(country: country as! [String : AnyObject])
           self.availableCountries.append(billingCountry)
        }
        
        self.availableStates = [BillingState]()
        for state in address["AvailableStates"] as! NSArray {
            let billingState = BillingState(state: state as! [String : AnyObject])
            self.availableStates.append(billingState)
        }
    }

}

public class BillingCountry: NSObject {
    
    var countryName: NSString?
    var countryId: NSString?
    
    init(country: [String : AnyObject]) {
        self.countryName = country["Text"] as? NSString
        self.countryId = country["Value"] as? NSString
    }
}

public class BillingState: NSObject {
    
    var stateName: NSString?
    var stateId: NSString?
    
    init(state: [String : AnyObject]) {
        self.stateName =  state["Text"] as? NSString
        self.stateId =  state["Value"] as? NSString
    }
    
    init(stateName:NSString, stateId:NSString) {
        self.stateName = stateName
        self.stateId = stateId
    }
}


