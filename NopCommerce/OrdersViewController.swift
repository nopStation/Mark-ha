//
//  OrdersViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/28/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
class OrdersViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    
    var orderInfoArray = [OrderInfo]()
    //let dropDown = DropDown()
    var rightMenu : GlobalRightMenu?

    var ordersArray = NSMutableArray ()
    @IBOutlet weak var containerTableView: UITableView!
    var aPIManager : APIManager?
    
    @IBOutlet weak var dropDownContainerView: UIView!
    
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
        
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }

    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        containerTableView.registerNib(UINib(nibName: "OrdersTableViewCell", bundle: nil), forCellReuseIdentifier:"OrdersTableViewCell")
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.aPIManager = APIManager()

        self.aPIManager!.loadOrders(onSuccess: {
            gotOrders in
            
            self.orderInfoArray = gotOrders
            self.containerTableView.reloadData()
            self.hideLoading()
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
        
        


        // Do any additional setup after loading the view.
    }

    
    @IBAction func backBtnAct(sender: AnyObject) {
        
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
  
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }

    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        return self.orderInfoArray.count
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
 
        var cell:OrdersTableViewCell! = tableView.dequeueReusableCellWithIdentifier("OrdersTableViewCell") as? OrdersTableViewCell
        if(cell == nil)
        {
            cell = OrdersTableViewCell(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"OrdersTableViewCell")
        }
        cell?.selectionStyle = UITableViewCellSelectionStyle.None

        cell.containerView!.layer.cornerRadius = 4.0
        cell.orderNumberId.text   = NSString(format: "Order Number:%d", self.orderInfoArray[indexPath.row].Id) as String
        cell.orderStatus.text     = NSString(format: "Order Status:%@",self.orderInfoArray[indexPath.row].OrderStatus) as String
        cell.orderDate.text       = NSString(format: "Order Date:%@",self.orderInfoArray[indexPath.row].CreatedOn) as String
        cell.orderTotal.text      = NSString(format: "Order Total:%@",self.orderInfoArray[indexPath.row].OrderTotal) as String
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let orderDetailsViewC = OrderDetailsViewController(nibName: "OrderDetailsViewController", bundle: nil)
        orderDetailsViewC.orderInfo  = self.orderInfoArray[indexPath.row]
        self.navigationController!.pushViewController(orderDetailsViewC, animated: true)
        
        
        
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }


}
