//
//  ContactViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 2/11/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import MessageUI

class ContactViewController: UIViewController, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    let mailRecipient = "razib@brainstation-23.com"
    let mailSubject = "Enquiry"
    let mailBody = ""
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    
    
    
    let textMessageRecipient = "+8801912055164"
    let textMessageBody = ""
    
    let phoneNumber = "+8801912055164"
    
    let skypeId = ""
    
    var rightMenu : GlobalRightMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.containerView.layer.cornerRadius = 4.0
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
    }

    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
        let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
        self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func emailBtnAct(sender: AnyObject) {
        
        if MFMailComposeViewController.canSendMail() {
            let mailComposeViewController = configuredMailComposeViewController()
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self 
        
        mailComposerVC.setToRecipients([self.mailRecipient])
        mailComposerVC.setSubject(self.mailSubject)
        mailComposerVC.setMessageBody(self.mailBody, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    @IBAction func smsBtnAct(sender: AnyObject) {
        
        if (MFMessageComposeViewController.canSendText()) {
            let messageComposeVC = configuredMessageComposeViewController()
            self.presentViewController(messageComposeVC, animated: true, completion: nil)
        } else {
            self.showSendSMSErrorAlert()
        }
    }
    
    func configuredMessageComposeViewController() -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self
        
        messageComposeVC.recipients = [self.textMessageRecipient]
        messageComposeVC.body = self.textMessageBody
        
        return messageComposeVC
    }
    
    func showSendSMSErrorAlert() {
        let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
        errorAlert.show()
    }
    
    @IBAction func phoneCallBtnAct(sender: AnyObject) {
        let phoneUrl = NSURL(string: "telprompt:\(self.phoneNumber)")
        
        if UIApplication.sharedApplication().canOpenURL(phoneUrl!)
        {
            UIApplication.sharedApplication().openURL(phoneUrl!)
        }
        else
        {
            self.showPhoneCallErrorAlert()
        }
    }
    
    func showPhoneCallErrorAlert() {
        let errorAlert = UIAlertView(title: "Cannot Make Phone Call", message: "Your device is not able to make phone calls.", delegate: self, cancelButtonTitle: "OK")
        errorAlert.show()
    }
    
    @IBAction func skypeBtnAct(sender: AnyObject) {
        
        let skypeInstalledUrl = NSURL(string: "skype:")
        let skypeCallUrl = NSURL(string: "skype://razib.bs23?call")
        let skypeInstallationUrl = NSURL(string: "https://itunes.apple.com/app/skype/id304878510?mt=8")
        
        let installed = UIApplication.sharedApplication().canOpenURL(skypeInstalledUrl!)
        if installed
        {
            UIApplication.sharedApplication().openURL(skypeCallUrl!)
        }
        else
        {
            UIApplication.sharedApplication().openURL(skypeInstallationUrl!)
        }
    }
    
    func hideLoading() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        switch (result) {
            case MFMailComposeResultCancelled:
                self.showToast("Mail cancelled")
                break
            case MFMailComposeResultSaved:
                self.showToast("Mail saved")
                break
            case MFMailComposeResultSent:
                self.showToast("Mail sent")
                break
            case MFMailComposeResultFailed:
                self.showToast("Mail couldn't be sent. \(error!.localizedDescription)")
                break
            default:
                break
        }
    }
    
    // MARK: MFMessageComposeViewControllerDelegate Method
    
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        
        controller.dismissViewControllerAnimated(true, completion: nil)
        
        switch (result) {
            case MessageComposeResultCancelled:
                self.showToast("SMS cancelled")
                break
            case MessageComposeResultFailed:
                self.showToast("SMS couldn't be sent")
                break
            case MessageComposeResultSent:
                self.showToast("SMS sent")
                break
            default:
                break
        }
    }
}
