//
//  ItemsTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/15/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class ItemsTableViewCell: UITableViewCell {

    var request: Alamofire.Request?
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var amountLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var incrementLbl: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var plusBtn: UIButton!
    
}

/*

{
    BillingAddress =     {
        Address1 = Mohakhali;
        Address2 = "";
        AvailableCountries =         (
        );
        AvailableStates =         (
        );
        City = Dhaka;
        CityEnabled = 1;
        CityRequired = 1;
        Company = BS;
        CompanyEnabled = 1;
        CompanyRequired = 0;
        CountryEnabled = 1;
        CountryId = 10;
        CountryName = Bangladesh;
        CustomAddressAttributes =         (
        );
        CustomProperties =         {
        };
        Email = "raiyan.yousuf@bs-23.com";
        FaxEnabled = 1;
        FaxNumber = "";
        FaxRequired = 0;
        FirstName = Raiyan;
        FormattedCustomAddressAttributes = "";
        Id = 84;
        LastName = Yousuf;
        PhoneEnabled = 1;
        PhoneNumber = 01720047748;
        PhoneRequired = 1;
        StateProvinceEnabled = 1;
        StateProvinceId = "<null>";
        StateProvinceName = "<null>";
        StreetAddress2Enabled = 1;
        StreetAddress2Required = 0;
        StreetAddressEnabled = 1;
        StreetAddressRequired = 1;
        ZipPostalCode = 1001;
        ZipPostalCodeEnabled = 1;
        ZipPostalCodeRequired = 1;
    };
    CanRePostProcessPayment = 0;
    CheckoutAttributeInfo = "Gift wrapping: No";
    CreatedOn = "2015-12-23T16:28:02.047+06:00";
    CustomValues =     {
    };
    DisplayTax = 1;
    DisplayTaxRates = 0;
    DisplayTaxShippingInfo = 0;
    ErrorList =     (
    );
    GiftCards =     (
    );
    Id = 34;
    IsReOrderAllowed = 1;
    IsReturnRequestAllowed = 0;
    IsShippable = 1;
    Items =     (
        {
            AttributeInfo = "Color: Red [+$10.00]<br />HDD: 320 [+$100.00]<br />Software: Windows os [+$200.00]<br />Software: Linux os [+$50.00]<br />Custom Text: None";
            CustomProperties =             {
            };
            DownloadId = 0;
            Id = 55;
            LicenseId = 0;
            OrderItemGuid = "3771d7be-3fd3-43c2-ac30-c880e6a9ddcd";
            Picture =             {
                ImageUrl = "http://103.4.146.91:9090/content/images/thumbs/0000024_apple-macbook-pro-13-inch_80.jpeg";
            };
            ProductId = 4;
            ProductName = "Apple MacBook Pro 13-inch";
            ProductSeName = "apple-macbook-pro-13-inch";
            Quantity = 2;
            RentalInfo = "<null>";
            Sku = "<null>";
            SubTotal = "$4,320.00";
            UnitPrice = "$2,160.00";
        },
        {
            AttributeInfo = "Color: White [+$20.00]<br />HDD: 320 [+$100.00]<br />Software: Linux os [+$50.00]<br />Custom Text: None";
            CustomProperties =             {
            };
            DownloadId = 0;
            Id = 56;
            LicenseId = 0;
            OrderItemGuid = "5f67fa0e-00dd-40fa-83f6-3de6fa24a3f0";
            Picture =             {
                ImageUrl = "http://103.4.146.91:9090/content/images/thumbs/0000024_apple-macbook-pro-13-inch_80.jpeg";
            };
            ProductId = 4;
            ProductName = "Apple MacBook Pro 13-inch";
            ProductSeName = "apple-macbook-pro-13-inch";
            Quantity = 1;
            RentalInfo = "<null>";
            Sku = "<null>";
            SubTotal = "$1,970.00";
            UnitPrice = "$1,970.00";
        },
        {
            AttributeInfo = "";
            CustomProperties =             {
            };
            DownloadId = 0;
            Id = 57;
            LicenseId = 0;
            OrderItemGuid = "2fb7d212-4b21-41f4-85df-10fc6b9f1e15";
            Picture =             {
                ImageUrl = "http://103.4.146.91:9090/content/images/thumbs/0000040_apple-icam_80.jpeg";
            };
            ProductId = 17;
            ProductName = "Apple iCam";
            ProductSeName = "apple-icam";
            Quantity = 1;
            RentalInfo = "<null>";
            Sku = "<null>";
            SubTotal = "$1,300.00";
            UnitPrice = "$1,300.00";
        },
        {
            AttributeInfo = "";
            CustomProperties =             {
            };
            DownloadId = 0;
            Id = 58;
            LicenseId = 0;
            OrderItemGuid = "e1afd4a6-3f9d-4155-a9df-4b0bbde656f3";
            Picture =             {
                ImageUrl = "http://103.4.146.91:9090/content/images/thumbs/0000027_samsung-series-9-np900x4c-premium-ultrabook_80.jpeg";
            };
            ProductId = 6;
            ProductName = "Samsung Series 9 NP900X4C Premium Ultrabook";
            ProductSeName = "samsung-series-9-np900x4c-premium-ultrabook";
            Quantity = 1;
            RentalInfo = "<null>";
            Sku = "<null>";
            SubTotal = "$1,590.00";
            UnitPrice = "$1,590.00";
        }
    );
    OrderNotes =     (
    );
    OrderShipping = "$0.00";
    OrderStatus = Pending;
    OrderSubTotalDiscount = "<null>";
    OrderSubtotal = "$9,180.00";
    OrderTotal = "$7,344.00";
    OrderTotalDiscount = "($1,836.00)";
    PaymentMethod = "Cash On Delivery (COD)";
    PaymentMethodAdditionalFee = "<null>";
    PaymentMethodStatus = Pending;
    PdfInvoiceDisabled = 0;
    PickUpInStore = 0;
    PricesIncludeTax = 0;
    PrintMode = 0;
    RedeemedRewardPoints = 0;
    RedeemedRewardPointsAmount = "<null>";
    Shipments =     (
    );
    ShippingAddress =     {
        Address1 = Mohakhali;
        Address2 = "";
        AvailableCountries =         (
        );
        AvailableStates =         (
        );
        City = Dhaka;
        CityEnabled = 1;
        CityRequired = 1;
        Company = BS;
        CompanyEnabled = 1;
        CompanyRequired = 0;
        CountryEnabled = 1;
        CountryId = 10;
        CountryName = Bangladesh;
        CustomAddressAttributes =         (
        );
        CustomProperties =         {
        };
        Email = "raiyan.yousuf@bs-23.com";
        FaxEnabled = 1;
        FaxNumber = "";
        FaxRequired = 0;
        FirstName = Raiyan;
        FormattedCustomAddressAttributes = "";
        Id = 85;
        LastName = Yousuf;
        PhoneEnabled = 1;
        PhoneNumber = 01720047748;
        PhoneRequired = 1;
        StateProvinceEnabled = 1;
        StateProvinceId = "<null>";
        StateProvinceName = "<null>";
        StreetAddress2Enabled = 1;
        StreetAddress2Required = 0;
        StreetAddressEnabled = 1;
        StreetAddressRequired = 1;
        ZipPostalCode = 1001;
        ZipPostalCodeEnabled = 1;
        ZipPostalCodeRequired = 1;
    };
    ShippingMethod = Ground;
    ShippingStatus = "Not yet shipped";
    ShowSku = 0;
    StatusCode = 200;
    SuccessMessage = "<null>";
    Tax = "$0.00";
    TaxRates =     (
        {
            CustomProperties =             {
            };
            Rate = 0;
            Value = "$0.00";
        }
    );
    VatNumber = "<null>";
}*/