//
//  HomePageCategoryModel.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/6/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

public class HomePageCategoryModel: NSObject {
    
    var name :              String?
    var imageUrl :          String?
    var idd :                NSInteger?
    
    var products : [GetHomePageProducts] = []
    var subCategory : [HomePageCategories] = []
 
    init (JSON:AnyObject){
        
        self.name = JSON.valueForKeyPath("Category.Name") as? String
        self.imageUrl = JSON.valueForKeyPath("Category.DefaultPictureModel.ImageUrl") as? String
        self.idd = JSON.valueForKeyPath("Category.Id") as? NSInteger

        let productArray = JSON["Product"] as! [AnyObject]
        for data in productArray {
            var customProperties = data.valueForKeyPath("CustomProperties") as? String
            let imageUrl = data.valueForKeyPath("DefaultPictureModel.ImageUrl") as? String
            let idd = data.valueForKeyPath("Id") as? NSInteger
            let name = data.valueForKeyPath("Name") as? String
            var oldPrice = data.valueForKeyPath("ProductPrice.OldPrice") as? NSInteger
            let price = data.valueForKeyPath("ProductPrice.Price") as? String
            
            print(price)
            
            let allowCustomerReviews = data.valueForKeyPath("ReviewOverviewModel.AllowCustomerReviews") as? Bool
            let productId = data.valueForKeyPath("ReviewOverviewModel.ProductId") as? NSInteger
            let ratingSum = data.valueForKeyPath("ReviewOverviewModel.RatingSum") as? NSInteger
            let totalReviews = data.valueForKeyPath("ReviewOverviewModel.TotalReviews") as? NSInteger
            var shortDescription = data.valueForKeyPath("ShortDescription") as? String
            if customProperties==nil{
                customProperties = ""
            }
            
            
            if shortDescription==nil{
                shortDescription = ""
            }
            if oldPrice==nil{
                oldPrice = 0
            }
            
            
            let getHomePageProducts = GetHomePageProducts(CustomProperties: customProperties!, ImageUrl: imageUrl!, Id: idd!, Name: name!, OldPrice: oldPrice!, Price: price!, AllowCustomerReviews: allowCustomerReviews!, ProductId: productId!, RatingSum: ratingSum!, TotalReviews: totalReviews!, ShortDescription: shortDescription!)
            
            self.products.append(getHomePageProducts)
        }
        
        for data in (JSON.valueForKeyPath("SubCategory") as? NSArray)! {
            
            let name = data.valueForKeyPath("Name") as? String
            let id = data.valueForKeyPath("Id") as? NSInteger
            let imageUrl = data.valueForKeyPath("IconPath") as? String

            
            
            let homePageSubCategories = HomePageCategories(name: name!, imageUrl: imageUrl!, idd: id!)
            self.subCategory.append(homePageSubCategories)
        }

        
        
    }
}
























