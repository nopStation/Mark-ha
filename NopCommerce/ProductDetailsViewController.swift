//
//  ProductDetailsViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 11/9/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
class ProductDetailsViewController: UIViewController,UIPageViewControllerDataSource,UICollectionViewDataSource,UICollectionViewDelegate , UIWebViewDelegate, UITableViewDataSource, UITableViewDelegate ,UITextFieldDelegate{

    @IBOutlet weak var contentCollectionView: UICollectionView!
    var pageViewController: UIPageViewController!
    var pageImages: NSArray!
    var productId : NSInteger!
    var shoppingCartId : NSInteger!

    var aPIManager : APIManager?
    var productDetailsArray = [ProductDetailsInfo]()
    var imageUrlArray = [NSString]()
    var relatedProductDetailsInfoArray = CategoryDetailsInfo()

    @IBOutlet weak var containerScrollView: UIScrollView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    @IBOutlet weak var fullDescription: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    @IBOutlet weak var htmlWebView: UIWebView!
    
    @IBOutlet weak var viewInScrollView: UIView!
    
    @IBOutlet weak var viewInScrollViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightForHtmlWebView: NSLayoutConstraint!
    
    @IBOutlet weak var attributsViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollBottomViewConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var plusBtn: UIButton!
    
    @IBOutlet weak var contentTableVIew: UITableView!
    
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var attributsVIew: UIView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    var dropDown = DropDown()
    var rightMenu : GlobalRightMenu?

    @IBOutlet weak var collectionViewContainer: UIView!
    var dropDownMenuSequenceNumber = NSInteger ()
    var dropDownMenuSelectedIndex = NSInteger ()

    var values_id = NSMutableArray ()
    var dictionaryArray = NSMutableArray ()
    var  textFieldDic = [Int:AnyObject]()
    
    @IBOutlet weak var addToWishListBtn: UIButton!
    @IBOutlet weak var addToCartBtn: UIButton!
    
    var textFieldString = NSString ()
    var totalAttributsViewHeight = NSInteger ()
    
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var pagerContentView: UIView!
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    
    
    
    
    
    override func viewWillAppear(animated: Bool) {
        
        
      reloadDropDownMenuList()
        
    }

    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionViewHeight.constant = 0
        self.containerScrollView.hidden = true

        
        self.collectionViewContainer.layer.cornerRadius = 4
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
        
        self.dropDownMenuSequenceNumber = -1
        
        
        dropDown.selectionAction = {
            [unowned self] (index, item) in
            print(self.productDetailsArray[0].ProductAttributes[self.dropDownMenuSequenceNumber].Values[index].Name as String)
            self.productDetailsArray[0].ProductAttributes[self.dropDownMenuSequenceNumber].Values[index].Name as String
            self.dropDownMenuSelectedIndex = index
            let indexPath = NSIndexPath(forRow: self.dropDownMenuSequenceNumber, inSection: 0)
            self.contentTableVIew.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            self.loadProductDetailsPagePrice(Int(self.dropDownMenuSequenceNumber),itemIndex: Int(self.dropDownMenuSelectedIndex))

            

        }
   
        
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)

        
        
        
        
        
        self.contentCollectionView.registerNib(UINib(nibName: "ProductCollectionViewCell", bundle: NSBundle(identifier: "ProductCollectionViewCell")), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        
        self.roundRectToAllView(titleView)
        self.roundRectToAllView(descriptionView)
        self.roundRectToAllView(attributsVIew)
        self.roundRectToAllView(collectionViewContainer)
        self.roundRectToAllView(pagerContentView)

        self.addToCartBtn.layer.cornerRadius = 4
        self.addToWishListBtn.layer.cornerRadius = 4
        
        
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        self.aPIManager = APIManager()
        
        
        if (self.shoppingCartId != nil){
            
           addToCartBtn.setTitle("UPDATE CART", forState: .Normal )
            
           self.aPIManager!.AddProductToCartEdit( self.productId, shoppingCartTypeId: self.shoppingCartId ,
            onSuccess:{
            
            getProductsDetails in
            self.productDetailsArray = getProductsDetails
            self.titleLbl.text = (self.productDetailsArray[0].Name ?? "") as String
            self.priceLbl.text = (self.productDetailsArray[0].Price ?? "")  as String
            self.htmlWebView.delegate = self
                
            if let fullDesc = self.productDetailsArray[0].FullDescription {
                
                self.htmlWebView.loadHTMLString(fullDesc as String, baseURL: nil)
            }
        
            
            self.totalAttributsViewHeight = 0
            
            
            for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes.count ; i++ {
                let productAttributes = self.productDetailsArray[0].ProductAttributes[i] as ProductAttributesInfo
                if productAttributes.AttributeControlType == 1
                {
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    
                    for var j = 0 ; j < self.productDetailsArray[0].ProductAttributes[i].Values.count ; j++ {
                        
                        
                        if self.productDetailsArray[0].ProductAttributes[i].Values[j].IsPreSelected {
                            
                            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[i].Values[j].Id,
                                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[i].ProductId,
                                    self.productDetailsArray[0].ProductAttributes[i].ProductAttributeId ,
                                    self.productDetailsArray[0].ProductAttributes[i].Id)]
                            self.dictionaryArray.addObject(parameters)
                            break
                        }
                    }
                    
                }
                else if productAttributes.AttributeControlType == 2{
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 85
                    
                    for var j = 0 ; j < self.productDetailsArray[0].ProductAttributes[i].Values.count ; j++ {
                        
                        
                        if self.productDetailsArray[0].ProductAttributes[i].Values[j].IsPreSelected {
                            
                            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[i].Values[j].Id,
                                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[i].ProductId,
                                    self.productDetailsArray[0].ProductAttributes[i].ProductAttributeId ,
                                    self.productDetailsArray[0].ProductAttributes[i].Id)]
                            self.dictionaryArray.addObject(parameters)
                            break
                        }
                    }
                    
                }
                    
                else if productAttributes.AttributeControlType == 3{
                    
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 31 + 44 * productAttributes.Values.count
                    
                    
                    
                }
                    
                else if productAttributes.AttributeControlType == 4{
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    
                    
                    self.textFieldDic[i] = self.productDetailsArray[0].ProductAttributes[i].DefaultValue!
                
                }
            }
            
            
            self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight)
                
                
            for picturs in self.productDetailsArray[0].PictureModels{
                let imageUrl = (picturs.valueForKeyPath("ImageUrl") as! NSString)
                print(imageUrl)
                self.imageUrlArray.append(imageUrl)
            }
            
            self.pageImages =  self.imageUrlArray
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            self.pageViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
            self.pageViewController.dataSource = self
            let startVC = self.viewControllerAtIndex(0) as PagerViewController
            let viewControllers = NSArray(object: startVC)
            self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil)
            self.pageViewController.view.frame = CGRectMake(0, 0,self.pagerContentView.frame.width, 300)
            self.pageViewController.view.layer.cornerRadius = 4.0
            self.addChildViewController(self.pageViewController)
            self.pagerContentView.addSubview(self.pageViewController.view)
            self.pageViewController.didMoveToParentViewController(self)
            
            print(self.productId)
            
            self.contentTableVIew.reloadData()
            
            
            if (self.productId != nil) {
                self.loadRelatedProduct()
            }
            else {
                
                
                self.containerScrollView.hidden = false

                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        },
        onError: {
            message in
            print(message)
            self.hideLoading()
            self.showToast(message)

            
        })
            
            
        }
        else {
          addToCartBtn.setTitle("ADD TO CART", forState: .Normal )
          self.aPIManager!.loadProductDetails(self.productId,
              onSuccess:{
        
                getProductsDetails in
                self.productDetailsArray = getProductsDetails
                self.titleLbl.text = (self.productDetailsArray[0].Name ?? "") as String
                self.priceLbl.text = (self.productDetailsArray[0].Price ?? "") as String
                self.htmlWebView.delegate = self
                if let fullDesc = self.productDetailsArray[0].FullDescription {
                    
                    self.htmlWebView.loadHTMLString(fullDesc as String, baseURL: nil)
                }
                
             
                self.totalAttributsViewHeight = 0
                
                
            for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes.count ; i++ {
                let productAttributes = self.productDetailsArray[0].ProductAttributes[i] as ProductAttributesInfo
                if productAttributes.AttributeControlType == 1
                {
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    for var j = 0 ; j < self.productDetailsArray[0].ProductAttributes[i].Values.count ; j++ {
                        if self.productDetailsArray[0].ProductAttributes[i].Values[j].IsPreSelected {
                            
                            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[i].Values[j].Id,
                                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[i].ProductId,
                                    self.productDetailsArray[0].ProductAttributes[i].ProductAttributeId ,
                                    self.productDetailsArray[0].ProductAttributes[i].Id)]
                            self.dictionaryArray.addObject(parameters)
                            break
                        }
                    }
                     }
                else if productAttributes.AttributeControlType == 2{
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 85
                    
                    for var j = 0 ; j < self.productDetailsArray[0].ProductAttributes[i].Values.count ; j++ {
                        
                        
                        if self.productDetailsArray[0].ProductAttributes[i].Values[j].IsPreSelected {
                            
                            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[i].Values[j].Id,
                                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[i].ProductId,
                                    self.productDetailsArray[0].ProductAttributes[i].ProductAttributeId ,
                                    self.productDetailsArray[0].ProductAttributes[i].Id)]
                            self.dictionaryArray.addObject(parameters)
                            break
                        }
                    }
                    
                }

               else if productAttributes.AttributeControlType == 3{
                    
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 31 + 44 * productAttributes.Values.count
  
                }
               else if productAttributes.AttributeControlType == 4{
                    self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    
                    
                    
                    if let defaultValue = self.productDetailsArray[0].ProductAttributes[i].DefaultValue   {
                    
                     self.textFieldDic[i] = defaultValue
                        
                    }

                  }
                }
    
            self.attributsViewHeight.constant = CGFloat(self.totalAttributsViewHeight) 

                for picturs in self.productDetailsArray[0].PictureModels{
                    let imageUrl = (picturs.valueForKeyPath("ImageUrl") as! NSString)
                    print(imageUrl)
                    self.imageUrlArray.append(imageUrl)
                }
                self.pageImages =  self.imageUrlArray
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                self.pageViewController = mainStoryBoard.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
                self.pageViewController.dataSource = self
                let startVC = self.viewControllerAtIndex(0) as PagerViewController
                let viewControllers = NSArray(object: startVC)
                self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil)
                self.pageViewController.view.frame = CGRectMake(0, 0,self.pagerContentView.frame.width, 300)
                self.pageViewController.view.layer.cornerRadius = 4.0
                self.addChildViewController(self.pageViewController)
                self.pagerContentView.addSubview(self.pageViewController.view)
                self.pageViewController.didMoveToParentViewController(self)
                self.contentTableVIew.reloadData()
                if (self.productId != nil) {
                 self.loadRelatedProduct()
                }
                else {
                    
                    self.containerScrollView.hidden = false

                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                }
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)


        })
      }
  
    }

    deinit {
        
        if self.view != nil {
            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
 

        
        
    }
    
    func loadRelatedProduct(){
        
        print(productId)
        self.aPIManager!.loadRelatedProduct(productId,
            onSuccess:{
                getRelatedProductDetails in
               self.relatedProductDetailsInfoArray = getRelatedProductDetails
                
                self.containerScrollView.hidden = false

                
                if self.relatedProductDetailsInfoArray.productsInfo.count == 0
                {
                    
                    self.collectionViewHeight.constant = 0
                }
                else
                {
                    self.collectionViewHeight.constant = 230
                }
                
                self.contentCollectionView.reloadData()
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)

                
            },
            onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                
        })

    } 

    
    func roundRectToAllView(view: UIView )->Void{
//        view.layer.cornerRadius = 4
//        view.layer.masksToBounds = true
//        view.backgroundColor = UIColor.whiteColor()
//        view.layer.shadowColor = UIColor.lightGrayColor().CGColor
//        view.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
//        view.layer.shadowOpacity = 0.50
//        view.layer.shadowRadius = 1
//        view.layer.masksToBounds = true
//        view.clipsToBounds = false
//        view.layer.borderWidth = 0.5
//        view.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor
            //UIColor.lightGrayColor().CGColor
        
        
        
        
//        view.layer.cornerRadius = 4
//        view.layer.shadowOffset = CGSize(width: 0.4, height: 0.4)
//        view.layer.shadowOpacity = 0.8
//        view.layer.shadowRadius = 0.5
//        view.layer.borderColor = UIColor.redColor().CGColor

        view.layer.cornerRadius = 4
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 0.5
        
        
        
        
        
        
        
        
        
        
//        view.layer.shadowColor = UIColor.lightGrayColor().CGColor
//        view.layer.shadowOffset = CGSizeMake(5, 5);
//        view.layer.shadowOpacity = 1;
//        view.layer.shadowRadius = 1.0;
        
        
//        view.layer.cornerRadius = 4
//        view.layer.masksToBounds = true
//        view.backgroundColor = UIColor.whiteColor()
//        view.layer.shadowColor = UIColor.lightGrayColor().CGColor
//        view.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
//        view.layer.shadowOpacity = 0.30
//        view.layer.shadowRadius = 1
//        view.layer.masksToBounds = true
//        view.clipsToBounds = false
//        view.layer.borderWidth = 0.2
//        view.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor
        
    }
    
//    func roundRectToAllView(view: UIView )->Void{
//        
//        view.layer.cornerRadius = 5
////        view.layer.masksToBounds = true
////        view.backgroundColor = UIColor.whiteColor()
////        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
////        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
////        view.layer.shadowOpacity = 0.50
////        view.layer.shadowRadius = 1
////        view.layer.masksToBounds = true
////        view.clipsToBounds = false
//        
//        
//    }
    
    @IBAction func plusBtnAct(sender: UIButton) {
        
        
        if sender.selected {
          
            self.plusBtn.selected = false
            self.viewInScrollViewHeight.constant = 45
            self.heightForHtmlWebView.constant = 0
        }
        else{
            self.plusBtn.selected = true
            self.viewInScrollViewHeight.constant = 270
            self.heightForHtmlWebView.constant = 225
        }
        
    }
    
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }

        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    
    @IBAction func addToCartBtnAct(sender: AnyObject) {
      
      let finalDicArray = NSMutableArray ()
        
      for key in self.textFieldDic.keys {
            
        let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                "key":String(format: "product_attribute_%d_%d_%d", self.productDetailsArray[0].ProductAttributes[key].ProductId, self.productDetailsArray[0].ProductAttributes[key].ProductAttributeId, self.productDetailsArray[0].ProductAttributes[key].Id)]
        finalDicArray.addObject(parameters)
            
      }
        
      let parameters:Dictionary<String,AnyObject> = ["value": 1,
            "key":"addtocart_1.EnteredQuantity"]
      finalDicArray.addObject(parameters)
        
      if (self.shoppingCartId != nil){
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartId,
                "key":String(format: "addtocart_%d.UpdatedShoppingCartItemId",self.productId)]
             finalDicArray.addObject(parameters)
            
            finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
            
            
            self.showLoading()
            
            self.aPIManager!.loadAddProductToCart(1, productId: productId, parameters: finalDicArray,
                onSuccess:{
                    priceValue in
                    self.hideLoading()
                    self.updateShoppingCartCount()
                    self.showToast("Cart Updated Successfully")

                },
                onError: {
                    message in
                    print(message)
                    self.hideLoading()
                    self.showToast(message)
            })
   
            
      }
      else {
            
            finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
            self.showLoading()

            self.aPIManager!.loadAddProductToCart(1, productId: productId, parameters: finalDicArray,
            onSuccess:{
                priceValue in
                self.hideLoading()
                self.updateShoppingCartCount()
                self.showToast("Successfully added to cart")
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)
           })
      }
 
    }
    
    
    @IBAction func addToWishListBtnAct(sender: AnyObject) {
        
        let finalDicArray = NSMutableArray ()
        
        for key in self.textFieldDic.keys {
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.textFieldDic[key]!,
                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[key].ProductId,
                    self.productDetailsArray[0].ProductAttributes[key].ProductAttributeId ,
                    self.productDetailsArray[0].ProductAttributes[key].Id)]
            finalDicArray.addObject(parameters)
            
        }
        
        let parameters:Dictionary<String,AnyObject> = ["value": 1, "key":"addtocart_1.EnteredQuantity"]
        finalDicArray.addObject(parameters)

        finalDicArray.addObjectsFromArray(self.dictionaryArray as [AnyObject])
        
        self.showLoading()
        self.aPIManager!.loadAddProductToCart(2, productId: productId, parameters: finalDicArray,
                onSuccess:{
                    priceValue in
                    self.hideLoading()
                    self.showToast("Successfully added to wish list")
                    
                },
                onError: {
                    message in
                    print(message)
                    self.hideLoading()
                    self.showToast(message)
                    
                    
        })
        
    }
    
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }

    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: UIWebView Delegate

    func webViewDidFinishLoad(webView: UIWebView) {
        
        //let = webView.scrollView.contentSize.height as NSInteger
        //print(webView.scrollView.contentSize.height)
        //var  newBounds = webView.bounds as CGRect;
        //newBounds.size.height = webView.scrollView.contentSize.height;
       // print(newBounds.size.height)
        
    }
    
    
    
    @IBAction func cartBtnAct(sender: AnyObject) {
        
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
        }
    
    
    
    
    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let productAttributes = self.productDetailsArray[0].ProductAttributes[indexPath.row] as ProductAttributesInfo

        var cell = CGFloat()
        
        if productAttributes.AttributeControlType == 1 {
           
            cell = 65
            
        }
        else if productAttributes.AttributeControlType == 2 {
           
            cell = 85

            
        }
        else if productAttributes.AttributeControlType == 3 {
            
            cell = 31 + 44 * CGFloat(productAttributes.Values.count)
            
            
        }else if productAttributes.AttributeControlType == 4 {
            
            cell = 65
            
            
        }
        
        return cell;
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (productDetailsArray.first != nil) {
        
         return self.productDetailsArray[0].ProductAttributes.count
        }
        
        return 0
        
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
         let productAttributes = self.productDetailsArray[0].ProductAttributes[indexPath.row] as ProductAttributesInfo
         let index = indexPath.row as Int
         print(index)
         print(productAttributes.AttributeControlType)
        
        
          if productAttributes.AttributeControlType == 1 {

            var cell: DropdownListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell

            print(productAttributes.AttributeControlType)

            if cell == nil {
                tableView.registerNib(UINib(nibName: "DropdownListTableViewCell", bundle: nil), forCellReuseIdentifier: "DropdownListTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("DropdownListTableViewCell") as? DropdownListTableViewCell
              }

            print(cell.frame.origin.y)
            cell?.selectionStyle = UITableViewCellSelectionStyle.None

            cell.dropDownBtn.tag = indexPath.row
            
            if self.productDetailsArray[0].ProductAttributes[indexPath.row].IsRequired {
                
                 let labelText = NSMutableAttributedString(string: self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String)
                 labelText.appendAttributedString(NSAttributedString(string:"*"))
                 let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String
            }

            for var j = 0 ; j < self.productDetailsArray[0].ProductAttributes[indexPath.row].Values.count ; j++ {
                if self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[j].IsPreSelected {
                    cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[j].Name as String
                  break
                    
                }
            }
            
        
            if self.dropDownMenuSequenceNumber == indexPath.row {
                
               print(self.dropDownMenuSequenceNumber)
               cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[self.dropDownMenuSequenceNumber].Values[dropDownMenuSelectedIndex].Name as String
               }
            cell.dropDownBtn.addTarget(self, action: "buttonActionForDropDown:", forControlEvents: UIControlEvents.TouchUpInside)

            return cell

            
        }
        else if productAttributes.AttributeControlType == 2 {
            
            var cell: RadioListTableViewCell! = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
            
           // if cell == nil {
                tableView.registerNib(UINib(nibName: "RadioListTableViewCell", bundle: nil), forCellReuseIdentifier: "RadioListTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("RadioListTableViewCell") as? RadioListTableViewCell
           // }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None

            
            if self.productDetailsArray[0].ProductAttributes[indexPath.row].IsRequired {
                
                let labelText = NSMutableAttributedString(string: self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String
            }

            
            
            cell.scrollViewForRadioList.contentSize = CGSize(width:self.productDetailsArray[0].ProductAttributes[indexPath.row].Values.count * 150, height:44)
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            titleXPoint = 38
            buttonXPoint = 5
            

            for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes[indexPath.row].Values.count ; i++ {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( CGFloat(buttonXPoint), 10, 30, 30)
                button.setImage(UIImage(named: "radio-selected.png"), forState: UIControlState.Selected)
                button.setImage(UIImage(named: "radio-unselected.png"), forState: UIControlState.Normal)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = i
                button.addTarget(self, action: "buttonActionForRadioList:", forControlEvents: UIControlEvents.TouchUpInside)
                
                if self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[i].IsPreSelected {
                    button.selected = true
                }
                else
                {
                    button.selected = false
                }
                
                
                cell.scrollViewForRadioList.addSubview(button)

                let label = UILabel(frame: CGRectMake(CGFloat(titleXPoint), 10, 100, 30))
                label.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[i].Name as String
                cell.scrollViewForRadioList.addSubview(label)
                buttonXPoint = buttonXPoint + 100 + 32
                titleXPoint =  buttonXPoint + 35
                
            }

            return cell
  
        }
        
        else if productAttributes.AttributeControlType == 3{
            
            var cell: CheckboxesTableViewCell! = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "CheckboxesTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckboxesTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("CheckboxesTableViewCell") as? CheckboxesTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None

            cell.checkBoxScrollView.contentSize = CGSize(width: 200, height: self.productDetailsArray[0].ProductAttributes[indexPath.row].Values.count * 44 )
            var titleXPoint = NSInteger ()
            var buttonXPoint = NSInteger ()
            
            if self.productDetailsArray[0].ProductAttributes[indexPath.row].IsRequired {
                
                let labelText = NSMutableAttributedString(string: self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.titleLbl.attributedText = labelText
            }
            else
            {
                cell.titleLbl.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String
            }
            
            
            
            titleXPoint = 5
            buttonXPoint = 5
            for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes[indexPath.row].Values.count ; i++ {
                
                let button   = UIButton(type: UIButtonType.Custom) as UIButton
                button.frame = CGRectMake( 5, CGFloat (buttonXPoint), 30, 30)
                button.setImage(UIImage(named: "unselected.png"), forState: UIControlState.Normal)
                button.setImage(UIImage(named: "selected.png"), forState: UIControlState.Selected)
                button.accessibilityValue = String(format: "%d/%d",indexPath.row,i)
                button.tag = i
                button.addTarget(self, action: "buttonActionForCheckMark:", forControlEvents: UIControlEvents.TouchUpInside)
                
                let label = UILabel(frame: CGRectMake(38, CGFloat(titleXPoint), 250, 30))
                
                if self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[i].IsPreSelected {
                    
                    button.selected = true
                    
                    let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[i].Id,
                        "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[indexPath.row].ProductId,
                            self.productDetailsArray[0].ProductAttributes[indexPath.row].ProductAttributeId ,
                            self.productDetailsArray[0].ProductAttributes[indexPath.row].Id)]
                    self.dictionaryArray.addObject(parameters)

                    
                  }
                else
                {
                    button.selected = false
                }
                
                cell.checkBoxScrollView.addSubview(button)
                label.text = self.productDetailsArray[0].ProductAttributes[indexPath.row].Values[i].Name as String
                
                cell.checkBoxScrollView.addSubview(label)
                buttonXPoint = buttonXPoint + 38
                titleXPoint =  titleXPoint + 38
                
            }

            return cell
 
        }
        else if productAttributes.AttributeControlType == 4{
            var cell: TextBoxTableViewCell! = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "TextBoxTableViewCell", bundle: nil), forCellReuseIdentifier: "TextBoxTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("TextBoxTableViewCell") as? TextBoxTableViewCell
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.None

            cell.titleLbl.text = productAttributes.DefaultValue as? String
            cell.titleLbl.tag = indexPath.row;
            
            cell.titleLbl.delegate = self
            cell.titleLbl.addTarget(self, action: "textFieldDidChanged:", forControlEvents: UIControlEvents.EditingChanged)
            
            if self.productDetailsArray[0].ProductAttributes[indexPath.row].IsRequired {
                
                let labelText = NSMutableAttributedString(string: self.productDetailsArray[0].ProductAttributes[indexPath.row].Name as String)
                labelText.appendAttributedString(NSAttributedString(string:"*"))
                let selectedRange = NSMakeRange(labelText.length - 1, 1);
                
                labelText.addAttribute(NSForegroundColorAttributeName, value: UIColor.redColor(), range: selectedRange)
                labelText.addAttribute(NSBaselineOffsetAttributeName, value: 2, range: selectedRange)
                
                cell.nameLbl.attributedText = labelText
            }
            else
            {
                cell.nameLbl.text = productAttributes.Name as String
            }
            

            
            
        
            
            return cell
        }

        var cell:CategoryRow! = tableView.dequeueReusableCellWithIdentifier("cell") as? CategoryRow
        if(cell == nil)
        {
            cell = CategoryRow(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"cell")
        }
        return cell
    }
    
    // MARK: - Table View Delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

        
    }
    

    // MARK: -UITableView Cell Button Action

    
    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.scrollBottomViewConstraint.constant = 15
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.scrollBottomViewConstraint.constant = keyboardSize.height + 15
                self.view.layoutIfNeeded()
            }
        }
    }


    func buttonActionForRadioList(sender:UIButton!)
    {
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])

        for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes[rowIndex!].Values.count ; i++ {
            
           self.productDetailsArray[0].ProductAttributes[rowIndex!].Values[i].IsPreSelected = false
        }
        
        self.productDetailsArray[0].ProductAttributes[rowIndex!].Values[itemIndex!].IsPreSelected = true
        let indexPath = NSIndexPath(forRow: rowIndex!, inSection: 0)
        self.contentTableVIew.reloadRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        self.loadProductDetailsPagePrice(rowIndex!,itemIndex: itemIndex!)
        
        
    }


    
    func loadProductDetailsPagePrice(rowIndex : NSInteger , itemIndex: NSInteger){
        
        if Int(dictionaryArray.count as NSInteger ) != 0 {
            
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
            for var i = 0 ; i < self.productDetailsArray[0].ProductAttributes[rowIndex].Values.count ; i++ {
                
                for var j = 0 ; j < dictionaryArray.count ; j++ {
                    let dic  = dictionaryArray.objectAtIndex(j)

                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(self.productDetailsArray[0].ProductAttributes[rowIndex].Values[i].Id) == Int(id)  {
                    
                    indexValueCheckflag = 1
                        let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[rowIndex].Values[itemIndex].Id,
                            "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[rowIndex].ProductId,
                                self.productDetailsArray[0].ProductAttributes[rowIndex].ProductAttributeId ,
                                self.productDetailsArray[0].ProductAttributes[rowIndex].Id)]
                        self.dictionaryArray.replaceObjectAtIndex(j, withObject: parameters)
                        break
                    }
                }
            }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[rowIndex].Values[itemIndex].Id,
                    "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[rowIndex].ProductId,
                        self.productDetailsArray[0].ProductAttributes[rowIndex].ProductAttributeId ,
                        self.productDetailsArray[0].ProductAttributes[rowIndex].Id)]
                self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[rowIndex].Values[itemIndex].Id,
                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[rowIndex].ProductId,
                    self.productDetailsArray[0].ProductAttributes[rowIndex].ProductAttributeId ,
                    self.productDetailsArray[0].ProductAttributes[rowIndex].Id)]
            self.dictionaryArray.addObject(parameters)
            
        }
        

        
        
        
        self.aPIManager!.loadProductDetailsPagePrice(productId, parameters: dictionaryArray,
            onSuccess:{
                
                priceValue in
                let priceValue = priceValue["Price"]
                self.priceLbl.text = priceValue as? String
                print(priceValue)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            },
            onError: {
                message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                
        })
        
    }

    
    func buttonActionForDropDown(sender:UIButton!)
    {
        
        
        print(sender.tag)
        
        dropDownMenuSequenceNumber = sender.tag
        let coordinates = sender.frame.origin as CGPoint
        print(coordinates)
       
        let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.contentTableVIew)
        let cellIndexPath = self.contentTableVIew.indexPathForRowAtPoint(pointInTable)
        
        let cell = self.contentTableVIew.cellForRowAtIndexPath(cellIndexPath!)
        dropDown.anchorView = cell
        dropDown.bottomOffset = CGPoint(x: cell!.frame.origin.x, y:cell!.frame.origin.y)
        dropDown.dataSource.removeAll()
        
        for var index = 0; index < self.productDetailsArray[0].ProductAttributes[sender.tag].Values.count ; index++ {
            
            dropDown.dataSource.append(self.productDetailsArray[0].ProductAttributes[sender.tag].Values[index].Name as String)
            
        }
        if dropDown.hidden {
            dropDown.show()
        } else {
            dropDown.hide()
        }
        
        

        
    }

    
    func buttonActionForCheckMark(sender:UIButton!)
    {
        
        
        var values = sender.accessibilityValue?.componentsSeparatedByString("/")
        let rowIndex:Int? = Int(values![0])
        let itemIndex:Int? = Int(values![1])

        
        print(sender.tag)
        
        if  sender.selected {
            sender.selected = false
            
        }
        else {
            sender.selected = true
            
        }
        
        
        
        
        if Int(dictionaryArray.count as NSInteger ) != 0 {
            var indexValueCheckflag = NSInteger ()
            indexValueCheckflag = 0
                for var j = 0 ; j < dictionaryArray.count ; j++ {
                    let dic  = dictionaryArray.objectAtIndex(j)
                    let id  = dic.valueForKey("value") as! NSInteger
                    if Int(self.productDetailsArray[0].ProductAttributes[rowIndex!].Values[itemIndex!].Id) == Int(id)  {
                        
                        indexValueCheckflag = 1
                        self.dictionaryArray.removeObjectAtIndex(j)
                        break
                    }
                }
            
            if indexValueCheckflag == 0{
                let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[rowIndex!].Values[itemIndex!].Id,
                    "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[rowIndex!].ProductId,
                        self.productDetailsArray[0].ProductAttributes[rowIndex!].ProductAttributeId ,
                        self.productDetailsArray[0].ProductAttributes[rowIndex!].Id)]
                 self.dictionaryArray.addObject(parameters)
                
                
            }
            
        }
        else {
            
            let parameters:Dictionary<String,AnyObject> = ["value": self.productDetailsArray[0].ProductAttributes[rowIndex!].Values[itemIndex!].Id,
                "key":String(format: "product_attribute_%d_%d_%d",self.productDetailsArray[0].ProductAttributes[rowIndex!].ProductId,
                    self.productDetailsArray[0].ProductAttributes[rowIndex!].ProductAttributeId ,
                    self.productDetailsArray[0].ProductAttributes[rowIndex!].Id)]
                self.dictionaryArray.addObject(parameters)
            
        }
        
        
        
        self.aPIManager!.loadProductDetailsPagePrice(productId, parameters: dictionaryArray,
            onSuccess:{
                
                priceValue in
                let priceValue = priceValue["Price"]
                
                self.priceLbl.text = priceValue as? String
                print(priceValue)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            },
            onError: {
                message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                
        })
        
        
    }
    

    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            
            return CGSizeMake(150, 176)
            
    }
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return self.relatedProductDetailsInfoArray.productsInfo.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell : ProductCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductCollectionViewCell", forIndexPath: indexPath) as! ProductCollectionViewCell
        

        
//        cell.layer.cornerRadius = 5
//        cell.layer.masksToBounds = true
//        cell.backgroundColor = UIColor.whiteColor()
//        cell.layer.borderWidth = 1.0
//        cell.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        
        self.roundCell(cell)
        print(indexPath.item)
        
        //cell.AlbumImage!.image = nil
        
        let imageUrl = self.relatedProductDetailsInfoArray.productsInfo[indexPath.item].ImageUrl as String
        
        cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
//        Alamofire.request(.GET,imageUrl ).response{
//            (
//            request, response, data, error) in
//            cell.AlbumImage!.image = UIImage(data: data!, scale:1)
//        }
        
        cell.nameLbl.text = self.relatedProductDetailsInfoArray.productsInfo[indexPath.item].Name as String
        if  let price = self.relatedProductDetailsInfoArray.productsInfo[indexPath.item].Price {
         cell.amountLbl.text = String(format:"%@",price)
        }
        return cell
    }
    
    
    // MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        if self.relatedProductDetailsInfoArray.productsInfo[indexPath.item].id != 0 {

            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.relatedProductDetailsInfoArray.productsInfo[indexPath.item].id
            self.navigationController!.pushViewController(productDetailsViewController, animated: true)
        }
        
        
    }
    
    func roundCell(cell:UIView) {
        
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
        cell.layer.shadowOpacity = 0.30
        cell.layer.shadowRadius = 1
        cell.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor
        
        
    }

    
    
    func viewControllerAtIndex(index: Int) -> PagerViewController
    {
         if ((self.pageImages.count == 0) || (index >= self.pageImages.count)) {
            
            return PagerViewController()
          }
        let vc: PagerViewController = PagerViewController(nibName: "PagerViewController", bundle: nil)
        vc.imageFile = self.pageImages[index] as! String
        vc.pageIndex       = index
        vc.productName  = (self.productDetailsArray[0].Name ?? "") as String

        
        return vc
    }
    
    // MARK: - UITextField Delegate

    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
        
        
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        textFieldString = String(format: "%@",textField.text!)
        print(textField)
        print(textFieldString)

        textField.resignFirstResponder();
        

        return true;
    }
    
    func textFieldDidChanged(textField:UITextField ){
        
        print(textField.tag)
        print(textField.text)
        self.textFieldDic[textField.tag] = textField.text!
        
        
    }
    
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        
        let vc = viewController as! PagerViewController
        var index = vc.pageIndex as Int
        
        if (index == 0 || index == NSNotFound)
        {
            return nil
            
        }
        
        index--
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! PagerViewController
        var index = vc.pageIndex as Int
        
        if (index == NSNotFound)
        {
            return nil
        }
        
        index++
        
        if (index == self.pageImages.count)
        {
            return nil
        }
        
        return self.viewControllerAtIndex(index)
        
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return self.pageImages.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return 0
    }
}
