//
//  LoginViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 10/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MFSideMenu
class LoginViewController: UIViewController {

    
    
    //let slideContainer.MFSideMenuContainerViewController()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBtnAct(sender: AnyObject) {
        
        //let slideContainer = MFSideMenuContainerViewController()
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let slideContainer = appDelegate.slideContainer
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let slideMenuViewC = SlideMenuViewController(nibName: "SlideMenuViewController", bundle: nil)
        let homeViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("ViewController") as! ViewController
        slideContainer.leftMenuViewController=slideMenuViewC;
        slideContainer.centerViewController=homeViewC;
        
//        let pageControl = UIPageControl.appearance()
//        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
//        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
//        pageControl.backgroundColor = UIColor.grayColor()

        
        self.navigationController!.pushViewController(slideContainer, animated: true)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
