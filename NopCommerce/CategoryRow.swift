//
//  CategoryRow.swift
//  TwoDirectionalScroller
//
//  Created by Robert Chen on 7/11/15.
//  Copyright (c) 2015 Thorn Technologies. All rights reserved.
//

import UIKit
import Alamofire
import MFSideMenu

class CategoryRow : UITableViewCell {
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    var request: Alamofire.Request?
    var collectionData: NSArray!
    var apiManagerClient = APIManagerClient()
    var homePageProductArray  = [GetHomePageProducts]()
    var homePageMenuFactureArray  = [HomePageMenuFacture]()
    var homePageCategoriesModels  = [HomePageCategoryModel]()

    var array = NSArray()
    var sectionIndex = NSInteger()
    
    
    func getCategoryArray(dataArray:NSArray,sectionIndex:NSInteger) -> Void{
        
        self.apiManagerClient = APIManagerClient.sharedInstance

       self.sectionIndex = sectionIndex
       self.array = dataArray
       self.contentCollectionView?.setContentOffset(CGPointZero, animated: true)
       self.contentCollectionView?.reloadData()
     }
    
   }

extension CategoryRow : UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if self.sectionIndex == 0 {
            return self.array.count
        
        } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
           return self.array.count
        } else {
            self.homePageCategoriesModels = self.array as! [HomePageCategoryModel]
            return self.homePageCategoriesModels[0].products.count
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AlbumCell", forIndexPath: indexPath) as! AlbumCell
        if self.sectionIndex==0 {
            
            self.homePageProductArray = self.array as! [GetHomePageProducts]
            
            let imageUrl = homePageProductArray[indexPath.item].ImageUrl
            
                cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
                cell.nameLbl.text = self.homePageProductArray[indexPath.item].Name
                cell.amountLbl.text = self.homePageProductArray[indexPath.item].Price
            
        } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            
            self.homePageMenuFactureArray = self.array as! [HomePageMenuFacture]
            
            let imageUrl = self.homePageMenuFactureArray[indexPath.item].imageUrl
            cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
            cell.nameLbl.text = self.homePageMenuFactureArray[indexPath.item].name
            cell.amountLbl.text = ""

            
            
        }else {
            
            self.homePageCategoriesModels = self.array as! [HomePageCategoryModel]
            let imageUrl = self.homePageCategoriesModels[0].products[indexPath.item].ImageUrl
            cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
            cell.nameLbl.text = self.homePageCategoriesModels[0].products[indexPath.item].Name
            cell.amountLbl.text = self.homePageCategoriesModels[0].products[indexPath.item].Price

        }
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
        cell.layer.shadowOpacity = 0.30
        cell.layer.shadowRadius = 1
        cell.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.layer.borderWidth = 0.2
        cell.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor

        
        return cell
    }
    
    
    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize{
            
            return CGSizeMake(collectionView.bounds.size.width/2 + 15, 256)
            
    }
    
    
}

extension CategoryRow : UICollectionViewDelegate{
    
    func  collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
     {
        
        print(self.sectionIndex)
        print(indexPath.row)
        print(indexPath.item)
        
         if self.sectionIndex==0 {
            
            self.homePageProductArray = self.array as! [GetHomePageProducts]
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.homePageProductArray[indexPath.item].Id
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.navController?.pushViewController(productDetailsViewController, animated: true)
            
         } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let aVariable = appDelegate.slideContainer
            
            self.homePageMenuFactureArray = self.array as! [HomePageMenuFacture]
            let catDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
            catDetailsViewController.categoryId  = self.homePageMenuFactureArray[indexPath.item].idd
            catDetailsViewController.categoryName = self.homePageMenuFactureArray[indexPath.item].name
            catDetailsViewController.manufacturerFlag = true
            
            
            appDelegate.slideContainer.centerViewController = catDetailsViewController
            let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
            aVariable.rightMenuViewController = rightMenuViewC;
    
         }
   
         else {
            
            self.homePageCategoriesModels = self.array as! [HomePageCategoryModel]
            
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = self.homePageCategoriesModels[0].products[indexPath.item].Id
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.navController?.pushViewController(productDetailsViewController, animated: true)

        }
        
     }
}


