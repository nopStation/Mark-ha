//
//  CustomLabel.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/13/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = 4.0
        self.layer.masksToBounds = true;
    }
    
    override func intrinsicContentSize() -> CGSize {
        let contentSize = super.intrinsicContentSize()
        return CGSizeMake(contentSize.width + 10, contentSize.height)
    }

}
