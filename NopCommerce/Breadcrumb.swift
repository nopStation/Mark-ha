//
//  Breadcrumb.swift
//  NopCommerce
//
//  Created by BS-125 on 11/20/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class Breadcrumb: NSObject {
    
    var CustomProperties :        NSDictionary?
    var idd  :                    NSInteger
    var IncludeInTopMenu :        NSInteger
    var Name :                    NSString
    var NumberOfProducts :        NSString?
    var SeName :                  NSString?
    var SubCategories :           NSArray

    init(JSONValue : AnyObject) {
        
        self.CustomProperties     = (JSONValue.valueForKeyPath("CustomProperties") as! NSDictionary)
        self.idd                  = (JSONValue.valueForKeyPath("Id") as! NSInteger)
        self.IncludeInTopMenu     = (JSONValue.valueForKeyPath("IncludeInTopMenu") as! NSInteger)
        self.Name                 = (JSONValue.valueForKeyPath("Name") as! NSString)
        self.NumberOfProducts     = (JSONValue.valueForKeyPath("NumberOfProducts") as? NSString)
        self.SeName               = (JSONValue.valueForKeyPath("SeName") as? NSString)
        self.SubCategories        = (JSONValue.valueForKeyPath("SubCategories") as! NSArray)
    }
    
}

/*
"Data" : [
{
"Id" : 1,
"Name" : "Computers",
"ParentCategoryId" : 0,
"DisplayOrder" : 1
},
{
"Id" : 2,
"Name" : "Desktops",
"ParentCategoryId" : 1,
"DisplayOrder" : 1
},
{
"Id" : 3,
"Name" : "Notebooks",
"ParentCategoryId" : 1,
"DisplayOrder" : 2
},
{
"Id" : 4,
"Name" : "Software",
"ParentCategoryId" : 1,
"DisplayOrder" : 3
},
{
"Id" : 5,
"Name" : "Electronics",
"ParentCategoryId" : 0,
"DisplayOrder" : 2
},
{
"Id" : 6,
"Name" : "Camera & photo",
"ParentCategoryId" : 5,
"DisplayOrder" : 1
},
{
"Id" : 7,
"Name" : "Cell phones",
"ParentCategoryId" : 5,
"DisplayOrder" : 2
},
{
"Id" : 8,
"Name" : "Others",
"ParentCategoryId" : 5,
"DisplayOrder" : 3
},
{
"Id" : 9,
"Name" : "Apparel",
"ParentCategoryId" : 0,
"DisplayOrder" : 3
},
{
"Id" : 10,
"Name" : "Shoes",
"ParentCategoryId" : 9,
"DisplayOrder" : 1
},
{
"Id" : 11,
"Name" : "Clothing",
"ParentCategoryId" : 9,
"DisplayOrder" : 2
},
{
"Id" : 12,
"Name" : "Accessories",
"ParentCategoryId" : 9,
"DisplayOrder" : 3
},
{
"Id" : 13,
"Name" : "Digital downloads",
"ParentCategoryId" : 0,
"DisplayOrder" : 4
},
{
"Id" : 14,
"Name" : "Books",
"ParentCategoryId" : 0,
"DisplayOrder" : 5
},
{
"Id" : 15,
"Name" : "Jewelry",
"ParentCategoryId" : 0,
"DisplayOrder" : 6
},
{
"Id" : 16,
"Name" : "Gift Cards",
"ParentCategoryId" : 0,
"DisplayOrder" : 7
}
],
"ErrorList" : null,
"StatusCode" : 200
}*/
