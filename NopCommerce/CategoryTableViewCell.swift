//
//  CategoryTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 10/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class CategoryTableViewCell: UITableViewCell {
    
    var request: Alamofire.Request?

    @IBOutlet weak var closureImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collapseButton: UIButton!
    
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var imageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabelLeadingConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
