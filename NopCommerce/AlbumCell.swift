//
//  AlbumCell.swift
//  UICollectionView+Swift
//
//  Created by Mobmaxime on 14/08/14.
//  Copyright (c) 2014 Jigar M. All rights reserved.
//

import UIKit
import Alamofire
class AlbumCell: UICollectionViewCell {
    
    var request: Alamofire.Request?

    @IBOutlet var AlbumImage : UIImageView?
    @IBOutlet var nameLbl: UILabel!
    
    @IBOutlet var amountLbl: UILabel!
}
