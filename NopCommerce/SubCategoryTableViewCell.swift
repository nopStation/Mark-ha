//
//  SubCategoryTableViewCell.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/8/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class SubCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var subCategoryButton: UIButton!
    
}
