//
//  APIManagerClient.swift
//  NopCommerce
//
//  Created by BS-125 on 11/5/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation

class APIManagerClient: NSObject {
    
    var homePageProductArray  = [GetHomePageProducts]()
    var homePageMenuFactureArray  = [HomePageMenuFacture]()
    
    var parentCategoriesArray = [GetAllCategories]()
    var leftMenuCategoriesArray = [GetAllCategories]()
    
    var homePageCategories = [HomePageCategories]()
    
    var categoryDetailsInfo = CategoryDetailsInfo()

    var categoryArray = NSMutableArray()
    var subCategoryArray = NSMutableArray()
  
    var shoppingCartCount = 0
    
    
    var AppStartUp = AppStartUpModel?()
    
    var baseURL = "https://www.mark-ha.com/api"
    var defaultBaseUrl = "https://www.mark-ha.com/api"
        
        class var sharedInstance: APIManagerClient {
            struct Static {
                static var onceToken: dispatch_once_t = 0
                static var instance: APIManagerClient? = nil
            }
            dispatch_once(&Static.onceToken) {
                Static.instance = APIManagerClient()
            }
            return Static.instance!
        }
    
    override init() {
        
    }
    
    func gethomePageProduct() -> [GetHomePageProducts] {
        return self.homePageProductArray
    }
}
