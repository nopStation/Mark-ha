//
//  APIManager.swift
//  NopCommerce
//
//  Created by BS-125 on 11/4/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import SwiftyJSON
import MBProgressHUD
import ObjectMapper
public class APIManager: NSObject {
    
    var homePageProducts = [GetHomePageProducts]()
    var homePageBanner   = [String]()
    var homePageMenuFacture = [HomePageMenuFacture]()
    var allCategories = [GetAllCategories]()
    var homePageCategoryModels = [HomePageCategoryModel]()
    var baseURL = APIManagerClient.sharedInstance.baseURL
    
    var featuredProductsAndCategory = [FeaturedProductsAndCategory]()
    
    let deviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
    let headers = ["DeviceId": UIDevice.currentDevice().identifierForVendor!.UUIDString]
    
    var categoryArray = [CategoryDetailsInfo]()
    var productsArray = [ProductDetailsInfo]()
    var shoppingCartArray = [ShoppingCartInfo]()
    

    
    func getHeaderDic() -> [String:String] {
        
        var headerDictionary = self.headers
        
        print(headerDictionary)

        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            headerDictionary = ["Token":token,"DeviceId": self.deviceId]
            print(token)
        }

        
        return headerDictionary
        
    }
    
    // MARK: ShoppingCartCount
    
    
    public func getShoppingCartCount(onSuccess successBlock: (Void -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        Alamofire.request(.GET, NSString(format: "%@/categories", baseURL) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                
                successBlock()
                
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
            
        }
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    
    
    // MARK: HomePageProducts

    public func loadHomePageProducts(onSuccess successBlock: ([GetHomePageProducts] -> Void)!, onError errorBlock: ((String!) -> Void)!) {

        
        
       
        
        Alamofire.request(.GET, NSString(format: "%@/homepageproducts", baseURL) as String , parameters:["thumbPictureSize":"320"], headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                 if let resData = json["Data"].array {
                    for data in resData{
                        var customProperties = data["CustomProperties"].string
                        let imageUrl = data["DefaultPictureModel"]["ImageUrl"].string
                        let idd = data["Id"].intValue
                        let name = data["Name"].string
                        let oldPrice = data["ProductPrice"]["OldPrice"].intValue
                        let price = data["ProductPrice"]["Price"].string
                        
                        print(price)
                        
                        let allowCustomerReviews = data["ReviewOverviewModel"]["AllowCustomerReviews"].boolValue
                        let productId = data["ReviewOverviewModel"]["ProductId"].intValue
                        let ratingSum = data["ReviewOverviewModel"]["RatingSum"].intValue
                        let totalReviews = data["ReviewOverviewModel"]["TotalReviews"].intValue
                        var shortDescription = data["ShortDescription"].string
                        if customProperties==nil{
                            customProperties = ""
                          }
                        
                        if shortDescription==nil{
                            shortDescription = ""
                        }
                        
                        let getHomePageProducts = GetHomePageProducts(CustomProperties: customProperties ?? "", ImageUrl: imageUrl ?? "", Id: idd, Name: name ?? "", OldPrice: oldPrice, Price: price ?? "", AllowCustomerReviews: allowCustomerReviews, ProductId: productId, RatingSum: ratingSum, TotalReviews: totalReviews, ShortDescription: shortDescription ?? "")
                        
                        self.homePageProducts.append(getHomePageProducts)
                    }
                    successBlock(self.homePageProducts)
                    
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
            
        }

    }
//------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
   
    
    
    // MARK: HomePageBanner
 
    
    public func loadHomePageBanner(onSuccess successBlock: ([String] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
       
        
        
        Alamofire.request(.GET, NSString(format: "%@/homepagebanner", baseURL) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if let resData = json["Data"].array {
                    for data in resData{
                        var imageUrl = data["ImageUrl"].string
                        print(imageUrl)
                        if imageUrl==nil{
                            imageUrl = ""
                        }
                        else{
                        self.homePageBanner.append(imageUrl!)
                        }
                    }
                    successBlock(self.homePageBanner)
                    
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
            
        }
        
    }
//------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------

    // MARK: HomePageMenuFacture
    
    public func loadHomePageMenuFacture(onSuccess successBlock: ([HomePageMenuFacture] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
       
        
        Alamofire.request(.GET, NSString(format: "%@/homepagemanufacture", baseURL) as String, parameters:["thumbPictureSize":"320"] , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if let resData = json["Data"].array {
                    for data in resData{
                        let name = data["Name"].string
                        let imageUrl = data["DefaultPictureModel"]["ImageUrl"].string
                        let idd = data["Id"].intValue
                        let homePageMenu = HomePageMenuFacture(name: name!, imageUrl: imageUrl!, idd: idd)
                        self.homePageMenuFacture.append(homePageMenu)
                    }
                    successBlock(self.homePageMenuFacture)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
//------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: AllCategories
 
    public func loadAllCategories(onSuccess successBlock: ([GetAllCategories] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
       
        Alamofire.request(.GET, NSString(format: "%@/categories", baseURL) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                
                if let resData = json["Data"].array {
                    for data in resData{
                        let name = data["Name"].string
                        let idd = data["Id"].intValue
                        let parentCategoryId = data["ParentCategoryId"].intValue
                        let displayOrder = data["DisplayOrder"].intValue
                        let iconPath = data["IconPath"].string

                        let allCatagories = GetAllCategories(parentCategoryId: parentCategoryId, name: name!, idd: idd, displayOrder: displayOrder, iconPath: iconPath!)
                        self.allCategories.append(allCatagories)
                    }
                    successBlock(self.allCategories)
                    
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
            
        }
        
    }
    
    
    
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: TestURL
    
    public func testUrl(url : String , onSuccess successBlock: (String -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        let testUrlString = String(format: "%@/api", url)
        
        Alamofire.request(.GET, NSString(format: "%@/categories", testUrlString) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200
                {
                    successBlock(testUrlString)
                }
                else
                {
                    let errorMessage = "Invalid URL"
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error.localizedDescription)")
                errorBlock("Request failed with error: \(error.localizedDescription)")
            }
            
        }
        
    }
    

    
    
    
    
    
    
    
//------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: HomePageCategories
    
    
    public func loadHomePageCategories(onSuccess successBlock: ([HomePageCategoryModel] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.GET, NSString(format: "%@/catalog/homepagecategorieswithproduct", baseURL) as String, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
                case .Success(let data):
                let json = JSON(data)
                print(json)
                
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    
                    self.homePageCategoryModels.removeAll()
                    if let resData = json["Data"].array {
                        for data in resData {
                            let homePageCategory = HomePageCategoryModel(JSON:data.dictionaryObject!)
                            print(homePageCategory)
                            self.homePageCategoryModels.append(homePageCategory)
                        }
                    }
                    
                    successBlock(self.homePageCategoryModels)
                    
                }
                else
                {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
                
                case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                }
            
        }
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    // MARK: LoadProductDetails
    public func loadProductDetails(productId:NSInteger,  onSuccess successBlock: ([ProductDetailsInfo] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.GET, NSString(format: "%@/productdetails/%d",baseURL, productId) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(data)
                
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    if let resData = json["Data"].dictionaryObject{
                        let categorisInfo = ProductDetailsInfo(JSON: resData as AnyObject)
                        self.productsArray.append(categorisInfo)
                        successBlock(self.productsArray)
                    }

                    
                }else
                {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }

    
    // MARK: AddProductToCartEdit
    public func AddProductToCartEdit(productId:NSInteger, shoppingCartTypeId : NSInteger,  onSuccess successBlock: ([ProductDetailsInfo] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        
        
        Alamofire.request(.GET,NSString(format: "%@/productdetails/%d?updatecartitemid=%d", baseURL, productId, shoppingCartTypeId) as String , headers:self.getHeaderDic()).responseJSON {

            response in

            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(data)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                  if let resData = json["Data"].dictionaryObject{
                    let categorisInfo = ProductDetailsInfo(JSON: resData as AnyObject)
                    self.productsArray.append(categorisInfo)
                    successBlock(self.productsArray)
                    }
                    
                }else
                {
                    let errorList = json["ErrorList"].arrayObject
                    
                    var errorMessage = ""
                    for error in errorList! {
                        
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                    
                    
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }



//------------------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    
    // MARK: FeaturedProductsAndCategory
    
  public  func loadFeaturedProductsAndCategory(categoryId: NSInteger, onSuccess successBlock: ([FeaturedProductsAndCategory] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
    
    
    
        Alamofire.request(.GET,NSString(format: "%@/categoryfeaturedproductandsubcategory/%d", baseURL,categoryId) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if let resData = json["SubCategories"].array {
                    for data in resData{
                        let name = data["Name"].string
                        let imageUrl = data["PictureModel"]["ImageUrl"].string
                        let idd = data["Id"].intValue
                        let featured_Product_and_category = FeaturedProductsAndCategory(name: name!, imageUrl: imageUrl!, idd: idd)
                        self.featuredProductsAndCategory.append(featured_Product_and_category)
                    }
                    successBlock(self.featuredProductsAndCategory)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------


    
    // MARK: CategoryDetails
    
    
    public  func loadCategoryDetails( manufacturerFlag: Bool, categoryId: NSInteger, params: [String: AnyObject], onSuccess successBlock: (CategoryDetailsInfo -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        var urlSubString = "Category"
        if manufacturerFlag == true
        {
            urlSubString = "Manufacturer"
        }
        
        Alamofire.request(.GET,NSString(format: "%@/%@/%d", baseURL, urlSubString, categoryId) as String, parameters:params , headers:self.getHeaderDic() ).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let categoryDetailsInfo = CategoryDetailsInfo ()
                
                
                categoryDetailsInfo.fromPrice = json["PriceRange"]["From"].double
                categoryDetailsInfo.toPrice = json["PriceRange"]["To"].double
                
                
                
                categoryDetailsInfo.TotalPages = json["TotalPages"].int!
                
                if let resData = json["Products"].arrayObject {
                    for productDictionary in resData {
                
                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject])
                        categoryDetailsInfo.productsInfo.append(products)
                    }
                    
                }
                
                print(categoryDetailsInfo.productsInfo.count)
                
                if let resData = json["NotFilteredItems"].arrayObject {
                    for notFilteredItemDictionary in resData {
                        
                        let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as! [String : AnyObject])
                        categoryDetailsInfo.notFilteredItems.append(notFilteredItem)
                    }
                }
                
                if let resData = json["AlreadyFilteredItems"].arrayObject {
                    for alreadyFilteredItemDictionary in resData {
                        
                        let alreadyFilteredItem = NotFilteredItems(JSON: alreadyFilteredItemDictionary as! [String : AnyObject])
                        categoryDetailsInfo.alreadyFilteredItems.append(alreadyFilteredItem)
                    }
                }
                

                    
                if let resData = json["AvailableSortOptions"].arrayObject {
                    for availableSortOptionDictionary in resData {
                            
                        let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as! [String : AnyObject])
                        categoryDetailsInfo.availableSortOptions.append(availableSortOptionItem)
                    }
                }

                successBlock(categoryDetailsInfo)

        
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
        }
//    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: LoadRelatedProduct
    
    public  func loadRelatedProduct( productId: NSInteger, onSuccess successBlock: (CategoryDetailsInfo -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
       
        
        
        Alamofire.request(.GET,NSString(format: "%@/relatedproducts/%d", baseURL, productId) as String, parameters:["thumbPictureSize":"320"] , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let categoryDetailsInfo = CategoryDetailsInfo ()

                
                if let resData = json["Data"].arrayObject {
                    for productDictionary in resData {
                        
                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject])
                        categoryDetailsInfo.productsInfo.append(products)
                    }
 
                }

                if let resData = json["NotFilteredItems"].arrayObject {
                    for notFilteredItemDictionary in resData {
                        
                        let notFilteredItem = NotFilteredItems(JSON: notFilteredItemDictionary as! [String : AnyObject])
                        categoryDetailsInfo.notFilteredItems.append(notFilteredItem)
                    }
                }
                
                
                if let resData = json["AvailableSortOptions"].arrayObject {
                    for availableSortOptionDictionary in resData {
                        
                        let availableSortOptionItem = AvailableSortOptions(JSON: availableSortOptionDictionary as! [String : AnyObject])
                        categoryDetailsInfo.availableSortOptions.append(availableSortOptionItem)
                    }
                }
                successBlock(categoryDetailsInfo)


                
                
                
                
                
//                if let resData = json["Data"].array {
//                    for data in resData{
//                        
//                        let ProductId = data["ReviewOverviewModel"]["ProductId"].intValue
//                        print(productId)
//                        let TotalReviews = data["ReviewOverviewModel"]["TotalReviews"].intValue
//                        let RatingSum = data["ReviewOverviewModel"]["RatingSum"].intValue
//                        let AllowCustomerReviews = data["ReviewOverviewModel"]["AllowCustomerReviews"].boolValue
//                        let idd = data["Id"].intValue
//                        //print(idd)
//                        
//                        var CustomProperties = data["CustomProperties"].string
//                        let ImageUrl = data["DefaultPictureModel"]["ImageUrl"].string
//                        
//                        let Name = data["Name"].string
//                        let ShortDescription = data["ShortDescription"].string
//                        var OldPrice = data["ProductPrice"]["OldPrice"].string
//                        
//                        if OldPrice==nil{
//                            OldPrice = ""
//                        }
//                        let Price = data["ProductPrice"]["Price"].string
//                        
//                        if CustomProperties==nil{
//                            CustomProperties = ""
//                        }
//                        
//                        let products = CategoryDetailsInfo(ProductId: ProductId, TotalReviews: TotalReviews, RatingSum: RatingSum, AllowCustomerReviews: AllowCustomerReviews, idd: idd, CustomProperties: CustomProperties!, ImageUrl: ImageUrl!, name: Name!, ShortDescription:ShortDescription!, OldPrice: OldPrice!, Price: Price!)
//                        
//                        self.categoryArray.append(products)
//                    }
//                    successBlock(self.categoryArray)
//                }
//                
//                successBlock(self.categoryArray)
 
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: ProductDetailsPagePrice
    
    public  func loadProductDetailsPagePrice( productId: NSInteger, parameters: NSMutableArray , onSuccess successBlock: (AnyObject -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ProductDetailsPagePrice/%d", baseURL, productId) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }

        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .Success(let data):
               // let json = JSON(data)
                //let price = json["Price"] as String
                //print(json)
                successBlock(data)

            case .Failure( let error):
            print("Request failed with error: \(error)")
            errorBlock("Request failed with error: \(error)")

            }
        }
        

    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    // MARK: AddProductToCart
    
    public  func loadAddProductToCart( cartTypeId: NSInteger, productId: NSInteger, parameters: NSMutableArray , onSuccess successBlock: (AnyObject -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/AddProductToCart/%d/%d", baseURL, productId, cartTypeId) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .Success(let data):
                 let json = JSON(data)
                 
                 let statusCode = json["StatusCode"].int
                 if statusCode == 200 {
                    
                    if cartTypeId == 1 {
                        APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    }
                    
                    successBlock(data)
                    
                 } else {
                 
                    let errorList = json["ErrorList"].arrayObject
                    
                    var errorMessage = ""
                    for error in errorList! {
                        
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
  
                    
                }

                
            case .Failure( let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
                //MBProgressHUD.show
                
            }
        }
        
        
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------



    
    
    // MARK: LoadShoppingCart
    
    public  func loadShoppingCart( onSuccess successBlock: ([ShoppingCartInfo] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ShoppingCart", baseURL) as String)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        
        
        
        //request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if let resData = json.dictionaryObject{
                    
                    print(resData)
                    let shoppingInfo = ShoppingCartInfo(JSON: resData as AnyObject)
                    self.shoppingCartArray.append(shoppingInfo)
                    successBlock(self.shoppingCartArray)
                }
                
            case .Failure( let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
                
            }
        }
        
        
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: LoadWishList
    
    public func loadWishList( onSuccess successBlock: (([WishListItems]) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
       
        
        Alamofire.request(.GET,NSString(format: "%@/shoppingCart/wishlist", baseURL) as String, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    
                    var wishListItems : [WishListItems] = []
                    if let resData = json["Items"].arrayObject {
                        
                        for item in resData {
                            let wishListItem = WishListItems(JSON: item)
                            wishListItems.append(wishListItem)
                        }
                        
                    }
                    successBlock(wishListItems)
                    
                } else {
                    errorBlock("Wishlist couldn't be loaded, try again")
                }
                
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    // MARK: BillingAddress
    
    public func loadBillingAddressForm( onSuccess successBlock: (BillingAddressModel -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
       
        Alamofire.request(.GET,NSString(format: "%@/checkout/billingform", baseURL) as String, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    let newBillingAddress = GenericBillingAddress(address: json["NewAddress"].dictionaryObject!)
                    
                    var existingBillingAddresses = [GenericBillingAddress]()
                    for address in json["ExistingAddresses"].arrayObject! {
                        
                        let existingBillingAddress = GenericBillingAddress(address: address as! [String : AnyObject])
                        existingBillingAddresses.append(existingBillingAddress)
                    }
                    
                    let billingAddress = BillingAddressModel()
                    billingAddress.newBillingAddress = newBillingAddress
                    billingAddress.existingBillingAddresses = existingBillingAddresses
                    
                    successBlock(billingAddress)
                } else {
                    errorBlock("Data couldn't be loaded, try again")
                }
                
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    // MARK: GetBillingStates
    
    public func getBillingStates(countryId:NSInteger,  onSuccess successBlock: ([BillingState] -> Void)!, onError errorBlock: ((String!) -> Void)!) {

        Alamofire.request(.GET, NSString(format: "%@/country/getstatesbycountryid/%d",baseURL, countryId) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var states = [BillingState]()
                    if let resData = json["Data"].arrayObject {
                        for stateDictionary in resData {
                            let stateName = stateDictionary["name"] as! NSString
                            let stateId = stateDictionary["id"] as! NSInteger
                            
                            let state = BillingState(stateName: stateName , stateId: String(stateId))
                            states.append(state)
                        }
                    }
                    
                    successBlock(states)
                } else {
                    errorBlock("Data couldn't be loaded, try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    // MARK: saveBillingOrShippingAddress
    public func saveBillingOrShippingAddress(address:NSInteger, from existingAddress:NSInteger,  onSuccess successBlock: (Void -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        Alamofire.request(.POST, NSString(format: "%@/checkout/checkoutsaveadressid/%d", baseURL, address) as String, parameters:["value":existingAddress] , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
                    var errorMessage = "Billing Address"
                    if address == 2 {
                        errorMessage = "Shipping Address"
                    }
                    errorMessage += "couldn't be saved, try again."
                    
                    errorBlock(errorMessage)
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    
    
    public func saveBillingOrShippingAddress(address:NSInteger, parameters: [AnyObject],  onSuccess successBlock: (Void -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/checkout/checkoutsaveadress/%d", baseURL, address) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in  
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    successBlock()
                } else {
//                    var errorMessage = ""
//                    if errors = json["ErrorList"] {
//                        errorMessage = errors[0]
//                    }
                    //errorMessage += "couldn't be saved, try again."
                   errorBlock ("\(json["ErrorList"][0])")
                }
                
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    
    
    
    
    // MARK: GetShippingMethod
    
    public func getShippingMethod( onSuccess successBlock: ([ShippingMethod] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.GET, NSString(format: "%@/checkout/checkoutgetshippingmethods",baseURL) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var shippingMethod = [ShippingMethod]()
                    if let resData = json["ShippingMethods"].arrayObject {
                        for stateDictionary in resData {
                            
                            let state = ShippingMethod(JSON: stateDictionary as! [String : AnyObject])
                            shippingMethod.append(state)
                        }
                        
                        successBlock(shippingMethod)
                    }
                } else {
                    errorBlock("Data couldn't be loaded, try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    // MARK: GetPaymentMethod
    
    public func getPaymentMethod( onSuccess successBlock: ([PaymentMethod] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.GET, NSString(format: "%@/checkout/checkoutgetpaymentmethod",baseURL) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var paymentMethod = [PaymentMethod]()
                    if let resData = json["PaymentMethods"].arrayObject {
                        for stateDictionary in resData {
                            
                            let state = PaymentMethod(JSON: stateDictionary as! [String : AnyObject])
                            paymentMethod.append(state)
                        }
                        
                        successBlock(paymentMethod)
                    }
                } else {
                    errorBlock("Data couldn't be loaded, try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    
    //MARK: SetShippingMethods
    
    public  func setShippingMethods(couponText : NSString,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.POST,NSString(format: "%@/checkout/checkoutsetshippingmethod", baseURL) as String ,parameters:["value":couponText] ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    //let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock()
                }
                else {
                    
                    errorBlock("The coupon code you entered couldn't be applied to your order")
                    
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    //MARK: SetPaymentMethods
    
    public  func setPaymentMethods(couponText : NSString,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        Alamofire.request(.POST,NSString(format: "%@/checkout/checkoutsavepaymentmethod", baseURL) as String ,parameters:["value":couponText] ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    //let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock()
                }
                else {
                    
                    errorBlock("The coupon code you entered couldn't be applied to your order")
                    
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    
    // MARK: loadCheckOutOrderInformation
    
    public func loadCheckOutOrderInformation( onSuccess successBlock: (CheckOutOrderInfo -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        
        Alamofire.request(.GET, NSString(format: "%@/shoppingcart/checkoutorderinformation",baseURL) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                  if let resData = json.dictionaryObject{
                        
                        print(resData)
                        let orderInfo = CheckOutOrderInfo(JSON: resData as AnyObject)
                    
                        successBlock(orderInfo)
                    }
                    
                } else {
                    errorBlock("Data couldn't be loaded, try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    // MARK: CheckOutComplete
    
    public func checkOutComplete( onSuccess successBlock: ((JSON ) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
 
        
        Alamofire.request(.GET, NSString(format: "%@/checkout/checkoutcomplete",baseURL) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(data)
                
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                  successBlock(json)
                    
                } else {
                    errorBlock("Checkout failed, try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    
    
    // MARK: LoadTotalOrder
    
    public  func loadTotalOrder(onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        
        
        Alamofire.request(.GET,NSString(format: "%@/ShoppingCart/OrderTotal", baseURL) as String , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                 
                    let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                   
                    successBlock(totalOrder)
                }
                else {
                    
                    errorBlock("Data Couldn't be loaded,Try again")
                    
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            
        }
      }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    
    // MARK: RemoveGiftCard
    
    public  func RemoveGiftCard(ID : NSInteger ,  onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
     
        
        Alamofire.request(.POST,NSString(format: "%@/ShoppingCart/RemoveGiftCard", baseURL) as String , parameters:["value":ID ] , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock(totalOrder)
                }
                else {
                    
                    errorBlock("Data Couldn't be loaded,Try again")
                    
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    //MARK: DeleteAndUpdateCart
    
    public  func DeleteAndUpdate(parameters : NSMutableArray,  onSuccess successBlock: (([ShoppingCartInfo]) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ShoppingCart/UpdateCart", baseURL) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in

            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    self.shoppingCartArray.removeAll()

                    if let resData = json.dictionaryObject{
                        print(resData)
                        let shoppingInfo = ShoppingCartInfo(JSON: resData as AnyObject)
                        self.shoppingCartArray.append(shoppingInfo)
                        successBlock(self.shoppingCartArray)
                    }
                }
                else {
                    
                    errorBlock("Data Couldn't be loaded,Try again")
                    
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    //MARK: DeleteAndUpdateWishList
    
    public func deleteAndUpdateWishList(parameters : NSMutableArray, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ShoppingCart/UpdateWishlist", baseURL) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in
            
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    successBlock()
                }
                else {
                    
                    errorBlock("Wishlist item couldn't be deleted,try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    //MARK: AddProductFromWishlistToShoppingCart
    
    public func addProductFromWishlistToShoppingCart(parameters : NSMutableArray, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ShoppingCart/AddItemsToCartFromWishlist", baseURL) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in
            
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    APIManagerClient.sharedInstance.shoppingCartCount = json["Count"].intValue
                    
                    successBlock()
                }
                else {
                    
                    errorBlock("Wishlist items couldn't be added to cart,try again")
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    //MARK: ApplyDiscountCoupon
    
    public  func applyDiscountCoupon(couponText : NSString,  onSuccess successBlock: ((TotalOrder) -> Void)!, onError errorBlock: ((String!) -> Void)!) {

        
        
        Alamofire.request(.POST,NSString(format: "%@/ShoppingCart/ApplyDiscountCoupon", baseURL) as String ,parameters:["value":couponText] ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                 let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock(totalOrder)
                }

                else{
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                        
                        
                    }

            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    //MARK: RemoveDiscountCoupon
    
    public  func RemoveDiscountCoupon(  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.GET,NSString(format: "%@/ShoppingCart/RemoveDiscountCoupon", baseURL) as String ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    //let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock()
                }
                    
                    else{
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    //MARK: loadProceedToCheckout
    
    public  func loadProceedToCheckout(parameters : NSMutableArray,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/ShoppingCart/applycheckoutattribute", baseURL) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON { response in

            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    successBlock()
                }
                else
                {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                    
                    
                }

            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    //MARK: loadOrders
    
    public  func loadOrders(onSuccess successBlock: (([OrderInfo]) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
           Alamofire.request(.GET,NSString(format: "%@/order/customerorders", baseURL) as String,
            headers:self.getHeaderDic() ).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    var orderInfo = [OrderInfo]()
                    if let resData = json["Orders"].arrayObject {
                        for stateDictionary in resData {
                            
                            let order = OrderInfo(JSON: stateDictionary as! [String : AnyObject])
                            orderInfo.append(order)
                        }
                        
                        successBlock(orderInfo)
                    }
                } else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    //MARK: getOrderDetails
    
    public  func getOrderDetails(orderId:NSInteger, onSuccess successBlock: ((OrderDetailsInfo) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        

        
        Alamofire.request(.GET,NSString(format: "%@/order/details/%d", baseURL, orderId) as String,
            headers: self.getHeaderDic()).responseJSON {
                response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    print(json)
                    let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                       
                        let orderdetailsInfo = OrderDetailsInfo()
                          let  billingAddress = GenericBillingAddress(address: json["BillingAddress"].dictionaryObject!)
                          let  shippingAddress = GenericBillingAddress(address: json["ShippingAddress"].dictionaryObject!)

                          orderdetailsInfo.billingAddress = billingAddress
                          orderdetailsInfo.shippingAddress = shippingAddress
                          orderdetailsInfo.PaymentMethod = json["PaymentMethod"].string
                          orderdetailsInfo.PaymentMethodStatus = json["PaymentMethodStatus"].string
                          orderdetailsInfo.ShippingMethod = json["ShippingMethod"].string
                          orderdetailsInfo.ShippingStatus = json["ShippingStatus"].string
                        
                        orderdetailsInfo.OrderSubtotal = json["OrderSubtotal"].string
                        orderdetailsInfo.OrderTotal = json["OrderTotal"].string
                        orderdetailsInfo.OrderSubTotalDiscount = json["OrderSubTotalDiscount"].string
                        orderdetailsInfo.Tax = json["Tax"].string
                        orderdetailsInfo.OrderShipping = json["OrderShipping"].string
                        orderdetailsInfo.CheckoutAttributeInfo = json["CheckoutAttributeInfo"].string

                        
                        for address in json["Items"].arrayObject! {
                            
                            let items = CheckOutItems(JSON: address)
                            orderdetailsInfo.items.append(items)
                        }
                        
                        
                          successBlock(orderdetailsInfo)
                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    
    
    
    //MARK: Register
    
    public func registerCustomer(params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String!) -> Void)!) {
        
        
        
        Alamofire.request(.POST, NSString(format: "%@/customer/register", baseURL) as String ,parameters:params, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    //MARK: SignIn
    
    public func signInCustomer(params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String!) -> Void)!) {
        
        
        
        
        Alamofire.request(.POST, NSString(format: "%@/login", baseURL) as String ,parameters:params, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    let token = json["Token"].string
                    
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(token, forKey: "accessToken")
                    defaults.synchronize()
                    
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    //MARK: Customer Info
    
    public func loadCustomerInfo(onSuccess successBlock:(([String: AnyObject]) -> Void)!, onError errorBlock:((String!) -> Void)!) {
    
        
        Alamofire.request(.GET, NSString(format:"%@/customer/info", baseURL) as String, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                if json["StatusCode"].int == 200 {
                    
                    successBlock(json.dictionaryObject!)
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    
    public func saveCustomerInfo(params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String!) -> Void)!) {
        
        
        
        Alamofire.request(.POST, NSString(format:"%@/customer/info", baseURL) as String ,parameters:params, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    
    //MARK: Address
    
    public  func getAddress(onSuccess successBlock: (([GenericBillingAddress]) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
             Alamofire.request(.GET,NSString(format: "%@/customer/addresses", baseURL) as String,
            headers:self.getHeaderDic() ).responseJSON {
                response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    print(json)
                    let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                      
                      var existingAddress = [GenericBillingAddress]()
                      for address in json["ExistingAddresses"].arrayObject! {
                            
                        let  existingAdd = GenericBillingAddress(address: address as! [String : AnyObject])
                            existingAddress.append(existingAdd)
                        }
                        
                        
                        successBlock(existingAddress)

                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    
    //MARK: DeleteAddress
    
    public  func deleteAddress(addressId:NSInteger, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
    
        
        Alamofire.request(.GET,NSString(format: "%@/customer/address/remove/%d", baseURL, addressId) as String,
            headers: self.getHeaderDic()).responseJSON {
                response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    print(data)
                    let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                        
                        
                        
                        successBlock()
                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    //MARK: GetCustomerInfo
    
    public  func getCustomerInfo(addressId:NSInteger, onSuccess successBlock: ((GenericBillingAddress) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
    
        
        Alamofire.request(.GET,NSString(format: "%@/customer/address/edit/%d", baseURL, addressId) as String,
            headers: self.getHeaderDic()).responseJSON {
                response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    print(json)
                    let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                        
                        let  address = GenericBillingAddress(address: json["Address"].dictionaryObject!)
   
                        successBlock(address)
                        
                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------

    
    //MARK: Saved Edite Info
    
    public  func savedAddress(addressId:NSInteger, parameters: [AnyObject], onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {

        
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/customer/address/edit/%d", baseURL, addressId) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }


        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                   let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                        
                       // let  address = GenericBillingAddress(address: json["Address"].dictionaryObject!)
                        
                        successBlock()
                        
                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------


    //MARK: Add New Address
    
    public  func addNewAddress(parameters: [AnyObject], onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/customer/address/add", baseURL) as String)!)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
          request.setValue(token, forHTTPHeaderField: "Token")
          request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
          }
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(parameters, options: [])
        
        Alamofire.request(request).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                let statusCode = json["StatusCode"].int
                if statusCode == 200 {
                    
                    // let  address = GenericBillingAddress(address: json["Address"].dictionaryObject!)
                    
                    successBlock()
                    
                } else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    for error in errorList! {
                        errorMessage += "\(error as! String)\n"
                    }
                    errorBlock(errorMessage)
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
   
    
    //MARK: Paypal Confirm
    
    public  func paypalConfirm(PaymentId : NSString, orderId: NSInteger ,  onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        

        
        Alamofire.request(.POST,NSString(format: "%@/checkout/checkpaypalaccount", baseURL) as String ,parameters:["PaymentId":PaymentId,"OrderId":orderId] ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    //let totalOrder = TotalOrder(JSON: json.dictionaryObject!)
                    
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    if errorList != nil {
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        
                        errorBlock(errorMessage)
                        
                    }else {
                        
                        errorBlock("Payment Failed.Please try again")
                        
                        
                    }
                    
                   // {
                   //     "Message" : "Invalid HTTP response The request was aborted: Could not create SSL\/TLS secure channel."
                   // }
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    
    
    //MARK: Authorize.Net Confirm

    
    public func getAuthorizeDotNet(params:[String: AnyObject], onSuccess successBlock:(() -> Void)!, onError errorBlock:((String!) -> Void)!) {
        

        
        
        Alamofire.request(.POST, NSString(format:"%@/checkout/checkauthorizepayment", baseURL) as String ,parameters:params, headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    if errorList != nil {
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        
                        errorBlock(errorMessage)

                    }else {
                        
                        errorBlock("Payment Failed.Please try again")

                        
                    }
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }

    }
    
    
    
    //MARK: Redirect Payment Confirm
    
    public func getRedirectPayment(orderId: NSInteger, onSuccess successBlock:(() -> Void)!, onError errorBlock:((String!) -> Void)!)
    {
        var headerDictionary = self.headers
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            headerDictionary = ["Token":token,"DeviceId": "1213w213"]
            print(token)
        }
        
        Alamofire.request(.POST, NSString(format:"%@/checkout/checkredirectpayment", baseURL) as String ,parameters:["OrderId": orderId], headers:headerDictionary).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                    successBlock()
                }
                else {
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    if errorList != nil {
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                        
                    } else {
                        
                        errorBlock("Checkout Failed.Please try again")
                    }
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }

    
    
    
    // MARK: GetSearchResults
    public func getSearchResults(searchText: NSString, onSuccess successBlock: ([ProductsInfo] -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        Alamofire.request(.POST, NSString(format: "%@/catalog/search", baseURL) as String, parameters:["q":searchText] , headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                
                var productList = [ProductsInfo]()
                if let resData = json["Products"].arrayObject {
                    for productDictionary in resData {
                        
                        let products = ProductsInfo(JSON: productDictionary as! [String : AnyObject])
                        productList.append(products)
                    }
                    
                }
                

                
                successBlock(productList)

//                let statusCode = json["StatusCode"].int
//                if statusCode == 200 {
//                    successBlock()
//                } else{
//                    let errorList = json["ErrorList"].arrayObject
//                    var errorMessage = ""
//                    for error in errorList! {
//                        errorMessage += "\(error as! String)\n"
//                    }
//                    errorBlock(errorMessage)
//                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
            }
        }
    }
    

    //MARK: CheckOutForGuest
    
    public  func checkOutForGuest(onSuccess successBlock: ((Bool) -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        Alamofire.request(.GET,NSString(format: "%@/checkout/opccheckoutforguest", baseURL) as String,
            headers:self.getHeaderDic() ).responseJSON {
                response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    print(json)
                    let statusCode = json["StatusCode"].int
                    if statusCode == 200 {
                        
                        if json["Data"].int == 1 {
                        successBlock(true)
                        }
                        else {
                            successBlock(false)
  
                            
                        }
                        
                    } else {
                        let errorList = json["ErrorList"].arrayObject
                        var errorMessage = ""
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                    }
                case .Failure(let error):
                    print("Request failed with error: \(error)")
                    errorBlock("Request failed with error: \(error)")
                }
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------------------------------------------------------
    

    //MARK: RegisterDeviceId
    
    public  func registerDeviceId(deviceId: String, onSuccess successBlock: (() -> Void)!, onError errorBlock: ((String!) -> Void)!) {
        
        
        
        Alamofire.request(.POST,NSString(format: "%@/appstart", baseURL) as String ,parameters:["DeviceTypeId":"5","SubscriptionId":deviceId,"EmailAddress":"mubassher@brainstation-23.com"] ,  headers:self.getHeaderDic()).responseJSON {
            response in
            switch response.result {
            case .Success(let data):
                let json = JSON(data)
                print(json)
                if json["StatusCode"].int == 200 {
                    
                 if let dic = Mapper<AppStartUpModel>().map(json["Data"].dictionaryObject){
                   
                    APIManagerClient.sharedInstance.AppStartUp = dic
                    print(APIManagerClient.sharedInstance.AppStartUp?.AppDescription)
                    print(APIManagerClient.sharedInstance.AppStartUp?.HeaderBackgroundColor)
                    //successBlock()
                  }
                }
                else {
                    
                    let errorList = json["ErrorList"].arrayObject
                    var errorMessage = ""
                    if errorList != nil{
                        for error in errorList! {
                            errorMessage += "\(error as! String)\n"
                        }
                        errorBlock(errorMessage)
                        
                    }
                }
            case .Failure(let error):
                print("Request failed with error: \(error)")
                errorBlock("Request failed with error: \(error)")
                
            }
        }
    }
   
    
}
