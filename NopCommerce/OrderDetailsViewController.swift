//
//  OrderDetailsViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/28/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import DropDown

class OrderDetailsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    var aPIManager : APIManager?
    var rightMenu : GlobalRightMenu?

    var orderInfo = OrderInfo! ()

    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var attributsLbl: UILabel!
    @IBOutlet weak var contentTableViewHeightConstant: NSLayoutConstraint!
    
    @IBOutlet weak var orderNumberLbl: UILabel!
    @IBOutlet weak var orderTotalLbl: UILabel!
    @IBOutlet weak var orderStatusLbl: UILabel!
    @IBOutlet weak var orderDateLbl: UILabel!
    
    @IBOutlet weak var billingAddressLbl: UILabel!
    @IBOutlet weak var shippingAddressLbl: UILabel!
    @IBOutlet weak var shippingMethod: UILabel!

    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var billingMethod: UILabel!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    
    @IBOutlet weak var orderView: UIView!
    
    @IBOutlet weak var billingAddressLblView: UIView!
    @IBOutlet weak var shippingAddressLblView: UIView!
    @IBOutlet weak var billingLblView: UIView!
    @IBOutlet weak var shippingLblView: UIView!
    @IBOutlet weak var attributsLblView: UIView!
    
    @IBOutlet weak var attributesLblViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributesLblBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var attributesLblTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    var flag = Bool ()

    var orderDetailsInfo = OrderDetailsInfo()

    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
        
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.contentScrollView.layoutIfNeeded()
        
        self.orderView.layer.cornerRadius = 4.0
        self.billingAddressLblView.layer.cornerRadius = 4.0
        self.shippingAddressLblView.layer.cornerRadius = 4.0
        self.billingLblView.layer.cornerRadius = 4.0
        self.shippingLblView.layer.cornerRadius = 4.0
        self.attributsLblView.layer.cornerRadius = 4.0
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)

        self.contentScrollView.hidden = true
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        self.aPIManager = APIManager()
        
        self.aPIManager!.getOrderDetails(self.orderInfo.Id, onSuccess: {
            gotOrderDetails in
            
            self.orderDetailsInfo = gotOrderDetails
            
            self.contentTableViewHeightConstant.constant = CGFloat(self.orderDetailsInfo.items.count) * 150.0

            
            let billingAddress = "\(self.orderDetailsInfo.billingAddress!.firstName ?? "")\(self.orderDetailsInfo.billingAddress!.lastName ?? "")\n\(self.orderDetailsInfo.billingAddress!.email ?? "")\n\(self.orderDetailsInfo.billingAddress!.phoneNumber ?? "")\n\(self.orderDetailsInfo.billingAddress!.address1 ?? ""),\(self.orderDetailsInfo.billingAddress!.cityName ?? "")\n\(self.orderDetailsInfo.billingAddress!.countryName ?? "")"
            self.billingAddressLbl.text = billingAddress
            
            
            let shippingAddress = "\(self.orderDetailsInfo.shippingAddress!.firstName ?? "")\(self.orderDetailsInfo.shippingAddress!.lastName ?? "")\n\(self.orderDetailsInfo.shippingAddress!.email ?? "")\n\(self.orderDetailsInfo.shippingAddress!.phoneNumber ?? "")\n\(self.orderDetailsInfo.shippingAddress!.address1 ?? ""),\(self.orderDetailsInfo.shippingAddress!.cityName ?? "")\n\(self.orderDetailsInfo.shippingAddress!.countryName ?? "")"
            
            
            print(shippingAddress)
            self.shippingAddressLbl.text = shippingAddress
            
            
            
            
            self.orderDateLbl.text    =  NSString(format: "Order Date : %@",self.orderInfo.OrderStatus) as String
            self.orderStatusLbl.text  =  NSString(format: "Order Status : %@",self.orderInfo.OrderStatus) as String
            self.orderTotalLbl.text   =  NSString(format: "Order Total : %@",self.orderInfo.OrderTotal) as String
            self.orderNumberLbl.text  =  NSString(format: "ORDER # %d",self.orderInfo.Id) as String
            
            
            let billingMethod = String(format: "Payment Method : %@\nPayment Status : %@", self.orderDetailsInfo.PaymentMethod ?? "", self.orderDetailsInfo.PaymentMethodStatus ?? "")
            self.billingMethod.text = billingMethod
            
            
            let shippingMethod = String(format: "Shipping Method : %@\nShipping Status : %@", self.orderDetailsInfo.ShippingMethod ?? "", self.orderDetailsInfo.ShippingStatus ?? "")
            self.shippingMethod.text = shippingMethod

            
            if let subTotal = self.orderDetailsInfo.OrderSubtotal {
                self.subTotalLbl.text = subTotal
            }
            
            if let shipping = self.orderDetailsInfo.OrderShipping {
                self.shippingLbl.text = shipping
            }
            
            if let  total = self.orderDetailsInfo.OrderTotal {
                self.totalLbl.text = total
            }
            
            if let tax = self.orderDetailsInfo.Tax {
                self.taxLbl.text = tax
            }
            
            let attributsArray:[String] = self.orderDetailsInfo.CheckoutAttributeInfo!.componentsSeparatedByString("<br />")
            
            var attributsLabelText = ""

            for attributs in attributsArray {
                 attributsLabelText += String(format: "%@\n",attributs)
              }
            
            attributsLabelText = attributsLabelText.substringToIndex(attributsLabelText.endIndex.predecessor())
            
            self.attributsLbl.text = attributsLabelText
            
            if attributsLabelText.characters.count == 0 {
                self.attributesLblViewTopConstraint.constant = 0
                self.attributesLblBottomConstraint.constant = 0
                self.attributesLblTopConstraint.constant = 0
                self.attributsLblView.hidden = true
            }
            
            self.contentTableView.reloadData()

            self.contentScrollView.hidden = false
            self.hideLoading()
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
        })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
    }
    
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
          }

    // MARK: - Table view data source
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.contentTableView {
                return self.orderDetailsInfo.items.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if tableView == contentTableView {
            
            var cell: ConfirmOrderTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "ConfirmOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmOrderTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            }
            
            cell.itemImageView.layer.borderWidth = 1.0
            cell.itemImageView.layer.masksToBounds = false
            cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
            cell.itemImageView.layer.cornerRadius = 13
            cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
            cell.itemImageView.clipsToBounds = true
            
            let imageUrl = self.orderDetailsInfo.items[indexPath.row].ImageUrl
            cell.itemImageView!.sd_setImageWithURL(NSURL(string: imageUrl!))
            cell?.selectionStyle = UITableViewCellSelectionStyle.None
            cell.titleLbl.text = self.orderDetailsInfo.items[indexPath.row].ProductName as String
            cell.UnitPrice.text = self.orderDetailsInfo.items[indexPath.row].UnitPrice as String
            cell.subTotal.text = self.orderDetailsInfo.items[indexPath.row].SubTotal as String
            cell.quantity.text = String(format: "Quantity:%d", self.orderDetailsInfo.items[indexPath.row].Quantity)

            if (indexPath.row == self.orderDetailsInfo.items.count-1) {
                cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            }

            
            return cell
        }
        
        let cell: ItemsTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ItemsTableViewCell") as? ItemsTableViewCell
        
        return cell
        
    }
    
    
    
    func roundRectToAllView(view: UIView )->Void{
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        view.layer.shadowOpacity = 0.50
        view.layer.shadowRadius = 1
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        
        
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        if tableView == contentTableView {
            
            
        }
    }
    
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }

    
}
