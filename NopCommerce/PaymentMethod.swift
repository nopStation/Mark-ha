//
//  PaymentMethod.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class PaymentMethod: NSObject {
    
    var Name             : NSString
    var Selected         : NSInteger
    var LogoUrl      : NSString
    var PaymentMethodSystemName         : NSString
    
    init (JSON: [String: AnyObject]) {
        
        self.Name = JSON["Name"] as! NSString
        self.Selected = JSON["Selected"] as! NSInteger
        self.LogoUrl = JSON["LogoUrl"] as! NSString
        self.PaymentMethodSystemName = JSON["PaymentMethodSystemName"] as! NSString
        
        
    }
    


}

/*

*/
