//
//  CheckOutRedirectPaymentViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/25/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD

class CheckOutRedirectPaymentViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var redirectWebView: UIWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let deviceId = UIDevice.currentDevice().identifierForVendor!.UUIDString
    let headers = ["DeviceId": UIDevice.currentDevice().identifierForVendor!.UUIDString]
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    var orderId = NSInteger ()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
//        let urlString = String(format: "https://www.banglagenie.com/api/checkout/OpcCompleteRedirectionPayment", )
//        let url = NSURL(string: urlString)
//        let requestObj = NSURLRequest(URL: url!)
//        self.redirectWebView.loadRequest(requestObj);
        
        self.loadingIndicator.startAnimating()
        let baseUrl = APIManagerClient.sharedInstance.baseURL
        let request = NSMutableURLRequest(URL: NSURL(string: NSString(format: "%@/checkout/OpcCompleteRedirectionPayment", baseUrl) as String)!)
        request.HTTPMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            request.setValue(token, forHTTPHeaderField: "Token")
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.setValue(self.deviceId, forHTTPHeaderField: "DeviceId")
        }
        redirectWebView .loadRequest(request)  
    }
    
    
    
    func getHeaderDic() -> [String:String] {
        
        var headerDictionary = self.headers
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            headerDictionary = ["Token":token,"DeviceId": self.deviceId]
            print(token)
        }
        
        
        return headerDictionary
        
    }

    
    @IBAction func dismissView(sender: AnyObject) {
        //self.navigationController?.popViewControllerAnimated(true)
        self.dismissViewControllerAnimated(true, completion:  {
            () -> Void in
            
            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
        })
        
    }
    
    // Mark: - UIWebView Delegate
    
    func webViewDidStartLoad(webView : UIWebView) {
        
        self.loadingIndicator.hidden = false
        self.loadingIndicator.startAnimating()

        
    }
    
    func webViewDidFinishLoad(webView : UIWebView) {
       
        self.loadingIndicator.hidden = true
        
        self.loadingIndicator.stopAnimating()

        let baseUrl = APIManagerClient.sharedInstance.baseURL
        let newString = baseUrl.stringByReplacingOccurrencesOfString("api", withString: "")
        if webView.request!.URL!.absoluteString == newString {
            
            
            APIManagerClient.sharedInstance.shoppingCartCount = 0
            
            self.showToast("Payment Successfull!")
            self.dismissViewControllerAnimated(true, completion:  {
                () -> Void in
                
                NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
            })
            
            

            
        }
        
        
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        
        self.loadingIndicator.hidden = true
        
    }
    
    
    
    func showToast(message:String) {
        
        let globalHud = MBProgressHUD.showHUDAddedTo(appDelegate.window, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(appDelegate.window, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(appDelegate.window, animated: true)
    }

    
    
}

