//
//  ConfirmOrderTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/22/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class ConfirmOrderTableViewCell: UITableViewCell {

    var request: Alamofire.Request?
    
    @IBOutlet weak var containerView: UIView!
    //@IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var UnitPrice: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var quantity: UILabel!

    @IBOutlet weak var itemImageView: UIImageView!
    //@IBOutlet weak var plusBtn: UIButton!
    
}
