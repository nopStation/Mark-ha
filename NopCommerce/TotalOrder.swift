//
//  TotalOrder.swift
//  NopCommerce
//
//  Created by BS-125 on 12/17/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class TotalOrder: NSObject {
    
    
    
    var subTotal    : NSString?
    var tax         : NSString?
    var orderTotal  : NSString?
    var shipping    : NSString?
    var discount    : NSString?
    
    init (JSON: [String: AnyObject]) {
        
        self.subTotal = JSON["SubTotal"] as? NSString
        
        self.tax = JSON["Tax"] as? NSString
        self.orderTotal = JSON["OrderTotal"] as? NSString
        self.shipping = JSON["Shipping"] as? NSString
        self.discount = JSON["OrderTotalDiscount"] as? NSString
        
    }

}
