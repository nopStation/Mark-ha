//
//  ViewController.swift
//  NopCommerce
//
//  Created by Jahid Hassan on 10/12/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import SwiftyJSON
import DropDown
import ZBarSDK


extension ZBarSymbolSet: SequenceType {
    public func generate() -> NSFastGenerator {
        return NSFastGenerator(self)
    }
}


class ViewController: UIViewController  , UITableViewDataSource, UITableViewDelegate ,UIPageViewControllerDataSource,UIPageViewControllerDelegate, ZBarReaderDelegate {
    
    var ZBarReader: ZBarReaderViewController?
    let reader: ZBarReaderViewController = ZBarReaderViewController()
    var parentCategoryArray = [GetAllCategories]()
    var pageViewController: UIPageViewController!
    var pageImages: NSArray!
    let cellIdentifier : String = "CartTableViewCell"
    var aPIManager : APIManager!
    var apiManagerClient: APIManagerClient!
    var homePageProductArray  = [GetHomePageProducts]()
    var homePageMenuFactureArray  = [HomePageMenuFacture]()
    var timer = NSTimer()
    var pageCounter = NSInteger ()
    var rightMenu : GlobalRightMenu?
    let subCategoryDropDown = DropDown()
    var pagerControl = UIPageControl()

    
    @IBOutlet weak var contentTableVIewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var myAccountBtn: UIButton!
    @IBOutlet weak var totalCart: CustomLabel!
    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var pager_View: UIView!
    @IBOutlet weak var dropDownContainerView: UIView!
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        self.reloadDropDownMenuList()
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
        self.aPIManager.getShoppingCartCount(
            onSuccess:{
                getTotal in
                self.updateShoppingCartCount()
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
        
    }
    
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCart.hidden = true
            totalCart.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCart.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCart.hidden = false
        }
    }

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"presentBarcodeReaderView:", name:"com.notification.BarcodeReader", object: nil)

        self.initializeAndDownloadData()
    }

    deinit {
        if self.view != nil {
            NSNotificationCenter.defaultCenter().removeObserver(self, name: "com.notification.BarcodeReader", object: nil)
        }
    }

    func initializeAndDownloadData()
    {
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        
        pageCounter = 0;
        
        // Setup Page view
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.lightGrayColor()
        pageControl.currentPageIndicatorTintColor = UIColor.blackColor()
        pageControl.backgroundColor = UIColor(red: 31/255.0, green: 32/255.0, blue: 32/255.0, alpha: 1)
        
        
        // Setup APIManager AND APIManagerClient
        self.aPIManager = APIManager()
        self.apiManagerClient = APIManagerClient.sharedInstance
        
        self.showLoading()
        self.aPIManager.loadHomePageProducts(
            onSuccess:{
                getHomeProducts in
                print(getHomeProducts.first)
                self.homePageProductArray = getHomeProducts
                self.apiManagerClient!.homePageProductArray = self.homePageProductArray
                
                self.hideLoading()
                self.loadHomePageBanner()
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
    }
    
    
      func loadHomePageBanner()
      {
        self.showLoading()
        self.aPIManager.loadHomePageBanner(
            onSuccess:{
                getHomePageBanner in
                print(getHomePageBanner.first)
                self.pageImages = getHomePageBanner
                
                self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
                self.pageViewController.dataSource = self
                self.pageViewController.delegate = self
                let startVC = self.viewControllerAtIndex(0) as PagerViewController
                let viewControllers = NSArray(object: startVC)
                self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil)
                self.pageViewController.view.frame = CGRectMake(0, 0, self.pager_View.frame.width, 215)
                self.pageViewController.view.layer.cornerRadius = 1
                self.pageViewController.view.backgroundColor = UIColor.whiteColor()
                self.pageViewController.view.layer.shadowColor = UIColor.darkGrayColor().CGColor
                self.pageViewController.view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
                self.pageViewController.view.layer.shadowOpacity = 2.0
                self.pageViewController.view.layer.shadowRadius = 2
                self.addChildViewController(self.pageViewController)
                self.pager_View.addSubview(self.pageViewController.view)
                self.pageViewController.didMoveToParentViewController(self)
                
                self.hideLoading()
                self.loadAllCategories()
                
                
                
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })

      }
    
    
    
    func loadAllCategories() {
                        self.showLoading()
        
                        self.aPIManager.loadAllCategories(
                            onSuccess:{
                                getAllCategories in
                                print(getAllCategories.first)
                                
                                self.updateShoppingCartCount()
                                
                                for category in getAllCategories {
                                    category.subCategories = []
                                    for subCategory in getAllCategories {
                                        if category.idd == subCategory.parentCategoryId {
                                            category.subCategories.append(subCategory)
                                        }
                                    }
                                    
                                }
                                
                                for lowestCategory in getAllCategories {
                                    
                                    let parentOfLowestCategory = lowestCategory.parentCategoryId
                                    for subCategory in getAllCategories {
                                        let subCategoryId = subCategory.idd
                                        if parentOfLowestCategory == subCategoryId {
                                            
                                            let parentOfSubCategory = subCategory.parentCategoryId
                                            for parentCategory in getAllCategories {
                                                let parentCategoryId = parentCategory.idd
                                                if parentOfSubCategory == parentCategoryId {
                                                    if parentCategory.parentCategoryId == 0 {
                                                        lowestCategory.isLowestItem = true
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                    
                                }
                                
                            self.apiManagerClient?.leftMenuCategoriesArray = getAllCategories
                            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadTableView", object: 1);
                            self.hideLoading()
                            self.loadHomePageMenuFacture()
                                
                            },
                            onError: { message in
                                print(message)
                                self.hideLoading()
                        })
    }
    
    func changeStatusBarAndNaviationBar() {
        
        self.statusView.backgroundColor = UIColor.getColorWithHexString((APIManagerClient.sharedInstance.AppStartUp?.BackgroundColorofPrimaryButton)!)
        self.navigationView.backgroundColor = UIColor.getColorWithHexString((APIManagerClient.sharedInstance.AppStartUp?.BackgroundColorofSecondaryButton)!)
    }
    
    
    func loadHomePageMenuFacture(){
        
        self.showLoading()

        self.aPIManager.loadHomePageMenuFacture(
            onSuccess:{

                getHomePageMenuFacture in
                
                self.hideLoading()
                self.homePageMenuFactureArray = getHomePageMenuFacture
                self.loadHomePageCategories()
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
    } 
    
    
    
    func loadHomePageCategories()
       {
        self.showLoading()
        self.aPIManager.loadHomePageCategories(
            onSuccess:{
                getHomePageCategoryModels in
                print(getHomePageCategoryModels.first)
                
                    self.apiManagerClient!.categoryArray.removeAllObjects()
                    self.apiManagerClient!.categoryArray.addObject(self.homePageProductArray)
                
                for homePageCategory in getHomePageCategoryModels{
                    
                    let homePageCategoryModelArray = [homePageCategory]
                    self.apiManagerClient!.categoryArray.addObject(homePageCategoryModelArray)

                }
                    self.apiManagerClient!.categoryArray.addObject(self.homePageMenuFactureArray)
                    self.contentTableView.reloadData()
                    self.contentTableVIewHeightConstraint.constant = self.contentTableView.contentSize.height
                    self.hideLoading()

                print(self.contentTableVIewHeightConstraint.constant)
                
                NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: "update", userInfo: nil, repeats: true)
                
                
         },
       onError: { message in
          print(message)
          self.showToast(message)
          self.hideLoading()
        })
 
    }

    
    func update() {
        
        pageCounter++

        
        if self.pageCounter == self.pageImages.count {
           self.pageCounter = 0
        }

        let startVC = self.viewControllerAtIndex( self.pageCounter) as PagerViewController
        let viewControllers = NSArray(object: startVC)
        self.pageViewController.setViewControllers(viewControllers as? [UIViewController], direction: .Forward, animated: true, completion: nil)

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func slideMenuBtnAct(sender: AnyObject) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        aVariable.toggleLeftSideMenuCompletion(nil)
        
    }

    @IBAction func cartBtnAct(sender: AnyObject) {
        
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)

    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
    }
    
    func presentBarcodeReaderView(notification: NSNotification) {
        
        reader.readerDelegate = self
        let scanner: ZBarImageScanner = reader.scanner
        scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
        presentViewController(reader, animated: true, completion: nil)
        
    }
    
    // MARK: - UIImagePickerDelegate
  
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let results: NSFastEnumeration = info["ZBarReaderControllerResults"] as! NSFastEnumeration
        var symbolFound : ZBarSymbol?
        
        for symbol in results as! ZBarSymbolSet {
            symbolFound = symbol as? ZBarSymbol
            break
        }
        
        let resultString = NSString(string: symbolFound!.data)
        //self.barCodeText.text = resultString as String    //set barCode
        print(resultString)
        
        reader.dismissViewControllerAnimated(true) { () -> Void in
            
            
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = resultString.integerValue
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            appDelegate.navController?.pushViewController(productDetailsViewController, animated: true)
            
        }
    }
    
    // MARK: - Table view data source

    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = NSBundle.mainBundle().loadNibNamed("HomePageCategoryHeaderView", owner: nil, options: nil)[0] as! HomePageCategoryHeaderView

        headerView.headerContainerView.layer.cornerRadius = 8.0
        
        
        if section == 0 {
            headerView.headerCategoryButton.hidden = true
            headerView.headerNameButton.setTitle(NSLocalizedString("FEATURED PRODUCTS", comment: ""), forState: UIControlState.Normal)
        }
        
        else if self.apiManagerClient.categoryArray.count-1 == section  {
            
            headerView.headerCategoryButton.hidden = true
            headerView.headerNameButton.setTitle(NSLocalizedString("FEATURED MANUFACTURERS", comment: ""), forState: UIControlState.Normal)
            
        }
        else  {
            headerView.headerCategoryButton.hidden = false
 
            let categoryModelArray = self.apiManagerClient.categoryArray[section] as! [HomePageCategoryModel]
            headerView.headerNameButton.tag = section
            headerView.headerNameButton.addTarget(self, action: "viewAllItems:", forControlEvents: UIControlEvents.TouchUpInside)
            headerView.headerNameButton.setTitle(categoryModelArray[0].name?.uppercaseString, forState: UIControlState.Normal)
            
            
            headerView.headerCategoryButton.tag = section
            headerView.headerCategoryButton.addTarget(self, action: "showSubCategory:", forControlEvents: UIControlEvents.TouchUpInside)
            
            if categoryModelArray[0].subCategory.count == 0 {
                headerView.headerCategoryButton.hidden = true
            } else {
                headerView.headerCategoryButton.hidden = false
            }
            
        }
        
        return headerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 || section == self.apiManagerClient.categoryArray.count-1 {
            return 30.0
        } else {
            return 90.0
        }
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = NSBundle.mainBundle().loadNibNamed("HomePageCategoryFooterView", owner: nil, options: nil)[0] as! HomePageCategoryFooterView

        //footerView.footerContainerView.layer.cornerRadius = 8.0
        roundRectToAllView(footerView.footerContainerView)
        footerView.viewAllItemsButton.layer.cornerRadius = 8.0
        

        if section == 0 || section == self.apiManagerClient.categoryArray.count-1 {
            
            //self.roundView(footerView.footerContainerTopView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
        }
        
        footerView.viewAllItemsButton.tag = section
        footerView.viewAllItemsButton.addTarget(self, action: "viewAllItems:", forControlEvents: UIControlEvents.TouchUpInside)

        if section == 0 || section == self.apiManagerClient.categoryArray.count - 1 {
            footerView.viewAllItemsButton.hidden = true
        } else {
            footerView.viewAllItemsButton.hidden = false
        }
        
        return footerView
    }


    func roundView(aView: UIView, byRoundingCorners corners: UIRectCorner)
     {
       let maskLayer = CAShapeLayer()
       maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(8, 8)).CGPath
       aView.layer.mask = maskLayer
    }

    
    func roundRectToAllView(view: UIView )->Void{
        view.layer.cornerRadius = 8
        view.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        view.layer.shadowOpacity = 0.7
        view.layer.shadowRadius = 0.5
        
        
//        view.layer.cornerRadius = 4.0
//        view.layer.masksToBounds = false
//        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
//        view.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
//        view.layer.shadowOpacity = 1.0
//        view.layer.shadowRadius = 1.0


    }
    
    func viewAllItems(button: UIButton)
    {
    
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let aVariable = appDelegate.slideContainer
        
        let category = self.apiManagerClient!.categoryArray[button.tag] as! [HomePageCategoryModel]
        let categoryDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
        categoryDetailsViewController.categoryId = category[0].idd
        categoryDetailsViewController.categoryName = category[0].name
        
        appDelegate.slideContainer.centerViewController = categoryDetailsViewController
        let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
        aVariable.rightMenuViewController=rightMenuViewC;

    }
    
    
    func showSubCategory(button: UIButton)
    {
        subCategoryDropDown.anchorView = button.superview!
        subCategoryDropDown.bottomOffset = CGPoint(x: button.superview!.frame.origin.x+100, y: button.superview!.frame.origin.y-100)
        
        subCategoryDropDown.dataSource.removeAll()
        let category = self.apiManagerClient!.categoryArray[button.tag] as! [HomePageCategoryModel]
        for subCategory in category[0].subCategory {
            subCategoryDropDown.dataSource.append(subCategory.name)
        }

        subCategoryDropDown.selectionAction = {
            (index, item) in
            
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let aVariable = appDelegate.slideContainer
            
                let category = self.apiManagerClient!.categoryArray[button.tag] as! [HomePageCategoryModel]
                let categoryDetailsViewController = CategoryDetailsViewController(nibName: "CategoryDetailsViewController", bundle: nil)
                categoryDetailsViewController.categoryId = category[0].subCategory[index].idd
                categoryDetailsViewController.categoryName = category[0].subCategory[index].name
            
                appDelegate.slideContainer.centerViewController = categoryDetailsViewController
                let rightMenuViewC = RightSlideMenuViewController(nibName: "RightSlideMenuViewController", bundle: nil)
                aVariable.rightMenuViewController=rightMenuViewC;
        }
        
        subCategoryDropDown.selectionBackgroundColor = UIColor.clearColor()
        if subCategoryDropDown.hidden {
            subCategoryDropDown.show()
        } else {
            subCategoryDropDown.hide()
        }
        
    }

    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        print(self.apiManagerClient!.categoryArray.count)
        return self.apiManagerClient!.categoryArray.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1

    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:CategoryRow! = tableView.dequeueReusableCellWithIdentifier("cell") as? CategoryRow
        if(cell == nil)
        {
            cell = CategoryRow(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"cell")
        }
        let array = self.apiManagerClient!.categoryArray[indexPath.section]
        cell.getCategoryArray(array as! NSArray , sectionIndex:indexPath.section )
        
        return cell
    }

    // MARK: - Table view delegate
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // let cell = tableView.cellForRowAtIndexPath(indexPath) as! CartTableViewCell
        
    }

    func viewControllerAtIndex(index: Int) -> PagerViewController
    {
        if ((self.pageImages.count == 0) || (index >= self.pageImages.count)) {
            return PagerViewController()
        }
        let vc: PagerViewController = PagerViewController(nibName: "PagerViewController", bundle: nil)
        vc.imageFile = self.pageImages[index] as! String
        
        
        vc.pageIndex = index
        return vc
   
    }

    // MARK: - Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?
    {
        
        let vc = viewController as! PagerViewController
        var index = vc.pageIndex
        
        
        if (index == 0 || index == NSNotFound)
        {
            return nil
            
        }
        
        index--
        return self.viewControllerAtIndex(index)
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        let vc = viewController as! PagerViewController
        var index = vc.pageIndex
        
        if (index == NSNotFound)
        {
            return nil
        }
        
        index++
        
        if (index == self.pageImages.count)
        {
            return nil
        }
        
        return self.viewControllerAtIndex(index)
        
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return self.pageImages.count
    }
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int
    {
        return pageCounter
    }
    // MARK: - Page View Controller Delegate

    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
    }
    func hideLoading() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
}

