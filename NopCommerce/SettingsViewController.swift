//
//  SettingsViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 2/10/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import DropDown

class SettingsViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var urlTextField: UITextField!
    
    var rightMenu : GlobalRightMenu?
    var aPIManager : APIManager?
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.containerView.layer.cornerRadius = 4.0
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        
        self.aPIManager = APIManager()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
    }
    
    deinit
    {
        if self.view != nil{
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    @IBAction func testURLBtnAct(sender: AnyObject) {
        
        let url = self.urlTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if url.characters.count == 0 {
            self.showToast("Enter URL")
            return
        }
        
        self.urlTextField.resignFirstResponder()
        
        self.showLoading()
        self.aPIManager!.testUrl( url,
            onSuccess:{
                newBaseUrl in
                
                self.hideLoading()
                self.showNewBaseUrlSelectAlert(newBaseUrl)
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
    }
    
    func showNewBaseUrlSelectAlert(newBaseUrl: String)
    {
        let alertMessage = String(format: "The URL you typed is a valid URL. Do you want to set this as your nopCommerce URL ?")
        
        let alertController = UIAlertController(title: "Success", message: alertMessage, preferredStyle: .Alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: .Default) { (action:UIAlertAction!) in
            
            self.forceLogOut()
            APIManagerClient.sharedInstance.baseURL = newBaseUrl
            
            self.switchToRootViewController()
            
        }
        alertController.addAction(yesAction)
        
        let noAction = UIAlertAction(title: "No", style: .Cancel, handler: nil)
        alertController.addAction(noAction)
        
        self.presentViewController(alertController, animated: true, completion:nil)

    }
    
    @IBAction func defaultURLBtnAct(sender: AnyObject) {
        
        self.forceLogOut()
        APIManagerClient.sharedInstance.baseURL = APIManagerClient.sharedInstance.defaultBaseUrl
        
        self.switchToRootViewController()
    }
    
    func switchToRootViewController () {
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        delegate.switchToRootViewController()
    }
    
    func forceLogOut() {
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey("accessToken")
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
        let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
        self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    
    
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func hideLoading() {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.scrollViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.scrollViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }
    
}
