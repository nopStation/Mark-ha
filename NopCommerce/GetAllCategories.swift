//
//  GetAllCategories.swift
//  NopCommerce
//
//  Created by BS-125 on 11/6/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class GetAllCategories: NSObject {
    
    
    var idd                 :  NSInteger
    var name                :  String
    var parentCategoryId    :  NSInteger
    var displayOrder        :  NSInteger
    var iconPath            :  String
    var subCategories       :  [GetAllCategories] = []       
    var collapsed           :  Bool = false
    var isLowestItem        :  Bool = false
    
    init( parentCategoryId : NSInteger, name : String , idd : NSInteger , displayOrder : NSInteger, iconPath : String)
    {
        self.name = name
        self.parentCategoryId = parentCategoryId
        self.idd = idd
        self.displayOrder = displayOrder
        self.iconPath = iconPath
    }

}
