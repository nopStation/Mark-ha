//
//  WishListItems.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/12/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

public class WishListItems: NSObject {

    var ProductId      : NSInteger
    var ProductName    : NSString
    var ImageUrl       : String?
    var UnitPrice      : NSString
    
    init(JSON : AnyObject) {
        
        self.ImageUrl        = (JSON.valueForKeyPath("Picture.ImageUrl") as? String)
        self.ProductId       = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.ProductName     = (JSON.valueForKeyPath("ProductName") as! NSString)
        self.UnitPrice       = (JSON.valueForKeyPath("UnitPrice") as! NSString)
        
    }
    
}
