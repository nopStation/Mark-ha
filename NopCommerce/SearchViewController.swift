//
//  SearchViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/14/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown
import MFSideMenu
class SearchViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate {
    
    var aPIManager : APIManager?
    var apiManagerClient: APIManagerClient!
    var searchedProductList = [ProductsInfo]()

    @IBOutlet weak var search_bar: UISearchBar!
    @IBOutlet weak var contentCollectionView: UICollectionView!

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let textFieldInsideSearchBar = search_bar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.valueForKey("placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.whiteColor()
        
        self.search_bar.tintColor = UIColor.whiteColor()


        
        self.aPIManager = APIManager()

        self.contentCollectionView.registerNib(UINib(nibName: "CategoryDetailsCollectionViewCell", bundle: NSBundle(identifier: "CategoryDetailsCollectionViewCell")), forCellWithReuseIdentifier: "CategoryDetailsCollectionViewCell")
    }

    @IBAction func backBtnAct(sender: AnyObject)
    {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake(collectionView.bounds.size.width/2-15, 236)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return searchedProductList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell : CategoryDetailsCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CategoryDetailsCollectionViewCell", forIndexPath: indexPath) as! CategoryDetailsCollectionViewCell
        self.roundCell(cell)
        
        let imageUrl = searchedProductList[indexPath.item].ImageUrl as String
        cell.AlbumImage!.sd_setImageWithURL(NSURL(string: imageUrl))
        cell.nameLbl.text = searchedProductList[indexPath.item].Name as String
        cell.amountLbl.text = searchedProductList[indexPath.item].Price as! String
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
    {
        print(searchedProductList[indexPath.item].id)
        
        if searchedProductList[indexPath.item].id != 0 {
            
            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
            productDetailsViewController.productId  = searchedProductList[indexPath.item].id
            
            print(searchedProductList[indexPath.item].id)
            self.navigationController!.pushViewController(productDetailsViewController, animated: true)
        }
    }
    
    
    func roundCell(cell:UIView) {
        
        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true
        cell.backgroundColor = UIColor.whiteColor()
        cell.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.layer.shadowOffset = CGSize(width: 1.50, height: 1.50)
        cell.layer.shadowOpacity = 0.30
        cell.layer.shadowRadius = 1
        cell.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.layer.borderWidth = 0.5
        cell.layer.borderColor = UIColor(red: 218/255, green: 218/255, blue: 218/255, alpha: 1.0).CGColor
        
        
    }

    
    /*
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        // let currentOffset = scrollView.contentOffset.y;
        // let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        
        let offset = scrollView.contentOffset;
        NSLog("offset: %f", offset.y);
        
        
        if offset.y > 10 {
            
            if !isThereAnyPagingINdexFlag {
                
                MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                self.aPIManager = APIManager()
                
                var paramDic = [String:AnyObject]()
                paramDic["pagenumber"] = ++self.pagindIndex
                
                if let minPrice = self.apiManagerClient.categoryDetailsInfo.fromPrice, let maxPrice = self.apiManagerClient.categoryDetailsInfo.toPrice
                {
                    paramDic["price"] = String(format: "%f-%f", minPrice, maxPrice)
                }
                
                if (self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count > 0)
                {
                    var specsString = "\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[0].FilterId)"
                    for var i = 1; i < self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems.count; i++
                    {
                        specsString += ",\(self.apiManagerClient.categoryDetailsInfo.alreadyFilteredItems[i].FilterId)"
                    }
                    
                    paramDic["specs"] = specsString
                }
                
                self.aPIManager!.loadCategoryDetails(self.categoryId, params:  paramDic,
                    onSuccess:{
                        getProducts in
                        self.categoryDetailsInfo.productsInfo.appendContentsOf(getProducts.productsInfo)
                        self.apiManagerClient.categoryDetailsInfo.productsInfo.appendContentsOf(getProducts.productsInfo)
                        
                        self.contentCollectionView.reloadData()
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.reloadFilterTableView", object: 1);
                        
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                        
                        if getProducts.productsInfo.count == 0 {
                            self.isThereAnyPagingINdexFlag = true
                        }
                    },
                    onError: { message in
                        print(message)
                        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                })
            }
        }
    }*/
    
    // MARK: UISearchBarDelegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
        self.search_bar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked( searchBar: UISearchBar)
    {
        self.search_bar.resignFirstResponder()
        self.showLoading()

        self.aPIManager!.getSearchResults( searchBar.text!, onSuccess:{
                gotSearcheResults in
            
                self.searchedProductList = gotSearcheResults
                self.contentCollectionView.reloadData()
                self.hideLoading()
            
                if self.searchedProductList.count == 0 {
                    self.showToast("No result found")
                }
            },
            onError: {
                message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
    }

    func hideLoading()
    {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading()
    {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    
    func showToast(message:String)
    {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }

}
