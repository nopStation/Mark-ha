//
//  ProductDetailsInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 11/23/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

import SwiftyJSON

public class ProductDetailsInfo: NSObject {
    
    
    var AllowedQuantities :                   NSArray
    var AvailableForPreOrder :                NSInteger
    var CustomerEnteredPrice :                NSInteger
    var CustomerEnteredPriceRange :           NSInteger?
    var CustomerEntersPrice :                 NSInteger
    var DisableBuyButton :                    Bool
    var DisableWishlistButton :               Bool
    
    var EnteredQuantity :                     NSInteger
    var IsRental :                            Bool
    var PreOrderAvailabilityStartDateTimeUtc :String?
    var Cart_ProductId :                      NSInteger
    var UpdatedShoppingCartItemId :           NSInteger

    var AssociatedProducts :                  NSArray
    var CategoryBreadcrumb :                  [Breadcrumb]
    
    var Breadcrumb_CustomProperties :         NSString?
    var Enabled :                             Bool
    var Breadcrumb_ProductId :                NSInteger
    var ProductName :                         NSString?
    var ProductSeName :                       NSString?

    var CompareProductsEnabled :              Bool
    var CustomProperties :                    NSString?
    var ImageUrl :                            NSString
    var DefaultPictureZoomEnabled :           Bool
    var DeliveryDate :                        NSString?
    var DisplayBackInStockSubscription :      NSInteger
    var EmailAFriendEnabled :                 Bool
    var FreeShippingNotificationEnabled :     Bool
    var FullDescription :                     NSString?
    
    var GiftCard_CustomProperties :           NSString?
    var GiftCardType :                        NSInteger
    var IsGiftCard :                          Bool
    var Message :                             NSString?
    var RecipientEmail :                      NSString?
    var RecipientName :                       NSString?
    var SenderEmail :                         NSString?
    var SenderName :                          NSString?
    
    
    var HasSampleDownload :                   Bool
    var Id :                                  NSInteger
    var IsFreeShipping :                      Bool
    var GiftCard_IsRental :                   Bool
    var IsShipEnabled :                       Bool
    var ManufacturerPartNumber :              NSString?
    var Name :                                NSString?
    var PictureModels :                       NSArray
    
    
    var ProductAttributes :                   [ProductAttributesInfo]
    var ProductManufacturers :                NSArray
    
    
    
    var BasePricePAngV :                      NSString?
    var CallForPrice   :                      NSInteger
    var CurrencyCode  :                       NSString?
    var ProductPrice_CustomProperties :       NSString?
    var ProductPrice_CustomerEntersPrice :    NSInteger
    var DisplayTaxShippingInfo :              Bool
    var HidePrices :                          Bool
    var ProductPrice_IsRental :               Bool
    var OldPrice :                            NSString?
    var Price :                               NSString?
    var PriceValue :                          NSInteger
    var PriceWithDiscount :                   NSString?
    var PriceWithDiscountValue :              NSInteger
    var ProductPrice_ProductId :              NSInteger
    var RentalPrice  :                        NSString?
    
    
    var AllowCustomerReviews :                Bool
    var ProductReviewOverview_ProductId  :    NSInteger
    var RatingSum :                           NSInteger
    var TotalReviews :                        NSInteger
    
    
    
    var ProductTemplateViewPath :             NSString?
    var RentalEndDate :                       NSString?
    var RentalStartDate :                     NSString?
    var ShortDescription :                    NSString?
    var ShowManufacturerPartNumber :          NSInteger
    var ShowVendor :                          Bool
    var StockAvailability :                   NSString?
    var TierPrices  :                         NSArray?
    
    var VendorModel_CustomProperties :        NSString?
    var VendorModel_Id  :                     NSInteger
    var VendorModel_Name :                    NSString?
    var SeName :                              NSString?
    
    
    init(JSON : AnyObject) {
        print(JSON)
        self.AllowedQuantities             = (JSON.valueForKeyPath("AddToCart.AllowedQuantities") as! NSArray)
        self.AvailableForPreOrder          = (JSON.valueForKeyPath("AddToCart.AvailableForPreOrder") as! NSInteger)
        self.CustomerEnteredPrice          = (JSON.valueForKeyPath("AddToCart.CustomerEnteredPrice") as! NSInteger)
        self.CustomerEnteredPriceRange     = (JSON.valueForKeyPath("AddToCart.CustomerEnteredPriceRange") as? NSInteger)
        self.CustomerEntersPrice           = (JSON.valueForKeyPath("AddToCart.CustomerEntersPrice") as! NSInteger)
        self.DisableBuyButton              = (JSON.valueForKeyPath("AddToCart.DisableBuyButton") as! Bool)
        self.DisableWishlistButton         = JSON.valueForKeyPath("AddToCart.DisableWishlistButton") as! Bool
        self.EnteredQuantity               = (JSON.valueForKeyPath("AddToCart.EnteredQuantity") as! NSInteger)
        self.IsRental                      = (JSON.valueForKeyPath("AddToCart.EnteredQuantity") as! Bool)
        self.PreOrderAvailabilityStartDateTimeUtc = (JSON.valueForKeyPath("AddToCart.PreOrderAvailabilityStartDateTimeUtc") as? String)
        self.Cart_ProductId                = (JSON.valueForKeyPath("AddToCart.ProductId") as! NSInteger)
        self.UpdatedShoppingCartItemId     = (JSON.valueForKeyPath("AddToCart.UpdatedShoppingCartItemId") as! NSInteger)
        self.AssociatedProducts            = (JSON.valueForKeyPath("AssociatedProducts") as! NSArray)
        self.CategoryBreadcrumb            = [Breadcrumb]()
        
        
        for data in (JSON.valueForKeyPath("Breadcrumb.CategoryBreadcrumb") as! NSArray){
            
            let bread = Breadcrumb(JSONValue: data)
            print(bread.Name)
            self.CategoryBreadcrumb.append(bread)
        }
        
        self.Breadcrumb_CustomProperties   = (JSON.valueForKeyPath("Breadcrumb.CustomProperties") as? NSString)
        self.Enabled                       = JSON.valueForKeyPath("Breadcrumb.Enabled") as! Bool
        self.Breadcrumb_ProductId          = (JSON.valueForKeyPath("Breadcrumb.ProductId") as! NSInteger)
        self.ProductName                   = (JSON.valueForKeyPath("Breadcrumb.ProductName") as! NSString)
        self.ProductSeName                 = (JSON.valueForKeyPath("Breadcrumb.ProductSeName") as? NSString)
        self.CompareProductsEnabled        = (JSON.valueForKeyPath("CompareProductsEnabled") as! Bool)
        self.CustomProperties              = (JSON.valueForKeyPath("CustomProperties") as? NSString)
        self.ImageUrl                      = (JSON.valueForKeyPath("DefaultPictureModel.ImageUrl") as! NSString)
        self.DefaultPictureZoomEnabled     = (JSON.valueForKeyPath("DefaultPictureZoomEnabled") as! Bool)
        self.DeliveryDate                  = (JSON.valueForKeyPath("DeliveryDate") as? NSString)
        self.DisplayBackInStockSubscription = (JSON.valueForKeyPath("DisplayBackInStockSubscription") as! NSInteger)
        self.EmailAFriendEnabled           = (JSON.valueForKeyPath("EmailAFriendEnabled") as! Bool)
        self.FreeShippingNotificationEnabled = (JSON.valueForKeyPath("FreeShippingNotificationEnabled") as! Bool)
        self.FullDescription               = (JSON.valueForKeyPath("FullDescription") as? NSString)
        
        self.GiftCard_CustomProperties     = (JSON.valueForKeyPath("GiftCard.CustomProperties") as? NSString)
        self.GiftCardType                  = (JSON.valueForKeyPath("GiftCard.GiftCardType") as! NSInteger)
        self.IsGiftCard                    = (JSON.valueForKeyPath("GiftCard.IsGiftCard") as! Bool)
        self.Message                       = (JSON.valueForKeyPath("GiftCard.Message") as? NSString)
        self.RecipientEmail                = (JSON.valueForKeyPath("GiftCard.RecipientEmail") as? NSString)
        self.RecipientName                 = (JSON.valueForKeyPath("GiftCard.RecipientName") as? NSString)
        self.SenderEmail                   = (JSON.valueForKeyPath("GiftCard.SenderEmail") as? NSString)
        self.SenderName                    = (JSON.valueForKeyPath("GiftCard.SenderName") as? NSString)
        
        self.HasSampleDownload             = (JSON.valueForKeyPath("HasSampleDownload") as! Bool)
        self.Id                            = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.IsFreeShipping                = (JSON.valueForKeyPath("IsFreeShipping") as! Bool)
        self.GiftCard_IsRental             = (JSON.valueForKeyPath("IsFreeShipping") as! Bool)
        self.IsShipEnabled                 = (JSON.valueForKeyPath("IsShipEnabled") as! Bool)
        self.ManufacturerPartNumber        = (JSON.valueForKeyPath("ManufacturerPartNumber") as? NSString)
        self.Name                          = (JSON.valueForKeyPath("Name") as! NSString)

        self.PictureModels                 = (JSON.valueForKeyPath("PictureModels") as! NSArray)
        
        self.ProductAttributes             = [ProductAttributesInfo]()
            //(JSON.valueForKeyPath("ProductAttributes") as! NSArray)

        for productsAttData in (JSON.valueForKeyPath("ProductAttributes") as! NSArray){
            let productArr = ProductAttributesInfo(JSON: productsAttData)

            self.ProductAttributes.append(productArr)
            
        }
        self.ProductManufacturers          = (JSON.valueForKeyPath("ProductManufacturers") as! NSArray)
        self.BasePricePAngV                = (JSON.valueForKeyPath("ProductPrice.BasePricePAngV") as? NSString)
        self.CallForPrice                  = (JSON.valueForKeyPath("ProductPrice.CallForPrice") as! NSInteger)
        self.CurrencyCode                  = (JSON.valueForKeyPath("ProductPrice.CurrencyCode")as? NSString)
        self.ProductPrice_CustomProperties = (JSON.valueForKeyPath("ProductPrice.CustomProperties") as? NSString)
        self.ProductPrice_CustomerEntersPrice = (JSON.valueForKeyPath("ProductPrice.CustomerEntersPrice") as! NSInteger)

        self.DisplayTaxShippingInfo        = (JSON.valueForKeyPath("ProductPrice.DisplayTaxShippingInfo") as! Bool)
        self.HidePrices                    = (JSON.valueForKeyPath("ProductPrice.HidePrices") as! Bool)
        self.ProductPrice_IsRental         = (JSON.valueForKeyPath("ProductPrice.IsRental") as! Bool)
        self.OldPrice                      = (JSON.valueForKeyPath("ProductPrice.OldPrice") as? NSString)
        self.Price                         = (JSON.valueForKeyPath("ProductPrice.Price") as? NSString)
        self.PriceValue                    = (JSON.valueForKeyPath("ProductPrice.PriceValue") as! NSInteger)
        self.PriceWithDiscount             = (JSON.valueForKeyPath("ProductPrice.PriceWithDiscount") as? NSString)
        self.PriceWithDiscountValue        = (JSON.valueForKeyPath("ProductPrice.PriceWithDiscountValue") as! NSInteger)
        self.ProductPrice_ProductId        = (JSON.valueForKeyPath("ProductPrice.ProductId") as! NSInteger)
        self.RentalPrice                   = (JSON.valueForKeyPath("ProductPrice.RentalPrice") as? NSString)
        self.AllowCustomerReviews          = (JSON.valueForKeyPath("ProductReviewOverview.AllowCustomerReviews") as! Bool)
        self.ProductReviewOverview_ProductId = (JSON.valueForKeyPath("ProductReviewOverview.ProductId") as! NSInteger)
        self.RatingSum                     = (JSON.valueForKeyPath("ProductReviewOverview.RatingSum") as! NSInteger)
        self.TotalReviews                  = (JSON.valueForKeyPath("ProductReviewOverview.TotalReviews") as! NSInteger)
        self.ProductTemplateViewPath       = (JSON.valueForKeyPath("ProductTemplateViewPath") as? NSString)
        self.RentalEndDate                 = (JSON.valueForKeyPath("RentalEndDate") as? NSString)
        self.RentalStartDate               = (JSON.valueForKeyPath("RentalStartDate") as? NSString)
        
        
        self.ShortDescription              = (JSON.valueForKeyPath("ShortDescription") as? NSString)
        self.ShowManufacturerPartNumber    = (JSON.valueForKeyPath("ShowManufacturerPartNumber") as! NSInteger)
        self.ShowVendor                    = (JSON.valueForKeyPath("ShowVendor") as! Bool)
        self.StockAvailability             = (JSON.valueForKeyPath("StockAvailability") as? NSString)
        self.TierPrices                    = (JSON.valueForKeyPath("StockAvailability") as? NSArray)
        self.VendorModel_CustomProperties  = (JSON.valueForKeyPath("VendorModel.CustomProperties") as? NSString)
        self.VendorModel_Id                = (JSON.valueForKeyPath("VendorModel.Id") as! NSInteger)
        self.VendorModel_Name              = (JSON.valueForKeyPath("VendorModel.Name") as? NSString)
        self.SeName                        = (JSON.valueForKeyPath("VendorModel.SeName") as? NSString)
        
    }
    

}
