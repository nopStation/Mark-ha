//
//  HomePageCategoryFooterView.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/6/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class HomePageCategoryFooterView: UIView {

    @IBOutlet weak var footerContainerView: UIView!
    @IBOutlet weak var viewAllItemsButton: UIButton!
    @IBOutlet weak var footerContainerTopView: UIView!
    
}
