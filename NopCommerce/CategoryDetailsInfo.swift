//
//  CategoryDetailsInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 11/23/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit


public class CategoryDetailsInfo: NSObject {
    
    var productsInfo: [ProductsInfo] = []
    var fromPrice: Double?
    var toPrice: Double?
    var availableSortOptions: [AvailableSortOptions] = []
    var notFilteredItems: [NotFilteredItems] = []
    var alreadyFilteredItems : [NotFilteredItems] = []
    var TotalPages = 0
}





 public class ProductsInfo: NSObject {
    
    var ProductId :             NSInteger
    var TotalReviews :          NSInteger
    var RatingSum :             NSInteger
    var AllowCustomerReviews :  Bool
    var id :                    NSInteger
    var CustomProperties :      NSDictionary?
    
    var ImageUrl :              NSString
    var Name :                  NSString
    var ShortDescription :      NSString?
    var OldPrice :              NSString?
    var Price :                 NSString?
    
    init (JSON:AnyObject) {
        
        
        self.ProductId              = JSON.valueForKeyPath("ReviewOverviewModel.ProductId") as! NSInteger
        self.TotalReviews           = JSON.valueForKeyPath("ReviewOverviewModel.TotalReviews") as! NSInteger
        self.RatingSum              = JSON.valueForKeyPath("ReviewOverviewModel.RatingSum") as! NSInteger
        self.AllowCustomerReviews   = JSON.valueForKeyPath("ReviewOverviewModel.AllowCustomerReviews") as! Bool
        self.id                     = JSON.valueForKeyPath("Id") as! NSInteger
        self.CustomProperties       = JSON.valueForKeyPath("CustomProperties") as? NSDictionary
        
        self.ImageUrl               = JSON.valueForKeyPath("DefaultPictureModel.ImageUrl") as! NSString
        self.Name                   = JSON.valueForKeyPath("Name") as! NSString
        self.ShortDescription       = JSON.valueForKeyPath("ShortDescription") as? NSString
        self.OldPrice               = JSON.valueForKeyPath("ProductPrice.ProductPrice") as? NSString
        self.Price                  = JSON.valueForKeyPath("ProductPrice.Price") as? NSString

        
        
        
    }
    
    
    
    
//    init(ProductId : NSInteger, TotalReviews : NSInteger , RatingSum : NSInteger , AllowCustomerReviews : Bool, idd : NSInteger , CustomProperties : String
//        , ImageUrl : String , name : String , ShortDescription : String, OldPrice : String , Price : String) {
//            
//            self.ProductId =            ProductId
//            self.TotalReviews =         TotalReviews
//            self.RatingSum =            RatingSum
//            self.AllowCustomerReviews = AllowCustomerReviews
//            self.id =                  idd
//            self.CustomProperties =     CustomProperties
//            self.ImageUrl =             ImageUrl
//            self.Name  =                name
//            self.ShortDescription =     ShortDescription
//            self.OldPrice =             OldPrice
//            self.Price =                Price
//            
//            
//            
//    }


}


public class AvailableSortOptions: NSObject {
    
    var Selected :             Bool?
    var Group :                NSString?
    var Disabled :             Bool?
    var Text :                 NSString?
    var Value :                NSInteger?
    
    init (JSON:AnyObject) {
        
        self.Selected        = JSON.valueForKeyPath("Selected") as? Bool
        self.Group           = JSON.valueForKeyPath("Group") as? NSString
        self.Disabled        = JSON.valueForKeyPath("Disabled") as? Bool
        self.Text            = JSON.valueForKeyPath("Text") as? NSString
        self.Value           = JSON.valueForKeyPath("Value") as? NSInteger
        
     }

}

public class NotFilteredItems: NSObject {
    var SpecificationAttributeName       : NSString?
    var FilterId                         : NSInteger?
    var SpecificationAttributeOptionName : NSString?
    
    init (JSON:AnyObject) {
        
        self.SpecificationAttributeName          = JSON.valueForKeyPath("SpecificationAttributeName") as? NSString
        self.FilterId                            = JSON.valueForKeyPath("FilterId") as? NSInteger
        self.SpecificationAttributeOptionName    = JSON.valueForKeyPath("SpecificationAttributeOptionName") as? NSString
        
    }
    
}



