//
//  SignInViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/28/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown

class SignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    var aPIManager : APIManager?
    //let dropDown = DropDown()
    var rightMenu : GlobalRightMenu?

    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
        
    }
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.aPIManager = APIManager()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)

        
    }
    
    deinit
    {
        if self.view != nil{
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
    }

    @IBAction func login(sender: UIButton) {
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            self.showToast("Enter E-Mail address")
            return
        }
        
        if self.isValidEmail(email) == false {
            self.showToast("Invalid E-Mail address")
            return
        }
        
        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if password.characters.count == 0 {
            self.showToast("Enter password")
            return
        }
        
        self.proceedToLogIn()
    }
    
    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
        
    }
    
    func proceedToLogIn()
    {
        var params = [String: AnyObject]()
        params["email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        self.showLoading()
        aPIManager!.signInCustomer(params,
            onSuccess: {
                self.hideLoading()
                self.showToast("Login Successful")
                self.performSelector("backToRootView:", withObject: nil, afterDelay:2)
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
    }
    
    @IBAction func register(sender: UIButton) {
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let registerViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(registerViewC, animated: true)
    }
    
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else{
            self.rightMenu!.dropDown.hide()
        }
        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
        //if APIManagerClient.sharedInstance.shoppingCartCount > 0 {
            
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
            
        //} else {
            
        //    self.showToast("Cart is empty")
            
       // }
    }
    
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view.window, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
    }
    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) {
            () -> Void in
            self.scrollViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
        
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.scrollViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }
}