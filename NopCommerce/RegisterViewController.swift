//
//  RegisterViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 12/28/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown

class RegisterViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var maleRadioButton: UIButton!
    @IBOutlet weak var femaleRadioButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var newsLetterButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
    
    var dobSelected: Bool = false
    var dobDay: Int = 0
    var dobMonth: Int = 0
    var dobYear: Int = 0
    
    var genderSelected: Bool = false
    var gender: String = ""
    
    var newsLetterSelected: Bool = false
    
    var aPIManager : APIManager?
    var rightMenu : GlobalRightMenu?

    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    override func viewWillAppear(animated: Bool)
    {
        self.reloadDropDownMenuList()
    }
    
    
    func reloadDropDownMenuList()
    {
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()

    }
    
    func updateShoppingCartCount()
    {
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.aPIManager = APIManager()
        
        self.toggleNewsLetterOption(UIButton())
        
        self.containerView.layer.cornerRadius = 4.0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
    }

    deinit
    {
        if self.view != nil{
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
    }
    
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else{
            self.rightMenu!.dropDown.hide()
        }
        
    }
    
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
        let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
        self.navigationController!.pushViewController(shoppingCartView, animated: true)
        
    }
    
    @IBAction func showDOBPicker(sender: AnyObject) {
        
        DatePickerDialog().show("Choose Date of birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .Date) {
            (date) -> Void in
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US")
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            self.dobSelected = true
            self.dobLabel.text = "\(dateFormatter.stringFromDate(date))"
            
            let calendar = NSCalendar.currentCalendar()
            let components = calendar.components([.Day , .Month , .Year], fromDate: date)
            
            self.dobDay = components.day
            self.dobMonth = components.month
            self.dobYear =  components.year
        }

    }
    
    @IBAction func selectMale(sender: UIButton) {
        self.maleRadioButton.selected = true
        self.femaleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "M"
    }
    
    @IBAction func selectFemale(sender: UIButton) {
        self.femaleRadioButton.selected = true
        self.maleRadioButton.selected = false
        
        self.genderSelected = true
        self.gender = "F"
    }
    
    @IBAction func toggleNewsLetterOption(sender: UIButton) {
        if self.newsLetterButton.selected {
            self.newsLetterButton.selected = false
            self.newsLetterSelected = false
        } else {
            self.newsLetterButton.selected = true
            self.newsLetterSelected = true
        }
    }
    
    @IBAction func register(sender: UIButton) {
        
        let firstName = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if firstName.characters.count == 0 {
            self.showToast("Enter First Name")
            return
        }
        
        let lastName = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if lastName.characters.count == 0 {
            self.showToast("Enter Last Name")
            return
        }
        
//        if self.dobSelected == false {
//            self.showToast("Choose Date of birth")
//            return
//        }
//        
//        if self.genderSelected == false {
//            self.showToast("Choose gender")
//            return
//        }
        
        let email = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if email.characters.count == 0 {
            self.showToast("Enter E-Mail address")
            return
        }
        
        
        let phoneNumber = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if phoneNumber.characters.count == 0 {
            self.showToast("Enter Phone Number")
            return
        }
        
        if self.isValidEmail(email) == false {
            self.showToast("Invalid E-Mail address")
            return
        }
        
//        let company = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
//        if company.characters.count == 0 {
//            self.showToast("Enter company name")
//            return
//        }
        
        let password = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if password.characters.count == 0 {
            self.showToast("Enter password")
            return
        }
        
        let confirmedPassword = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        if confirmedPassword.characters.count == 0 {
            self.showToast("Confirm your password")
            return
        }
        
        if password != confirmedPassword {
            self.showToast("Passwords don't match")
            return
        }
        
        self.proceedToRegister()
    }
    
    func isValidEmail(testEmail:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testEmail)
    }
    
    func proceedToRegister()
    {
        var params = [String: AnyObject]()
        params["FirstName"] = self.firstNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["LastName"] = self.lastNameTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["DateOfBirthDay"] = self.dobDay
        params["DateOfBirthMonth"] = self.dobMonth
        params["DateOfBirthYear"] = self.dobYear
        params["Email"] = self.emailTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Phone"] = self.phoneNumberTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())

        params["Company"] = self.companyTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["Newsletter"] = self.newsLetterSelected ? "true":"false"
        params["Gender"] = self.gender
        params["Password"] = self.passwordTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        params["ConfirmPassword"] = self.confirmPassTextField.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        self.showLoading()
        aPIManager!.registerCustomer(params,
            onSuccess: {
                self.hideLoading()
                self.showToast("Registration Successful")
                self.performSelector("backToRootView:", withObject: nil, afterDelay:2)
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
    }
    
    
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view.window, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
    }
    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.scrollViewBottomConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.scrollViewBottomConstraint.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    // MARK: - UITextField Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool
    {
        textField.resignFirstResponder();
        return true;
    }
}
