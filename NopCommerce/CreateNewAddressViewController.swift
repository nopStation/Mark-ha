//
//  CreateNewAddressViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/30/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown

class CreateNewAddressViewController: UIViewController {

    var aPIManager : APIManager?
    var rightMenu : GlobalRightMenu?

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameRequiredLabel: UILabel!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var lastNameRequiredLabel: UILabel!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailRequiredLabel: UILabel!
    
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var companyRequiredLabel: UILabel!
    
    @IBOutlet weak var countryDropDownView: UIView!
    @IBOutlet weak var countryLabel: UILabel!
    
    @IBOutlet weak var stateDropDownView: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cityRequiredLabel: UILabel!
    
    @IBOutlet weak var address1TextField: UITextField!
    @IBOutlet weak var address1RequiredLabel: UILabel!
    
    @IBOutlet weak var address2TextField: UITextField!
    @IBOutlet weak var address2RequiredLabel: UILabel!
    
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var zipCodeRequiredLabel: UILabel!
    
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var phoneNumberRequiredTextField: UILabel!
    
    @IBOutlet weak var faxNumberTextField: UITextField!
    @IBOutlet weak var faxNumberRequiredLabel: UILabel!

    @IBOutlet weak var saveBtnBottomConstant: NSLayoutConstraint!
    
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    var customerAddressId = Int?()
    var requiredTextFieldArray = [UITextField]()
    var newBillingAddress: GenericBillingAddress?
    let countryDropDown = DropDown()
    let stateDropDown = DropDown()
    var selectedCountryId: Int?
    var selectedStateId: Int?
    var currentlyAvailabeStates = [BillingState]()
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()
        
    }
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.aPIManager = APIManager()
        self.selectedCountryId = 0
        self.selectedStateId = -1
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)

        
        
        countryDropDown.anchorView = self.countryDropDownView
        countryDropDown.bottomOffset = CGPoint(x: self.countryDropDownView.bounds.origin.x, y: self.countryDropDownView.bounds.origin.y + 50)
        
        countryDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            self.countryLabel.text = item
            
            let country = self.newBillingAddress!.availableCountries[index]
            
            self.selectedCountryId = Int(country.countryId! as String)
            self.selectedStateId = -1
            self.stateLabel.text = "Select state"
            
            self.stateDropDown.dataSource.removeAll()
            self.currentlyAvailabeStates.removeAll()
            
            if self.selectedCountryId == 0 {
                return
            }
            
            self.showLoading()
            self.aPIManager!.getBillingStates(self.selectedCountryId!,
                onSuccess: { billingStates in
                    self.hideLoading()
                    
                    self.currentlyAvailabeStates.appendContentsOf(billingStates)
                    for state in self.currentlyAvailabeStates {
                        self.stateDropDown.dataSource.append(state.stateName! as String)
                    }
                },
                onError: { message in
                    print(message)
                    self.hideLoading()
            })
        }
        
        stateDropDown.anchorView = self.stateDropDownView
        stateDropDown.bottomOffset = CGPoint(x: self.stateDropDownView.bounds.origin.x, y: self.stateDropDownView.bounds.origin.y + 50)
        stateDropDown.selectionAction = {
            [unowned self] (index, item) in
            
            let state = self.currentlyAvailabeStates[index]
            
            self.selectedStateId = Int(state.stateId! as String)
            self.stateLabel.text = item
        }
        
        if (self.newBillingAddress != nil) {
            self.populateNewAddressForm()
        }
        
        // Do any additional setup after loading the view.
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillShow:", name:UIKeyboardWillShowNotification, object: self.view.window)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"keyboardWillHide:", name:UIKeyboardWillHideNotification, object: self.view.window)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
 
       if (self.newBillingAddress == nil) {
        
        self.currentlyAvailabeStates.removeAll()
        self.countryDropDown.dataSource.removeAll()
        self.stateDropDown.dataSource.removeAll()
        self.requiredTextFieldArray.removeAll()
        self.countryLabel.text = "Select country"
        self.stateLabel.text = "Select state"
        self.selectedCountryId = 0
        self.selectedStateId = -1
        self.showLoading()
        self.aPIManager!.loadBillingAddressForm(
            onSuccess: { billingAddress in
                self.hideLoading()
                self.newBillingAddress = billingAddress.newBillingAddress!
                self.populateNewAddressForm()
            },
            onError: { message in
                print(message)
                self.hideLoading()
        })
      }
    }

    
    deinit
    {
        if self.view != nil{
            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillShowNotification, object: self.view.window)
            NSNotificationCenter.defaultCenter().removeObserver(self, name:UIKeyboardWillHideNotification, object: self.view.window)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }

        
    }
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    
    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    func populateNewAddressForm() {
        if let firstName = self.newBillingAddress!.firstName {
            self.firstNameTextField.text = firstName as String
        } else {
            self.firstNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.firstNameTextField)
        
        if let lastName = self.newBillingAddress!.lastName {
            self.lastNameTextField.text = lastName as String
        } else {
            self.lastNameTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.lastNameTextField)
        
        if let email = self.newBillingAddress!.email {
            self.emailTextField.text = email as String
        } else {
            self.emailTextField.text = ""
        }
        self.requiredTextFieldArray.append(self.emailTextField)
        
        if let company = self.newBillingAddress!.companyName {
            self.companyTextField.text = company as String
        } else {
            self.companyTextField.text = ""
        }
        self.companyRequiredLabel.hidden = true
        
        
        if self.newBillingAddress!.companyRequired {
            self.companyRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.companyTextField)
        }
        
        for country in self.newBillingAddress!.availableCountries {
            self.countryDropDown.dataSource.append(country.countryName! as String)
        }
        
        
        if let countryName = self.newBillingAddress!.countryName {
            self.countryLabel.text = countryName as String
            self.selectedCountryId = self.newBillingAddress!.countryId
        }
        
        
        if let stateName = self.newBillingAddress!.stateProvinceName {
            self.stateLabel.text = stateName as String
            self.selectedStateId = self.newBillingAddress!.stateProvinceId
        }

        
        if let city = self.newBillingAddress!.cityName {
            self.cityTextField.text = city as String
        } else {
            self.cityTextField.text = ""
        }
        self.cityRequiredLabel.hidden = true
        
        if self.newBillingAddress!.cityRequired {
            self.cityRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.cityTextField)
        }
        
        if let address1 = self.newBillingAddress!.address1 {
            self.address1TextField.text = address1 as String
        } else {
            self.address1TextField.text = ""
        }
        self.address1RequiredLabel.hidden = true
        if self.newBillingAddress!.streetAddressRequired {
            self.address1RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address1TextField)
        }
        
        if let address2 = self.newBillingAddress!.address2 {
            self.address2TextField.text = address2 as String
        } else {
            self.address2TextField.text = ""
        }
        self.address2RequiredLabel.hidden = true
        if self.newBillingAddress!.streetAddress2Required {
            self.address2RequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.address2TextField)
        }
        
        if let zipCode = self.newBillingAddress!.zipPostalCode {
            self.zipCodeTextField.text = zipCode as String
        } else {
            self.zipCodeTextField.text = ""
        }
        self.zipCodeRequiredLabel.hidden = true
        if self.newBillingAddress!.zipPostalCodeRequired {
            self.zipCodeRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.zipCodeTextField)
        }
        
        if let phone = self.newBillingAddress!.phoneNumber {
            self.phoneNumberTextField.text = phone as String
        } else {
            self.phoneNumberTextField.text = ""
        }
        self.phoneNumberRequiredTextField.hidden = true
        if self.newBillingAddress!.phoneRequired {
            self.phoneNumberRequiredTextField.hidden = false
            self.requiredTextFieldArray.append(self.phoneNumberTextField)
        }
        
        if let fax = self.newBillingAddress!.faxNumber {
            self.faxNumberTextField.text = fax as String
        } else {
            self.faxNumberTextField.text = ""
        }
        self.faxNumberRequiredLabel.hidden = true
        if self.newBillingAddress!.faxRequired {
            self.faxNumberRequiredLabel.hidden = false
            self.requiredTextFieldArray.append(self.faxNumberTextField)
        }
    }

    
    
    @IBAction func saveBtnAct(sender: AnyObject) {
        
        
    
                for textField in self.requiredTextFieldArray {
                    if textField.text?.characters.count == 0 {
                        self.showToast("Please fill up all the required fields.")
                        return
                    }
                }
                
                if self.selectedCountryId == 0 || self.selectedStateId == -1 {
                    self.showToast("Please fill up all the required fields.")
                    return
                }
                
                let firstNameDictionary = ["value":self.firstNameTextField.text!, "key":"Address.FirstName"]
                let lastNameDictionary = ["value":self.lastNameTextField.text!, "key":"Address.LastName"]
                let emailDictionary = ["value":self.emailTextField.text!, "key":"Address.Email"]
                let companyDictionary = ["value":self.companyTextField.text!, "key":"Address.Company"]
                let countryDictionary = ["value":String(self.selectedCountryId!), "key":"Address.CountryId"]
                let stateDictionary = ["value":String(self.selectedStateId!), "key":"Address.StateProvinceId"]
                let cityDictionary = ["value":self.cityTextField.text!, "key":"Address.City"]
                let address1Dictionary = ["value":self.address1TextField.text!, "key":"Address.Address1"]
                let address2Dictionary = ["value":self.address2TextField.text!, "key":"Address.Address2"]
                let zipCodeDictionary = ["value":self.zipCodeTextField.text!, "key":"Address.ZipPostalCode"]
                let phoneDictionary = ["value":self.phoneNumberTextField.text!, "key":"Address.PhoneNumber"]
                let faxDictionary = ["value":self.faxNumberTextField.text!, "key":"Address.FaxNumber"]
                
                let parameters = [firstNameDictionary, lastNameDictionary, emailDictionary, companyDictionary, countryDictionary, stateDictionary, cityDictionary, address1Dictionary, address2Dictionary, zipCodeDictionary, phoneDictionary, faxDictionary]
                print(parameters)
                
                self.showLoading()
        
        
        
        if self.customerAddressId != nil {
        
                self.aPIManager!.savedAddress(self.customerAddressId!, parameters: parameters,
                    onSuccess: {
                        self.hideLoading()
                        self.showToast("Successfully Updated Info")
                    },
                    onError: { message in
                        self.hideLoading()
                        print(message)
                        self.showToast(message)
                })
        }
        else{
            
            self.aPIManager!.addNewAddress(parameters,
                onSuccess: {
                    self.hideLoading()
                    self.showToast("Successfully Added Info")
                },
                onError: { message in
                    self.hideLoading()
                    print(message)
                    self.showToast(message)
            })
 
            
            
        }
    }
    

    
    
    @IBAction func countryDropDownTapped(sender: AnyObject) {
        if countryDropDown.hidden {
            countryDropDown.show()
        } else {
            countryDropDown.hide()
        }
    }
    
    @IBAction func stateDropDownTapped(sender: AnyObject) {
        if stateDropDown.hidden {
            stateDropDown.show()
        } else {
            stateDropDown.hide()
        }
    }

    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    
    func showToast(message:String) {
        
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view.window, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view.window, animated: true)
    }

    
    func keyboardWillHide(notif: NSNotification)
    {
        UIView.animateWithDuration(0.3) { () -> Void in
            self.saveBtnBottomConstant.constant = 10
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillShow(notif: NSNotification)
    {
        if let keyboardSize = (notif.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            UIView.animateWithDuration(0.3) { () -> Void in
                self.saveBtnBottomConstant.constant = keyboardSize.height + 10
                self.view.layoutIfNeeded()
            }
        }
    }

    
    // MARK: - UITextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(textField: UITextField) {
        print("TextField did end editing method called")
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        print("TextField should snd editing method called")
        return true;
    }
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("TextField should return method called")
        
        textField.resignFirstResponder();
        
        
        return true;
    }

}
