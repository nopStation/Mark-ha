//
//  CategoryDetailsCollectionViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 11/20/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class CategoryDetailsCollectionViewCell: UICollectionViewCell {

    var request: Alamofire.Request?
    
    @IBOutlet var AlbumImage : UIImageView?
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var amountLbl: UILabel!

}
