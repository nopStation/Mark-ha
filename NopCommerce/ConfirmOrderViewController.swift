//
//  ConfirmOrderViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class ConfirmOrderViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate, PayPalPaymentDelegate {

    @IBOutlet weak var containerScollView: UIScrollView!
    
    @IBOutlet weak var containerTableView: UITableView!
    //@IBOutlet weak var reviewOrderToConfirmLabel: UILabel!
    
    var aPIManager : APIManager?
    var checkOutOrderInfo = CheckOutOrderInfo! ()

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var containerTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var billingAddressLbl: UILabel!
    @IBOutlet weak var shippingAddressLbl: UILabel!
    
    @IBOutlet weak var subTotalLbl: UILabel!
    @IBOutlet weak var shippingLbl: UILabel!
    
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    var OrderId = NSInteger ()

    var pageMenu : CAPSPageMenu?
    
    
    var flag = Bool ()
    
    var payPalConfig = PayPalConfiguration()
    
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnectWithEnvironment(newEnvironment)
            }
        }
    }
    
    var acceptCreditCards: Bool = true {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }

    override func viewWillAppear(animated: Bool) {

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.containerScollView.hidden = true
        
        //reviewOrderToConfirmLabel.layer.cornerRadius = 4.0
        
        self.aPIManager = APIManager()

        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);

        //self.billingAddressLbl.estimatedRowHeight = 70.0
        //self.billingAddressLbl.rowHeight = UITableViewAutomaticDimension
        
        self.showLoading()
        
        self.aPIManager?.loadCheckOutOrderInformation(onSuccess: {
            gotcheckOutOrder in
            
            
            self.checkOutOrderInfo = gotcheckOutOrder
            
            
            print(self.checkOutOrderInfo)
            self.flag = true
            
          self.billingAddressLbl.text = "Billing Address"
          self.shippingAddressLbl.text = "Shipping Address"
            
            
           if self.checkOutOrderInfo.BillingAddress_FirstName?.length != nil {
            
            let billingAddress = "\(self.checkOutOrderInfo.BillingAddress_FirstName ?? "") \(self.checkOutOrderInfo.BillingAddress_LastName ?? "")\n\(self.checkOutOrderInfo.BillingAddress_Email ?? "")\n\(self.checkOutOrderInfo.BillingAddress_PhoneNumber ?? "")\n\(self.checkOutOrderInfo.BillingAddress_Address1 ?? ""), \(self.checkOutOrderInfo.BillingAddress_City ?? "")\n\(self.checkOutOrderInfo.BillingAddress_CountryName ?? "")"
            self.billingAddressLbl.text = billingAddress

            
            }

            
            if self.checkOutOrderInfo.ShippingAddress_FirstName?.length != nil {
 
            
                let shippingAddress = "\(self.checkOutOrderInfo.ShippingAddress_FirstName ?? "") \(self.checkOutOrderInfo.ShippingAddress_LastName ?? "")\n\(self.checkOutOrderInfo.ShippingAddress_Email ?? "")\n\(self.checkOutOrderInfo.ShippingAddress_PhoneNumber ?? "")\n\(self.checkOutOrderInfo.ShippingAddress_Address1 ?? ""), \(self.checkOutOrderInfo.ShippingAddress_City ?? "")\n\(self.checkOutOrderInfo.ShippingAddress_CountryName ?? "")"
                print(shippingAddress)
                
                
                self.shippingAddressLbl.text = shippingAddress
            }
            
            self.subTotalLbl.text    = self.checkOutOrderInfo.SubTotal as? String
            self.shippingLbl.text    = self.checkOutOrderInfo.Shipping as? String
            self.taxLbl.text         = self.checkOutOrderInfo.Tax as? String
            self.discountLbl.text    = self.checkOutOrderInfo.SubTotalDiscount as? String
            self.totalLbl.text       = self.checkOutOrderInfo.OrderTotal as? String

            
            self.containerTableViewHeight.constant = CGFloat(self.checkOutOrderInfo.Items.count) * 150.0
            
            self.containerTableView.reloadData()
            
            self.hideLoading()
            
            self.containerScollView.hidden = false

            print(gotcheckOutOrder)
            
            
            }, onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
        

        // Do any additional setup after loading the view.
    }

    
    
    
    @IBAction func confirmBtnAct(sender: AnyObject) {
        
        //let authorizedDotNetView = AuthorizedDotNetViewController(nibName: "AuthorizedDotNetViewController", bundle: nil)
        //authorizedDotNetView.OrderId  = self.OrderId
        //self.parentViewController?.presentViewController(authorizedDotNetView, animated: true, completion: nil)
        
        self.showLoading()
       
        self.aPIManager?.checkOutComplete(onSuccess: {
            checkOutConplete in
            
            self.hideLoading()

            let PaymentType = checkOutConplete["PaymentType"].intValue
            self.OrderId = checkOutConplete["OrderId"].intValue
            
            let completeOrder = checkOutConplete["CompleteOrder"].intValue

            
            if  completeOrder == 1  {
                
               let alertMessage = String(format: "Your Order is Complete.\nYour Order Id : %d",self.OrderId)
                
                let alertController = UIAlertController(title: "Congratulations", message: alertMessage, preferredStyle: .Alert)

                let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
                    
                    APIManagerClient.sharedInstance.shoppingCartCount = 0
                    NSNotificationCenter.defaultCenter().postNotificationName("com.notification.backHomePage", object: nil);
    
                }
                alertController.addAction(OKAction)
                
                self.presentViewController(alertController, animated: true, completion:nil)
                
            }
            
            else if PaymentType == 2 {
                
              PayPalMobile.initializeWithClientIdsForEnvironments([PayPalEnvironmentProduction: checkOutConplete["PayPal"]["ClientId"].string!, PayPalEnvironmentSandbox: checkOutConplete["PayPal"]["ClientId"].string!])
                
                
                self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
                //self.payPalConfig.merchantName = "Siva Ganesh Inc."
                //self.payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.sivaganesh.com/privacy.html")
                //self.payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.sivaganesh.com/useragreement.html")
                self.payPalConfig.languageOrLocale = NSLocale.preferredLanguages()[0]
                self.payPalConfig.payPalShippingAddressOption = .PayPal;
                PayPalMobile.preconnectWithEnvironment(self.environment)
                
                self.PaymentMethodVarification()
   
            }
            else if PaymentType == 3 {

                let authorizedDotNetView = AuthorizedDotNetViewController(nibName: "AuthorizedDotNetViewController", bundle: nil)
                authorizedDotNetView.OrderId  = self.OrderId
                self.presentViewController(authorizedDotNetView, animated: true, completion: nil)
                
            }
            else if PaymentType == 4 {
                
                
                let checkOutRedirectPayment = (self.storyboard?.instantiateViewControllerWithIdentifier("CheckOutRedirectPaymentViewController"))! as! CheckOutRedirectPaymentViewController
                checkOutRedirectPayment .orderId  = self.OrderId
                self.presentViewController(checkOutRedirectPayment, animated: true, completion: nil)

            }


            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)
        })
   
    }
    
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        
        if buttonIndex == 1 {
            
          
            
        }
      
        
    }
    
    func PaymentMethodVarification() {
        
        
        let newString = self.totalLbl.text!.stringByReplacingOccurrencesOfString("$", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let newString2 = newString.stringByReplacingOccurrencesOfString(",", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        print(newString2)
        
        let item1 = PayPalItem(name: "", withQuantity: 1, withPrice: NSDecimalNumber(string: newString2), withCurrency: "USD", withSku: "SivaGanesh-0001")
    
        
        let items = [item1]
        let subtotal = PayPalItem.totalPriceForItems(items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.decimalNumberByAdding(shipping).decimalNumberByAdding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "USD", shortDescription: "Total Price", intent: .Sale)
        
        //payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            presentViewController(paymentViewController, animated: true, completion: nil)
        }
        else {
            
            print("Payment not processable: \(payment)")
        }
        
       }
    
    
    func showToast(message:String) {
        
        let globalHud = MBProgressHUD.showHUDAddedTo(appDelegate.window, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(appDelegate.window, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(appDelegate.window, animated: true)
    }

    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.flag {
      return self.checkOutOrderInfo.Items.count
        }
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
            var cell: ConfirmOrderTableViewCell! = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            
            if cell == nil {
                tableView.registerNib(UINib(nibName: "ConfirmOrderTableViewCell", bundle: nil), forCellReuseIdentifier: "ConfirmOrderTableViewCell")
                cell = tableView.dequeueReusableCellWithIdentifier("ConfirmOrderTableViewCell") as? ConfirmOrderTableViewCell
            }
        
//            cell.itemImageView.layer.borderWidth=1.0
//            cell.itemImageView.layer.masksToBounds = false
//            cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
//            cell.itemImageView.layer.cornerRadius = 13
//            cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
//            cell.itemImageView.clipsToBounds = true
        
        
        cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
        cell.backgroundColor = UIColor.whiteColor()
        cell.itemImageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        cell.itemImageView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.itemImageView.layer.shadowOpacity = 0.70
        cell.itemImageView.layer.shadowRadius = 4
        cell.itemImageView.layer.masksToBounds = true
        cell.clipsToBounds = false
        cell.itemImageView.layer.borderWidth = 1
        cell.itemImageView.layer.borderColor = UIColor.grayColor().CGColor

        
        
           //cell.itemImageView!.image = nil
        
            let imageUrl = self.checkOutOrderInfo.Items[indexPath.row].ImageUrl
            print(imageUrl)
        
            cell.itemImageView!.sd_setImageWithURL(NSURL(string: imageUrl!))
//            Alamofire.request( .GET,  imageUrl! ).response{
//                (
//                request, response, data, error) in
//                cell.itemImageView!.image = UIImage(data: data!, scale:1)
//            }
        
            cell.titleLbl.text = self.checkOutOrderInfo.Items[indexPath.row].ProductName as String
            cell.UnitPrice.text = self.checkOutOrderInfo.Items[indexPath.row].UnitPrice as String
            cell.subTotal.text = self.checkOutOrderInfo.Items[indexPath.row].SubTotal as String
            cell.quantity.text = String(format: "Quantity:%d", self.checkOutOrderInfo.Items[indexPath.row].Quantity)
            cell.selectionStyle = UITableViewCellSelectionStyle.None
            print(self.checkOutOrderInfo.Items[indexPath.row].Quantity)

            //self.roundRectToAllView(cell.containerView)
        
            if (indexPath.row == self.checkOutOrderInfo.Items.count-1) {
                cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            }
        
            return cell
        }

    
    
    
    func roundRectToAllView(view: UIView )->Void{
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor.whiteColor()
        view.layer.shadowColor = UIColor.darkGrayColor().CGColor
        view.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        view.layer.shadowOpacity = 0.50
        view.layer.shadowRadius = 1
        view.layer.masksToBounds = true
        view.clipsToBounds = false
        
        
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
//        if tableView == containerTableView {
//            let productDetailsViewController = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
//            productDetailsViewController.productId  = self.checkOutOrderInfo.Items[indexPath.row].ProductId
//            self.navigationController!.pushViewController(productDetailsViewController, animated: true)
//            
//            
//        }
        
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(paymentViewController: PayPalPaymentViewController!) {
        print("PayPal Payment Cancelled")
        
        paymentViewController?.dismissViewControllerAnimated(true, completion: {
            () -> Void in
            // send completed confirmaion to your server
            
            APIManagerClient.sharedInstance.shoppingCartCount = 0

            let array = (self.navigationController?.viewControllers)! as NSArray
            self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
            
        })
        
    }
    
    func payPalPaymentViewController(paymentViewController: PayPalPaymentViewController!, didCompletePayment completedPayment: PayPalPayment!) {
        
        print("PayPal Payment Success !")
 
        print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")

        
        
        let dic = completedPayment.confirmation["response"] as! NSDictionary
        let paymentId  = dic.valueForKey("id") as! NSString
        
        print(paymentId)
        
        
        paymentViewController?.dismissViewControllerAnimated(true, completion:nil)
        
        /*
        [client: {
        environment = sandbox;
        "paypal_sdk_version" = "2.12.9";
        platform = iOS;
        "product_name" = "PayPal iOS SDK";
        }, response_type: payment, response: {
        "create_time" = "2016-01-08T12:20:58Z";
        id = "PAY-8RK22991L8553672XK2H2TGQ";
        intent = sale;
        state = approved;
        }]*/
        
        self.showLoading()

        aPIManager?.paypalConfirm(paymentId, orderId: self.OrderId, onSuccess: {

            self.hideLoading()
            self.showToast("PayPal Payment Successfull!")
            
            APIManagerClient.sharedInstance.shoppingCartCount = 0
            let array = (self.navigationController?.viewControllers)! as NSArray
            self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
            
            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)

                APIManagerClient.sharedInstance.shoppingCartCount = 0
                
                let array = (self.navigationController?.viewControllers)! as NSArray
                self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
                
        })
    }

}
