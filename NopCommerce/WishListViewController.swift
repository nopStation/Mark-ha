//
//  WishListViewController.swift
//  NopCommerce
//
//  Created by Masudur Rahman on 1/12/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import DropDown
import Alamofire

class WishListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var wishListTableView: UITableView!
    @IBOutlet weak var wishListTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalCartLabel: CustomLabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    var rightMenu : GlobalRightMenu?
    var aPIManager : APIManager?
    var wishListArray : [WishListItems] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.containerView.hidden = true
        self.aPIManager = APIManager()
        
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        self.loadWishList()
        
    }
    
    func loadWishList() {
    
        self.showLoading()
        aPIManager!.loadWishList(
            onSuccess: {
                wishListItems in
                self.hideLoading()
                
                self.wishListArray = wishListItems
                if self.wishListArray.count == 0 {
                    self.showToast("Your wish list is empty")
                } else {
                    self.containerView.hidden = false
                    self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count * 150)
                    self.wishListTableView.reloadData()
                }
                
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
        
    }
    override func viewWillAppear(animated: Bool) {
        self.reloadDropDownMenuList()
    }
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
    }
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    @IBAction func rightOptionBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
        
    }
    
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
    }
    @IBAction func searchBtnAct(sender: AnyObject) {
        
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController!.pushViewController(searchVC, animated: true)
    }
    @IBAction func dismissView(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func backToRootView(sender: UIButton) {
        
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }
    @IBAction func addToCartFromWishList(sender: AnyObject) {
        
        let dicArray = NSMutableArray()
        for item in self.wishListArray {
            let parameters : Dictionary <String, AnyObject> = ["value": item.ProductId, "key": "addtocart"]
            dicArray.addObject(parameters)
        }
    
        print(dicArray)
        
        self.showLoading()
        aPIManager?.addProductFromWishlistToShoppingCart(dicArray,
            onSuccess: {
                
                self.hideLoading()
                
                self.updateShoppingCartCount()
                
                self.wishListArray.removeAll()
                
                self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count) * 150
                self.containerView.hidden = true
                self.showToast("Wishlist items added to cart successfully")
                
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
        
    }
    
    // MARK: - TableView Delegate and DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.wishListArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell : WishListTableViewCell = tableView.dequeueReusableCellWithIdentifier("WishListTableViewCell") as! WishListTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: "deleteBtnAction:", forControlEvents: UIControlEvents.TouchUpInside)

        
        cell.titleLabel.text = self.wishListArray[indexPath.row].ProductName as String
        cell.amountLabel.text = self.wishListArray[indexPath.row].UnitPrice as String
        
        cell.itemImageView.layer.borderWidth=1.0
        cell.itemImageView.layer.masksToBounds = false
        cell.itemImageView.layer.borderColor = UIColor.whiteColor().CGColor
        cell.itemImageView.layer.cornerRadius = 13
        cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.height/2
        cell.itemImageView.clipsToBounds = true
        
        cell.itemImageView.image = nil
        
        let imageUrl = self.wishListArray[indexPath.row].ImageUrl!
        Alamofire.request(.GET, imageUrl ).response {
            (request, response, data, error) in
            cell.itemImageView.image = UIImage(data: data!, scale:1)
        }
        
        cell.containerView.layer.cornerRadius = 8
        cell.containerView.layer.masksToBounds = true
        cell.containerView.layer.shadowColor = UIColor.darkGrayColor().CGColor
        cell.containerView.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        cell.containerView.layer.shadowOpacity = 0.5
        cell.containerView.layer.shadowRadius = 1
        cell.containerView.layer.masksToBounds = true
        cell.containerView.clipsToBounds = false
        
        return cell
    }
    
//    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
//        
//        return true
//        
//    }
//    
//    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
//        
//        if editingStyle == UITableViewCellEditingStyle.Delete {
//                
//             let parameters : Dictionary <String, AnyObject> = ["value": self.wishListArray[indexPath.row].ProductId, "key": "removefromcart"]
//            
//             let dicArray = NSMutableArray()
//             dicArray.addObject(parameters)
//             print(dicArray)
//            
//             self.showLoading()
//             aPIManager?.deleteAndUpdateWishList(dicArray,
//                    onSuccess: {
//                        self.hideLoading()
//                        
//                        self.wishListArray.removeAtIndex(indexPath.row)
//                        if self.wishListArray.count == 0 {
//                            self.wishListTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//                            self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count) * 200.0
//                            self.containerView.hidden = true
//                            self.showToast("Your wishlist is empty")
//                        } else {
//                            self.wishListTableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//                            self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count) * 200.0
//                        }
//                    },
//                    onError: {
//                        message in
//                        self.hideLoading()
//                        self.showToast(message)
//                    })
//        }
//        
//    }
    
    
    func deleteBtnAction(sender:UIButton!)
    {

        let parameters : Dictionary <String, AnyObject> = ["value": self.wishListArray[sender.tag].ProductId, "key": "removefromcart"]
        
        let dicArray = NSMutableArray()
        dicArray.addObject(parameters)
        print(dicArray)
        
        self.showLoading()
        aPIManager?.deleteAndUpdateWishList(dicArray,
            onSuccess: {
                self.hideLoading()
               
                let pointInTable: CGPoint = sender.convertPoint(sender.bounds.origin, toView: self.wishListTableView)
                let cellIndexPath = self.wishListTableView.indexPathForRowAtPoint(pointInTable)
                
                self.wishListArray.removeAtIndex(sender.tag)
                if self.wishListArray.count == 0 {
                    self.wishListTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                    self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count) * 150
                    self.containerView.hidden = true
                    self.showToast("Your wishlist is empty")
                } else {
                    self.wishListTableView.deleteRowsAtIndexPaths([cellIndexPath!], withRowAnimation: UITableViewRowAnimation.Automatic)
                    self.wishListTableViewHeightConstraint.constant = CGFloat(self.wishListArray.count) * 150
                }
                

                self.wishListTableView.performSelector(#selector(UITableView.reloadData), withObject: nil, afterDelay: 0.5)
 
                
            },
            onError: {
                message in
                self.hideLoading()
                self.showToast(message)
        })
    }

    
}
