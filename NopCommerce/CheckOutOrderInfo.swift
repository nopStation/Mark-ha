//
//  CheckOutOrderInfo.swift
//  NopCommerce
//
//  Created by BS-125 on 12/22/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class CheckOutOrderInfo: NSObject {
    
    var ShippingMethod       :                NSString?
    var PaymentMethod :                       NSString?

    var SubTotal                         :    NSString?
    var SubTotalDiscount                 :    NSString?
    var Shipping                         :    NSString?
    var Tax                              :    NSString?
    var RequiresShipping                 :    Bool?
    var OrderTotal                       :    NSString?

    
    var ShippingAddress_Address1        :     NSString?
    var ShippingAddress_City            :     NSString?
    var ShippingAddress_Email           :     NSString?
    var ShippingAddress_LastName :            NSString?
    var ShippingAddress_CountryName     :     NSString?
    var ShippingAddress_StateProvinceName :   NSString?
    var ShippingAddress_ZipPostalCode :       NSString?
    var ShippingAddress_PhoneNumber :         NSString?
    var ShippingAddress_FirstName :           NSString?


    
    var BillingAddress_Address1        :      NSString?
    var BillingAddress_City :                 NSString?
    var BillingAddress_Email         :        NSString?
    var BillingAddress_LastName :             NSString?
    var BillingAddress_CountryName    :       NSString?
    var BillingAddress_StateProvinceName :    NSString?
    var BillingAddress_ZipPostalCode :        NSString?
    var BillingAddress_PhoneNumber :          NSString?
    var BillingAddress_FirstName :            NSString?
    
    
    
    var Items                         :       [CheckOutItems]
    var checkoutAttributes             :      [CheckoutAttributes]
    
    
    
    
    
    
    init (JSON : AnyObject) {
        
        
        ShippingMethod                            =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingMethod") as? NSString)
        PaymentMethod                             =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.PaymentMethod") as? NSString)
        
        
        
        SubTotal                                  = (JSON.valueForKeyPath("OrderTotalModel.SubTotal") as? NSString)
        SubTotalDiscount                          = (JSON.valueForKeyPath("OrderTotalModel.SubTotalDiscount") as? NSString)
        Shipping                                  = (JSON.valueForKeyPath("OrderTotalModel.Shipping") as? NSString)
        OrderTotal                                = (JSON.valueForKeyPath("OrderTotalModel.OrderTotal") as? NSString)

        Tax                                       = (JSON.valueForKeyPath("OrderTotalModel.Tax") as? NSString)
        RequiresShipping                          = (JSON.valueForKeyPath("OrderTotalModel.RequiresShipping") as? Bool)

        
        
        
        
        ShippingAddress_Address1                  =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.Address1") as? NSString)
        ShippingAddress_City                      =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.City") as? NSString)
        ShippingAddress_Email                     =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.Email") as? NSString)
        ShippingAddress_LastName                  =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.LastName") as? NSString)
        ShippingAddress_CountryName               =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.CountryName") as? NSString)
        ShippingAddress_StateProvinceName         =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.StateProvinceName") as? NSString)
        ShippingAddress_ZipPostalCode             =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.ZipPostalCode") as? NSString)
        ShippingAddress_PhoneNumber               =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.PhoneNumber") as? NSString)
        ShippingAddress_FirstName                 =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.ShippingAddress.FirstName") as? NSString)
        
        
        BillingAddress_Address1                    =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.Address1") as? NSString)
        BillingAddress_City                        =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.City") as? NSString)
        BillingAddress_Email                       =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.Email") as? NSString)
        BillingAddress_LastName                    =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.LastName") as? NSString)
        BillingAddress_CountryName                 =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.CountryName") as? NSString)
        BillingAddress_StateProvinceName           =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.StateProvinceName") as? NSString)
        BillingAddress_ZipPostalCode               =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.ZipPostalCode") as? NSString)
        BillingAddress_PhoneNumber                 =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.PhoneNumber") as? NSString)
        BillingAddress_FirstName                   =  (JSON.valueForKeyPath("ShoppingCartModel.OrderReviewData.BillingAddress.FirstName") as? NSString)

        
        Items                                      = [CheckOutItems] ()
        checkoutAttributes                         = [CheckoutAttributes] ()
        
        
        for data in (JSON.valueForKeyPath("ShoppingCartModel.Items") as! NSArray){
            
            let checkOut = CheckOutItems(JSON: data)
            self.Items.append(checkOut)
            
        }
        
        
        for data in (JSON.valueForKeyPath("ShoppingCartModel.CheckoutAttributes") as! NSArray){
            
            let attributs = CheckoutAttributes(JSON: data)
            self.checkoutAttributes.append(attributs)
            
        }
        
    }


}
