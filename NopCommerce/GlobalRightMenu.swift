//
//  GlobalRightMenu.swift
//  NopCommerce
//
//  Created by BS-125 on 2/9/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit
import DropDown
import MBProgressHUD

class GlobalRightMenu: NSObject {

    let dropDown = DropDown()
    var dropDownContainerView = UIView()
    var containingViewController = UIViewController()
    
    init (dropDownContainerView : UIView, containingViewController : UIViewController) {
        
        super.init()
        
        self.dropDown.selectionBackgroundColor = UIColor.whiteColor()
        
        self.dropDownContainerView = dropDownContainerView
        self.containingViewController = containingViewController
        
        self.dropDown.anchorView = self.dropDownContainerView
        self.dropDown.bottomOffset = CGPoint(x: self.dropDownContainerView.frame.origin.x-100, y:self.dropDownContainerView.frame.origin.y-44)
        
        self.dropDown.selectionAction = {
            [unowned self] (index, item) in
            
            print(item)
            
            let defaults = NSUserDefaults.standardUserDefaults()
            if (defaults.stringForKey("accessToken") != nil) {
                
                if index == 0 {
                    
                    self.showToast("You are Logged Out");
                    defaults.removeObjectForKey("accessToken")
                    self.containingViewController.performSelector("reloadDropDownMenuList", withObject: nil, afterDelay:0)
                    self.containingViewController.performSelector("backToRootView:", withObject: nil, afterDelay:2)
                    
                    
                } else if index == 1 {
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let myAccViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("MyAccountViewController") as! MyAccountViewController
                    self.containingViewController.navigationController?.pushViewController(myAccViewC, animated: true)
                    
                } else if index == 2 {
                    
                    let ordersViewC = AddressViewController(nibName: "AddressViewController", bundle: nil)
                    self.containingViewController.navigationController!.pushViewController(ordersViewC, animated: true)
                    
                } else if index == 3 {
                    
                    let ordersViewC = OrdersViewController(nibName: "OrdersViewController", bundle: nil)
                    self.containingViewController.navigationController!.pushViewController(ordersViewC, animated: true)
                    
                } else if index == 4 {
                    
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let wishListViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("WishListViewController") as! WishListViewController
                    self.containingViewController.navigationController?.pushViewController(wishListViewC, animated: true)
            
                } else if index == 5 {
                
                    self.navigateToSettingsPage()
                    
                } else if index == 6 {
                    
                    self.navigateToContactUsPage()
                }
                else if index == 7 {
                    
                    self.navigateToBarcodeReaderPage()
                    
                }
            }
            else {
                
                if index == 5 {
                
                    self.navigateToSettingsPage()
                    
                } else if index == 6 {
                    
                    self.navigateToContactUsPage()
                    
                } else if index == 7 {
                    
                    self.navigateToBarcodeReaderPage()
                    
                } else {
                
                    let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                    let signInViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SignInViewController") as! SignInViewController
                    self.containingViewController.navigationController?.pushViewController(signInViewC, animated: true)
                }
            }
        }
    }
    
    func navigateToSettingsPage() {
    
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let settingsViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("SettingsViewController") as! SettingsViewController
        self.containingViewController.navigationController?.pushViewController(settingsViewC, animated: true)
    }
    
    func navigateToBarcodeReaderPage() {
       
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.BarcodeReader", object: nil);
        
    }

    
    func navigateToContactUsPage() {
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let contactViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("ContactViewController") as! ContactViewController
        self.containingViewController.navigationController?.pushViewController(contactViewC, animated: true)
    }
    
    func reloadDropDownMenuList() {
        
        dropDown.dataSource.removeAll()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if let token = defaults.stringForKey("accessToken") {
            dropDown.dataSource.append(NSLocalizedString("SignOut", comment: ""))
            print(token)
        }
        else {
            dropDown.dataSource.append(NSLocalizedString("SignIn", comment: ""))
        }
        
        dropDown.dataSource.append(NSLocalizedString("MyAccount", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("Address", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("MyOrders", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("Wishlist", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("Settings", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("ContactUs", comment: ""))
        dropDown.dataSource.append(NSLocalizedString("BarCodeRead", comment: ""))
        
    }
    
    func hideLoading() {
        MBProgressHUD.hideHUDForView(self.containingViewController.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.containingViewController.view, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.containingViewController.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }

}
