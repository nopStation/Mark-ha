//
//  ShippingMethodViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD

class ShippingMethodViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{

    
    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    //@IBOutlet weak var tableViewContainerHeightConstraint: NSLayoutConstraint!
    
    var aPIManager : APIManager?
    var shippingMethodArray = [ShippingMethod]()
    var selectedRow = NSInteger ()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.headerView.hidden = true
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        self.containerTableView.estimatedRowHeight = 60.0
        //self.containerTableView.rowHeight = UITableViewAutomaticDimension
        
        self.aPIManager = APIManager()
        
        
        self.showLoading()
        self.aPIManager?.getShippingMethod(onSuccess: {
            gotShippingMethod in
        
            self.roundHeaderView()
            self.headerView.hidden = false
        
            self.shippingMethodArray = gotShippingMethod
            
            self.containerTableView.reloadData()
            print(self.shippingMethodArray.count)
            
            self.decorateContainerTableView()
            
            self.hideLoading()
            
            }, onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func roundHeaderView()
    {
        self.roundView(self.headerView, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight))
    }
    
    func decorateContainerTableView()
    {
//        self.containerTableView.layoutIfNeeded()
//        
//        if (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) > 0
//        {
//            self.tableViewBottomConstraint.constant = (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) + 10
//        }
//        
//        self.containerTableView.layoutIfNeeded()
        
        self.roundView(self.containerTableView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
        
        self.containerTableView.layer.borderWidth = 2
        self.containerTableView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func roundView(aView: UIView, byRoundingCorners corners: UIRectCorner)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(4, 4)).CGPath
        aView.layer.mask = maskLayer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.shippingMethodArray.count > 0 {
        return self.shippingMethodArray.count
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       let cell:ShippingMethodTableViewCell! = tableView.dequeueReusableCellWithIdentifier("cell") as? ShippingMethodTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
//        if indexPath.row == self.shippingMethodArray.count - 1
//        {
//            self.roundView(cell.containerView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
//        }
        
        cell.titleLbl.text = self.shippingMethodArray[indexPath.row].Name as String
        cell.descripTionLbl.text = self.shippingMethodArray[indexPath.row].Description as String
        cell.selectedBtn.tag = indexPath.row
        cell.selectedBtn.addTarget(self, action: "buttonActionForSelectedRow:", forControlEvents: UIControlEvents.TouchUpInside)
        
        if self.shippingMethodArray[indexPath.row].Selected == 1 {
            
          cell.containerView.backgroundColor = UIColor.clearColor()
          cell.selectedBtn.selected = true
            self.selectedRow = indexPath.row
        } else {
            cell.containerView.backgroundColor = UIColor.whiteColor()
            cell.selectedBtn.selected = false
            
        }
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        self.selectedRow = indexPath.row

        
        for checkSelectedIndex in self.shippingMethodArray{
            checkSelectedIndex.Selected = 0
            
        }
        
        self.shippingMethodArray[indexPath.row].Selected = 1
        self.containerTableView.reloadData()

        
    }

    
    func buttonActionForSelectedRow(sender:UIButton!)
    {
        
        self.selectedRow = sender.tag

        
        for checkSelectedIndex in self.shippingMethodArray{
           checkSelectedIndex.Selected = 0
            
        }
        
        self.shippingMethodArray[sender.tag].Selected = 1
        
        self.containerTableView.reloadData()
        
    }
    
    
    @IBAction func continueToNextPage(sender: AnyObject) {
        
        if self.shippingMethodArray.count > 0 {

        self.showLoading()
        let str = String(format: "%@___%@", self.shippingMethodArray[selectedRow].Name,self.shippingMethodArray[selectedRow].ShippingRateComputationMethodSystemName)
        
        aPIManager?.setShippingMethods(str, onSuccess: {
            
            self.hideLoading()
            self.showToast("Shipping method saved successfully")

            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 2);
            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)
        })
        }else {
            
            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 2);
            
        }


        
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
