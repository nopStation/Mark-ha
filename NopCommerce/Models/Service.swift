//
//  Service.swift
//  NopCommerce
//
//  Created by Jahid Hassan on 10/12/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import MBProgressHUD


import Foundation
import Alamofire
import SwiftyJSON

enum KeyFields: String {
    case appID = "appID"
    case jsKey = "jsKey"
    case clientKey = "clientKey"
}


extension UIImageView {
    
    public func imageFromUrl(urlString: String) {
        if let url = NSURL(string: urlString) {
            let request = NSURLRequest(URL: url)
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
                (response: NSURLResponse?, data: NSData?, error: NSError?) -> Void in
                self.image = UIImage(data: data!)
            }
        }
    }
}


class PhotoInfo: NSObject {
    let id: Int
    let url: String
    
    var name: String?
    
    var favoritesCount: Int?
    var votesCount: Int?
    var commentsCount: Int?
    
    var highest: Float?
    var pulse: Float?
    var views: Int?
    var camera: String?
    var desc: String?
    
    init(id: Int, url: String) {
        self.id = id
        self.url = url
    }
    
    required init(response: NSHTTPURLResponse, representation: AnyObject) {
        self.id = representation.valueForKeyPath("photo.id") as! Int
        self.url = representation.valueForKeyPath("photo.image_url") as! String
        
        self.favoritesCount = representation.valueForKeyPath("photo.favorites_count") as? Int
        self.votesCount = representation.valueForKeyPath("photo.votes_count") as? Int
        self.commentsCount = representation.valueForKeyPath("photo.comments_count") as? Int
        self.highest = representation.valueForKeyPath("photo.highest_rating") as? Float
        self.pulse = representation.valueForKeyPath("photo.rating") as? Float
        self.views = representation.valueForKeyPath("photo.times_viewed") as? Int
        self.camera = representation.valueForKeyPath("photo.camera") as? String
        self.desc = representation.valueForKeyPath("photo.description") as? String
        self.name = representation.valueForKeyPath("photo.name") as? String
    }
    
    override func isEqual(object: AnyObject!) -> Bool {
        return (object as! PhotoInfo).id == self.id
    }
    
    override var hash: Int {
        return (self as PhotoInfo).id
    }
}




class Category : NSObject
{
    let Id: Int
    let Name: String
    let ParentCategoryId: Int
    
    init(id: Int, name: String, parentId: Int)
    {
        self.Id = id
        self.Name = name
        self.ParentCategoryId = parentId
    }
}

class CategoryDetails : NSObject
{
    let Id: Int
    let Name: String
    let ShortDescription: String
    let OldPrice: String
    let Price: String
    let AvailableForPreOrder: Bool
    let ImageUrl: String
    
    init(JSON: AnyObject)
    {
        self.Id = JSON.valueForKeyPath("Id") as! Int
        self.Name = JSON.valueForKeyPath("Name") as! String
        self.ShortDescription = JSON.valueForKeyPath("ShortDescription") as! String
        
        if let prevPrice = JSON.valueForKeyPath("ProductPrice.OldPrice") as? String
        {
            self.OldPrice = prevPrice
        }
        else
        {
            self.OldPrice = ""
        }
        
        self.Price = JSON.valueForKeyPath("ProductPrice.Price") as! String
        self.AvailableForPreOrder = JSON.valueForKeyPath("ProductPrice.AvailableForPreOrder") as! Bool
        self.ImageUrl = JSON.valueForKeyPath("DefaultPictureModel.ImageUrl") as! String
    }
}

class ProductDetail : NSObject
{
    let Name: String
    let ShortDescription: String
    let FullDescription: String
    let OldPrice: String
    let Price: String
    let manufacturerName: String
    
    init(name: String, shortDesc: String, fullDesc: String, productPrice: AnyObject, productManufacturer: AnyObject)
    {
        self.Name = name
        self.ShortDescription = shortDesc
        self.FullDescription = fullDesc
        self.Price = productPrice.valueForKey("Price") as! String
        
        if let prevPrice = productPrice.valueForKey("OldPrice") as? String
        {
            self.OldPrice = prevPrice
        }
        else
        {
            self.OldPrice = ""
        }
        
        self.manufacturerName = productManufacturer.objectAtIndex(0).valueForKey("Name") as! String
    }
}

class AssociatedProducts : NSObject
{
    let Id: Int
    let Name: String
    let ShortDescription: String
    let OldPrice: String
    let Price: String
    let ImageUrl: String
    
    init(JSON: AnyObject)
    {
        self.Id = JSON.valueForKeyPath("ProductPrice.ProductId") as! Int
        self.Name = JSON.valueForKeyPath("Name") as! String
        self.ShortDescription = JSON.valueForKeyPath("ShortDescription") as! String
        
        if let prevPrice = JSON.valueForKeyPath("ProductPrice.OldPrice") as? String
        {
            self.OldPrice = prevPrice
        }
        else
        {
            self.OldPrice = ""
        }
        
        self.Price = JSON.valueForKeyPath("ProductPrice.Price") as! String
        self.ImageUrl = JSON.valueForKeyPath("DefaultPictureModel.ImageUrl") as! String
    }
}

class PictureModel: NSObject
{
    let ImageUrl: String
    let FullSizeImageUrl: String
    
    init(JSON: AnyObject)
    {
        self.ImageUrl = JSON.valueForKeyPath("ImageUrl") as! String
        self.FullSizeImageUrl = JSON.valueForKeyPath("FullSizeImageUrl") as! String
    }
}
