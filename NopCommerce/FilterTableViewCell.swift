//
//  FilterTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 1/4/16.
//  Copyright © 2016 Jahid Hassan. All rights reserved.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var itemScrollView: UIScrollView!
    @IBOutlet weak var itemName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
