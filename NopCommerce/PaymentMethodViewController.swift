//
//  PaymentMethodViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/21/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class PaymentMethodViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{

    var aPIManager : APIManager?
    var paymentMethodArray = [PaymentMethod]()
    var selectedRow = NSInteger ()

    @IBOutlet weak var containerTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.headerView.hidden = true
        
        self.aPIManager = APIManager()
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.accessNextPage", object: nil);
        
        self.showLoading()
        self.aPIManager?.getPaymentMethod(onSuccess: {
            gotShippingMethod in
            
            self.roundHeaderView()
            self.headerView.hidden = false
            
            self.paymentMethodArray = gotShippingMethod
            
            self.containerTableView.reloadData()
            print(gotShippingMethod)
            
            self.decorateContainerTableView()
            
            self.hideLoading()
            
            }, onError: { message in
                print(message)
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })
    }
    
    func roundHeaderView()
    {
        self.roundView(self.headerView, byRoundingCorners: UIRectCorner.TopLeft.union(.TopRight))
    }
    
    func decorateContainerTableView()
    {
        self.containerTableView.layoutIfNeeded()
        
        if (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) > 0
        {
            self.tableViewBottomConstraint.constant = (self.containerTableView.frame.size.height - self.containerTableView.contentSize.height) + 10
        }
        
        self.containerTableView.layoutIfNeeded()
        
        self.roundView(self.containerTableView, byRoundingCorners: UIRectCorner.BottomLeft.union(.BottomRight))
        
        self.containerTableView.layer.borderWidth = 2
        self.containerTableView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func roundView(aView: UIView, byRoundingCorners corners: UIRectCorner)
    {
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(4, 4)).CGPath
        aView.layer.mask = maskLayer
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return self.paymentMethodArray.count
            
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // let shippingMethod = self.shippingMethodArray[indexPath.row] as ShippingMethod
        // let index = indexPath.row as Int
        // print(index)
        
        
        let cell:PaymentMethodTableViewCell = tableView.dequeueReusableCellWithIdentifier("cell") as! PaymentMethodTableViewCell
//        if(cell == nil)
//        {
//            cell = PaymentMethodTableViewCell(style: UITableViewCellStyle.Subtitle,reuseIdentifier:"cell")
//        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.containerView.layer.cornerRadius = 4.0

        cell.titleLbl.text = self.paymentMethodArray[indexPath.row].Name as String
        //cell.logoImage!.image = nil
        
        let imageUrl = self.paymentMethodArray[indexPath.row].LogoUrl as String
        
        cell.logoImage!.sd_setImageWithURL(NSURL(string: imageUrl))
//        Alamofire.request(.GET,imageUrl ).response{
//            (
//            request, response, data, error) in
//            cell.logoImage!.image = UIImage(data: data!, scale:1)
//        }

        
        
        //cell.selectedBtn.tag = indexPath.row
        //cell.selectedBtn.addTarget(self, action: "buttonActionForSelectedRow:", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        
        
        if self.paymentMethodArray[indexPath.row].Selected == 1 {
            
            cell.containerView.backgroundColor = UIColor.clearColor()
            cell.selectedButton.selected = true
            self.selectedRow = indexPath.row
        }else {
            
            cell.containerView.backgroundColor = UIColor.whiteColor()
            cell.selectedButton.selected = false
            
        }
        
        return cell
    }
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        
        self.selectedRow = indexPath.row
        
        
        for checkSelectedIndex in self.paymentMethodArray{
            checkSelectedIndex.Selected = 0
            
        }
        
        self.paymentMethodArray[indexPath.row].Selected = 1
        self.containerTableView.reloadData()
        
        
    }
    
    
//    func buttonActionForSelectedRow(sender:UIButton!)
//    {
//        
//        self.selectedRow = sender.tag
//        
//        
//        for checkSelectedIndex in self.paymentMethodArray{
//            checkSelectedIndex.Selected = 0
//            
//        }
//        
//        self.paymentMethodArray[sender.tag].Selected = 1
//        
//        self.containerTableView.reloadData()
//        
//    }
    
    
    @IBAction func continueToNextPage(sender: AnyObject) {
        
        
        self.showLoading()

        let str = String(format: "%@", self.paymentMethodArray[selectedRow].PaymentMethodSystemName)
        

        aPIManager?.setPaymentMethods(str, onSuccess: {
            
            
            
            self.hideLoading()
            self.showToast("Payment method saved successfully")

            NSNotificationCenter.defaultCenter().postNotificationName("com.notification.continueCheckout", object: 3);
            },
            onError: { message in
                self.hideLoading()
                print(message)
                self.showToast(message)
        })

        
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
