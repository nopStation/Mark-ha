//
//  CheckboxesTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/3/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class CheckboxesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var checkBoxScrollView: UIScrollView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
