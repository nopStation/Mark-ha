//
//  AddressViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 12/29/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire
import DropDown

class AddressViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    var aPIManager : APIManager?

    @IBOutlet weak var contentTableView: UITableView!
    var rightMenu : GlobalRightMenu?

    @IBOutlet weak var dropDownContainerView: UIView!
    var existingAddress = [GenericBillingAddress]()

    @IBOutlet weak var totalCartLabel: CustomLabel!
    
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navigationView: UIView!

    
    
    
    
    func reloadDropDownMenuList() {
        
        self.updateShoppingCartCount()
        self.rightMenu?.reloadDropDownMenuList()
        
    }
    
    func updateShoppingCartCount() {
        
        if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
            totalCartLabel.hidden = true
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
        } else {
            totalCartLabel.text = "\(APIManagerClient.sharedInstance.shoppingCartCount)"
            totalCartLabel.hidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
            
        self.rightMenu = GlobalRightMenu(dropDownContainerView: dropDownContainerView, containingViewController: self)
        
        contentTableView.registerNib(UINib(nibName: "AddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddressTableViewCell")

        self.contentTableView.estimatedRowHeight = 150.0
        self.contentTableView.rowHeight = UITableViewAutomaticDimension

        // Do any additional setup after loading the view.
        
    }

    
    override func viewWillAppear(animated: Bool) {
        
        self.reloadDropDownMenuList()

        self.showLoading()
        self.aPIManager = APIManager()
        
        self.aPIManager!.getAddress(onSuccess: {
            gotAddress in
            
            self.existingAddress = gotAddress
            self.hideLoading()
            
            self.contentTableView.reloadData()
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backBtnAct(sender: AnyObject) {
        
        self.navigationController?.popViewControllerAnimated(true)
        
    }
    
    
    @IBAction func myAccountBtnAct(sender: AnyObject) {
        
        if self.rightMenu!.dropDown.hidden {
            self.rightMenu!.dropDown.show()
        } else {
            self.rightMenu!.dropDown.hide()
        }
        
    }
    


    
    // MARK: - Table view data source
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
     
        return self.existingAddress.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        
        var cell = tableView.dequeueReusableCellWithIdentifier("AddressTableViewCell") as! AddressTableViewCell!
        if cell == nil {
            cell = AddressTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "AddressTableViewCell")
        }
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.deleteBtn.tag = indexPath.row
        cell.editBtn.tag = indexPath.row
        
        cell.deleteBtn.addTarget(self, action: "deleteBtnAct:", forControlEvents: UIControlEvents.TouchUpInside)
        cell.editBtn.addTarget(self, action: "editBtnAct:", forControlEvents: UIControlEvents.TouchUpInside)

        cell.containerView.layer.cornerRadius = 4.0
        
        var firstName = ""
        if let fName = self.existingAddress[indexPath.row].firstName as? String {
            firstName = fName
        }
        
        var lastName = ""
        if let lName = self.existingAddress[indexPath.row].lastName as? String {
            lastName = lName
        }
        
        cell.nameLbl.text = String(format: "%@ %@", firstName, lastName)
        
        
        var email = ""
        if let mail = self.existingAddress[indexPath.row].email as? String {
            email = mail
        }
        
        var phoneNo = ""
        if let phone  = self.existingAddress[indexPath.row].phoneNumber as? String {
            phoneNo = phone
        }
        
        var faxNo = ""
        if let fax   = self.existingAddress[indexPath.row].faxNumber as? String {
            faxNo = fax
        }
        
        var address1 = ""
        if let address    = self.existingAddress[indexPath.row].address1 as? String {
            address1 = address
        }
        
        var cityName = ""
        if let city  = self.existingAddress[indexPath.row].cityName as? String {
            cityName = city
        }
        
        var countryName = ""
        if let country  = self.existingAddress[indexPath.row].countryName as? String {
            countryName = country
        }
        
        let detailsInfo = String(format: "Email : %@\nPhone number : %@\nFax number : %@\n%@, %@\n%@",email, phoneNo , faxNo, address1, cityName, countryName)
        
        cell.detailsInfoLbl.text =  detailsInfo
        
        return cell
        
    }
    
    
    // MARK: - Table view data source
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    func deleteBtnAct(sender:UIButton!)
    {
        
        self.showLoading()
        self.aPIManager!.deleteAddress(self.existingAddress[sender.tag].id! , onSuccess: {
            gotAddress in
            
            self.existingAddress.removeAtIndex(sender.tag)
            self.contentTableView.reloadData()
            
            
            self.hideLoading()
            self.showToast("Successfully Deleted Address!")
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
                
        })
 
       
    }
    
    func editBtnAct(sender:UIButton!)
    {
       
        self.showLoading()
        self.aPIManager!.getCustomerInfo(self.existingAddress[sender.tag].id! , onSuccess: {
            gotAddress in
            self.hideLoading()
            
            let createNewAddVC = CreateNewAddressViewController(nibName: "CreateNewAddressViewController", bundle: nil)
            createNewAddVC.newBillingAddress = gotAddress
            createNewAddVC.customerAddressId = self.existingAddress[sender.tag].id!
            self.navigationController!.pushViewController(createNewAddVC, animated: true)
            

            
            
            },
            onError: { message in
                print(message)
                self.hideLoading()
                self.showToast(message)
                
                
        })
 
        
        
        
        
        
    }
    
    
    @IBAction func shoppingCartBtnAct(sender: AnyObject) {
        
            let shoppingCartView = ShoppingCartViewController(nibName: "ShoppingCartViewController", bundle: nil)
            self.navigationController!.pushViewController(shoppingCartView, animated: true)
            
    }

    
    @IBAction func addNewBtnAct(sender: AnyObject) {
               
        let createNewAddVC = CreateNewAddressViewController(nibName: "CreateNewAddressViewController", bundle: nil)
        self.navigationController!.pushViewController(createNewAddVC, animated: true)

    }
    
    
    func backToRootView(sender: UIButton) {
        let array = (self.navigationController?.viewControllers)! as NSArray
        self.navigationController?.popToViewController(array.objectAtIndex(0) as! UIViewController, animated: true)
    }
    
    func showToast(message:String) {
        let globalHud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        globalHud.mode = MBProgressHUDMode.Text
        globalHud.detailsLabelText = message
        self.performSelector("hideLoading", withObject: nil, afterDelay: 2)
    }
    
    func hideLoading() {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    }
    
    func showLoading() {
        MBProgressHUD.showHUDAddedTo(self.view, animated: true)
    }


}
