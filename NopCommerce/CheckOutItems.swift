//
//  CheckOutItems.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class CheckOutItems: NSObject {
    
    var Sku                        : NSString?
    var ImageUrl                   : String?
    var ProductId                  : NSInteger
    var ProductName                : NSString
    var ProductSeName              : NSString
    var UnitPrice                  : NSString
    var SubTotal                   : NSString
    var Discount                   : NSInteger?
    var Quantity                   : NSInteger
    var AllowedQuantities          : NSArray?
    var AttributeInfo              : NSString
    var RecurringInfo              : NSString?
    var RentalInfo                 : NSString?
    var Warnings                   : NSArray?
    var Id                         : NSInteger
    var CustomProperties           : NSArray?



    
    init(JSON : AnyObject) {
        
        self.Sku                          = (JSON.valueForKeyPath("Sku") as? NSString)
        self.ImageUrl                     = (JSON.valueForKeyPath("Picture.ImageUrl") as? String)
        self.ProductId                    = (JSON.valueForKeyPath("ProductId") as! NSInteger)
        self.ProductName                  = (JSON.valueForKeyPath("ProductName") as! NSString)
        self.ProductSeName                = (JSON.valueForKeyPath("ProductSeName") as! NSString)
        self.UnitPrice                    = (JSON.valueForKeyPath("UnitPrice") as! NSString)
        self.SubTotal                     = (JSON.valueForKeyPath("SubTotal") as! NSString)
        self.Discount                     = (JSON.valueForKeyPath("Discount") as? NSInteger)
        self.Quantity                     = (JSON.valueForKeyPath("Quantity") as! NSInteger)
        self.AllowedQuantities            = (JSON.valueForKeyPath("AllowedQuantities") as? NSArray)
        self.AttributeInfo                = (JSON.valueForKeyPath("AttributeInfo") as! NSString)
        self.RecurringInfo                = (JSON.valueForKeyPath("RecurringInfo") as? NSString)
        self.RentalInfo                   = (JSON.valueForKeyPath("RentalInfo") as? NSString)
        self.Warnings                     = (JSON.valueForKeyPath("Warnings") as? NSArray)
        self.Id                           = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.CustomProperties             = (JSON.valueForKeyPath("CustomProperties") as? NSArray)


        
    }


}
