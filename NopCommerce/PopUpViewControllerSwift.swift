//
//  PopUpViewControllerSwift.swift
//  NMPopUpView
//
//  Created by Nikos Maounis on 13/9/14.
//  Copyright (c) 2014 Nikos Maounis. All rights reserved.
//

import UIKit
import QuartzCore

@objc public class PopUpViewControllerSwift : UIViewController {
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var logoImg: UIImageView!
    
    //var checkOutGuestFlag = false
    
    @IBOutlet weak var checkOutBtn: UIButton!
    
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: "handleTap:")
        self.view.addGestureRecognizer(tapGesture)

        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    public func showInView(aView: UIView!, withImage image : UIImage!, withMessage message: String!, animated: Bool, checkOutGuestflag: Bool)
    {
        
        self.view.frame = aView.frame
        aView.addSubview(self.view)
        
        if checkOutGuestflag == false {
            self.checkOutBtn.hidden = true
        }
            
        else {
            self.checkOutBtn.hidden = false
        }
        

        
       // self.view.addSubview(aView)
        //logoImg!.image = image
        //messageLabel!.text = message
        if animated
        {
            self.showAnimate()
        }
    }
    
    
    func handleTap(sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        self.removeAnimate()

    }
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction public func closePopup(sender: AnyObject) {
        self.removeAnimate()
        

        
    }
    @IBAction func checkoutBtnAct(sender: AnyObject) {
        
        self.removeAnimate()

        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.proceedToCheckOut", object: nil)

        
    }
   
    @IBAction func registerBtnAct(sender: AnyObject) {
        
        
        self.removeAnimate()
        
        NSNotificationCenter.defaultCenter().postNotificationName("com.notification.proceedToRegister", object: nil)


    }
    
    @IBAction func loginBtnAct(sender: AnyObject) {
        
        self.removeAnimate()

       NSNotificationCenter.defaultCenter().postNotificationName("com.notification.proceedToLogin", object: nil)
        
    }
    
    
}
