//
//  PagerViewController.swift
//  NopCommerce
//
//  Created by BS-125 on 11/9/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import Alamofire

class PagerViewController: UIViewController {

    let tapRec = UITapGestureRecognizer()
    var productName = String ()

    @IBOutlet weak var imageView: UIImageView!
    var pageIndex: Int = 0
    var titleText: String?
    var imageFile: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView.image = nil
        
        tapRec.numberOfTapsRequired = 1
        tapRec.addTarget(self, action: "imageViewTapped")
        self.view.addGestureRecognizer(tapRec)
        
        Alamofire.request(.GET,self.imageFile ?? "").response{
            (
            request, response, data, error) in
            self.imageView.image = UIImage(data: data!, scale:1)
        }
        
         }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func imageViewTapped()
    {
        
        let fullImageViewC = FullImageViewController(nibName: "FullImageViewController", bundle: nil)
        fullImageViewC.productImage  =  self.imageView.image
        fullImageViewC.productName = self.productName as String
        self.presentViewController(fullImageViewC, animated: true, completion: nil)
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
