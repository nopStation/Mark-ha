//
//  CheckoutAttributes.swift
//  NopCommerce
//
//  Created by BS-125 on 12/14/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

public class CheckoutAttributes: NSObject {
    var AllowedFileExtensions     : NSArray?
    var AttributeControlType      : NSInteger
    var CustomProperties          : NSDictionary?
    var DefaultValue              : NSString?
    var Id                        : NSInteger
    var IsRequired                : Bool
    var Name                      : NSString
    var SelectedDay               : NSString?
    var SelectedMonth             : NSString?
    var SelectedYear              : NSString?
    var TextPrompt                : NSString?
    var Values                    : [CheckOutValusInfo]
    
    init(JSON : AnyObject) {
        
        self.AllowedFileExtensions     = (JSON.valueForKeyPath("AllowedFileExtensions") as? NSArray)
        self.AttributeControlType      = (JSON.valueForKeyPath("AttributeControlType") as! NSInteger)
        self.CustomProperties          = (JSON.valueForKeyPath("CustomProperties") as? NSDictionary)
        self.DefaultValue              = (JSON.valueForKeyPath("DefaultValue") as? NSString)
        self.Id                        = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.IsRequired                = (JSON.valueForKeyPath("IsRequired") as! Bool)
        self.Name                      = (JSON.valueForKeyPath("Name") as! NSString)
        self.SelectedDay               = (JSON.valueForKeyPath("SelectedDay") as? NSString)
        self.SelectedMonth             = (JSON.valueForKeyPath("SelectedMonth") as? NSString)
        self.SelectedYear              = (JSON.valueForKeyPath("SelectedYear") as? NSString)
        self.TextPrompt                = (JSON.valueForKeyPath("TextPrompt") as? NSString)
        self.Values                    = [CheckOutValusInfo]()
        
        for value in (JSON.valueForKeyPath("Values") as! NSArray){
            
            let valueData = CheckOutValusInfo(JSON: value)
            self.Values.append(valueData)
            
        }
    }
  }


public class CheckOutValusInfo: NSObject {
    
    
    
    var ColorSquaresRgb            : NSString?
    var CustomProperties           : NSString?
    var Id                         : NSInteger
    var IsPreSelected              : Bool
    var Name                       : NSString
    var PriceAdjustment            : NSString?
    
    init(JSON : AnyObject) {
        
        self.ColorSquaresRgb       = (JSON.valueForKeyPath("ColorSquaresRgb") as? NSString)
        self.CustomProperties      = (JSON.valueForKeyPath("CustomProperties") as? NSString)
        self.Id                    = (JSON.valueForKeyPath("Id") as! NSInteger)
        self.IsPreSelected         = (JSON.valueForKeyPath("IsPreSelected") as! Bool)
        self.Name                  = (JSON.valueForKeyPath("Name") as! NSString)
        self.PriceAdjustment       = (JSON.valueForKeyPath("PriceAdjustment") as? NSString)
        
    }
    
}