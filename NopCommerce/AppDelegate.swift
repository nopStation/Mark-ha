//
//  AppDelegate.swift
//  NopCommerce
//
//  Created by Jahid Hassan on 10/12/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import CoreData
import MFSideMenu
import DropDown

import Alamofire
import MBProgressHUD
import SwiftyJSON
import SDWebImage

import FirebaseAnalytics
import FirebaseMessaging
import FirebaseInstanceID

//#define DELEGATE UIApplication.sharedApplication().delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var navController:UINavigationController?
    let slideContainer = MFSideMenuContainerViewController()
    var homeViewC : ViewController?
    var aPIManager : APIManager!

    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        self.initializeNotificationServices()
        self.switchToRootViewController()
        
        FIRApp.configure()
        
        //        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
        //            kFIRParameterContentType:"cont",
        //            kFIRParameterItemID:"1"
        //            ])
        
        //FIRAnalytics.logEventWithName(kFIREventSelectContent,parameters:[])
        
        return true
    }
    
    
    func switchToRootViewController () {
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let slideMenuViewC = SlideMenuViewController(nibName: "SlideMenuViewController", bundle: nil)
        
        self.homeViewC = mainStoryBoard.instantiateViewControllerWithIdentifier("ViewController") as? ViewController
        slideContainer.leftMenuViewController = slideMenuViewC;
        slideContainer.centerViewController = homeViewC;
        
        self.navController = UINavigationController(rootViewController: slideContainer)
        window!.rootViewController = navController
        self.navController?.navigationBarHidden=true
        self.window!.makeKeyAndVisible()
    }
    
    
    
    func initializeNotificationServices() -> Void {
        
        let settings = UIUserNotificationSettings(forTypes: [.Sound, .Alert, .Badge], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        
        // This is an asynchronous method to retrieve a Device Token
        // Callbacks are in AppDelegate.swift
        // Success = didRegisterForRemoteNotificationsWithDeviceToken
        // Fail = didFailToRegisterForRemoteNotificationsWithError
        UIApplication.sharedApplication().registerForRemoteNotifications()

    }
    
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        
        print("deviceToken=== \(deviceToken)")
        
        let deviceTokenStr = convertDeviceTokenToString(deviceToken)
        
        print("%@",deviceTokenStr)

        
        self.aPIManager = APIManager()
        
        self.aPIManager.registerDeviceId(deviceTokenStr,onSuccess:{
                getRegister in
                
                
            },
            onError: { message in
                print(message)
                
                MBProgressHUD.hideHUDForView(self.window, animated: true)
                
        })

        // ...register device token with our Time Entry API server via REST
    }
    
    
    private func convertDeviceTokenToString(deviceToken:NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.stringByReplacingOccurrencesOfString(">", withString: "", options:NSStringCompareOptions.LiteralSearch, range:nil)
        
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        // Our API returns token in all uppercase, regardless how it was originally sent.
        // To make the two consistent, I am uppercasing the token string here.
        deviceTokenStr = deviceTokenStr.uppercaseString
        return deviceTokenStr
    }
    

    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print("Device token for push notifications: FAIL -- ")
        print(error.description)
    }
    
    
    // Called when a notification is received and the app is in the
    // foreground (or if the app was in the background and the user clicks on the notification).
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // display the userInfo
        
        print("userInfo=====\(userInfo)")

       /* 
         [aps: {
         alert = "This is my 1\Ufe0f\U20e3 notification. I would \U2764\Ufe0f to do it. You first \Ud83c\Udf81 is ready.... \Ud83d\Ude18\Ud83d\Udc4d";
         badge = 1;
         sound = "sound.caf";
         }, customObject: {"NotificationTypeId":0,"NotificationType":null,"ItemId":0,"Subject":"Hello...😊"}]
         */
        
        if let notification = userInfo["aps"] as? NSDictionary,
            let alert = notification["alert"] as? String {
                
                print("alert=====\(alert)")
            
            UIApplication.sharedApplication().applicationIconBadgeNumber = (notification["badge"]?.integerValue)!

             let alertController = UIAlertController(title: "Message", message: alert, preferredStyle: UIAlertControllerStyle.Alert)
             let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action:UIAlertAction!) in
                
                print("you have pressed the Cancel button");
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                
               }
              alertController.addAction(cancelAction)
            
             let OKAction = UIAlertAction(title: "OK", style: .Default) { (action:UIAlertAction!) in
                
                print("you have pressed OK button");
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                
              }
            alertController.addAction(OKAction)
            // Find the presented VC...

            var presentedVC = self.window?.rootViewController
            while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
            presentedVC!.presentViewController(alertController, animated: true, completion: nil)
                
                // call the completion handler
                // -- pass in NoData, since no new data was fetched from the server.
            completionHandler(UIBackgroundFetchResult.NoData)
        }
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        //handle your notification's action
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0

        
        print("handleActionWithIdentifier_userInfo \(userInfo)")
    }
    
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], withResponseInfo responseInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        //handle your notification's action response info
        print("handleActionWithIdentifier_responseInfo \(responseInfo)")

    }
    
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.bs.NopCommerce" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("NopCommerce", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }

}

