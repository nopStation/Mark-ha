//
//  DropdownListTableViewCell.swift
//  NopCommerce
//
//  Created by BS-125 on 12/3/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

class DropdownListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var dropDownBtn: UIButton!
    
    @IBOutlet weak var bottomLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
